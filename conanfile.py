from conan import ConanFile

class Mythen2DetectorRecipe(ConanFile):
    name = "mythen2detector"
    executable = "ds_Mythen2Detector"
    version = "1.2.4"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Cedric Castel, Arafat Nourredine"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/acquisition/1d/mythen2detector.git"
    description = "Mythen2Detector device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("nexuscpp/[>=1.0]@soleil/stable")
        
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
