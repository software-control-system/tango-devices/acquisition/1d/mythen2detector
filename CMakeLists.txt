cmake_minimum_required(VERSION 3.15)
project(${PROJECT_NAME} CXX)

find_package(nexuscpp CONFIG REQUIRED)
find_package(yat4tango CONFIG REQUIRED)
if (NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")
    find_package(crashreporting2 CONFIG)
endif()

add_compile_definitions(
    PROJECT_NAME=${PROJECT_NAME}
    PROJECT_VERSION=${PROJECT_VERSION}
    USE_NX_FINALIZER
)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
)

add_executable(${EXECUTABLE_NAME} ${sources})
target_include_directories(${EXECUTABLE_NAME} PRIVATE ${includedirs})
target_link_libraries(${EXECUTABLE_NAME} PRIVATE nexuscpp::nexuscpp)
target_link_libraries(${EXECUTABLE_NAME} PRIVATE yat4tango::yat4tango)
if (NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")
    target_link_libraries(${EXECUTABLE_NAME} PRIVATE crashreporting2::crashreporting2)
endif()

install(TARGETS ${EXECUTABLE_NAME} DESTINATION "." RUNTIME DESTINATION bin)
