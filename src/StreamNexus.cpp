/*************************************************************************/
/*! 
 *  \file   StreamNexus.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamNexus.h"
#include "Controller.h"

namespace Mythen2Detector_ns
{

#if defined(USE_NX_FINALIZER)
    nxcpp::NexusDataStreamerFinalizer StreamNexus::m_data_streamer_finalizer;
    bool StreamNexus::m_is_data_streamer_finalizer_started = false;
#endif
    
/*******************************************************************
 * \brief constructor
 *******************************************************************/
StreamNexus::StreamNexus(Tango::DeviceImpl* dev, Controller * in_controller) : Stream(dev, in_controller)
{
    DEBUG_STREAM << "StreamNexus::StreamNexus() - [BEGIN]" << std::endl;

    yat::MutexLock scoped_lock(m_data_lock);

	m_writer = NULL;
	
#if defined(USE_NX_FINALIZER)
    if ( ! StreamNexus::m_is_data_streamer_finalizer_started )
    {
        DEBUG_STREAM << "starting the underlying NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        StreamNexus::m_data_streamer_finalizer.start();
        StreamNexus::m_is_data_streamer_finalizer_started = true;
        DEBUG_STREAM << "starting the underlying NexusDataStreamerFinalizer - [END]" << std::endl;
    }
#endif

    DEBUG_STREAM << "StreamNexus::StreamNexus() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamNexus::~StreamNexus() 
{
    DEBUG_STREAM << "StreamNexus::~StreamNexus() - [BEGIN]" << endl;

    yat::MutexLock scoped_lock(m_data_lock);

    if (m_writer)
    {
        //- this might be required, in case we could not pass the writer to the finalizer	 
        delete m_writer;
        m_writer = NULL;
    }

    DEBUG_STREAM << "StreamNexus::~StreamNexus() - [END]" << endl;
}

/*******************************************************************
 * \brief Get the stream type
 * \return the stream type
 *******************************************************************/
enum Stream::Types StreamNexus::get_type(void) 
{
    return NEXUS_STREAM;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_acquisition_data acquisition data
 *******************************************************************/
void StreamNexus::update(const AcquisitionData & in_acquisition_data)
{
    yat::MutexLock scoped_lock(m_data_lock);

    const size_t    nb_Frames_read    = in_acquisition_data.get_frames_number    ();
    const uint32_t  first_frame_index = in_acquisition_data.get_first_frame_index(); 

    DEBUG_STREAM << "StreamNexus::update - "
                 << "frames indexes [" << first_frame_index << "->" << (first_frame_index + nb_Frames_read -1) << "]) " 
                 << "number (" << nb_Frames_read << ") size (" << in_acquisition_data.get_frame(0).size() << ")" << std::endl;

    uint32_t items = get_items(); // get flags

    for(size_t frameIndex = 0 ; frameIndex < nb_Frames_read ; frameIndex++)
    {
        const std::vector<int32_t> & ChannelsData = in_acquisition_data.get_frame(frameIndex);

        if (m_writer)
        {
            int32_t nb_modules          = m_controller->get_nb_modules();
            int     first_channel_Index = 0;

            for (int32_t modules_index = 0 ; modules_index < nb_modules; modules_index++)
            {
                // we must cut the spectrum to get only the module spectrum
                int32_t module_channels_nb = m_controller->get_module_channels(modules_index);

                std::vector<int32_t> module_spectrum(ChannelsData.begin() + first_channel_Index, 
                                                     ChannelsData.begin() + first_channel_Index + module_channels_nb);

                first_channel_Index += module_channels_nb;

                // frame data
                if(items & STREAM_ITEM_FRAME) 
                {
                    m_writer->PushData(m_frame_names[modules_index], &(module_spectrum[0]));
                }
            }
        }
    }
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamNexus::reset_index(void)
{
    DEBUG_STREAM << "StreamNexus::reset_index() - [BEGIN]" << endl;
    DEBUG_STREAM << "- ResetBufferIndex()" << endl;

    yat::MutexLock scoped_lock(m_data_lock);
    nxcpp::DataStreamer::ResetBufferIndex();

    DEBUG_STREAM << "StreamNexus::reset_index() - [END]" << endl;
}

/*******************************************************************
 * \brief Manage a nexus exception
 *******************************************************************/
void StreamNexus::OnNexusException(const nxcpp::NexusException &ex)
{
    DEBUG_STREAM << "StreamNexus::OnNexusException() - [BEGIN]" << endl;
    ostringstream ossMsgErr;
    ossMsgErr << endl;
    ossMsgErr << "===============================================" << endl;
    ossMsgErr << "Origin\t: " << ex.errors[0].origin << endl;
    ossMsgErr << "Desc\t: "   << ex.errors[0].desc   << endl;
    ossMsgErr << "Reason\t: " << ex.errors[0].reason << endl;
    ossMsgErr << "===============================================" << endl;
    ERROR_STREAM << ossMsgErr.str() << endl;
    yat::MutexLock scoped_lock(m_data_lock);        

    //1 - finalize the DataStreamer , this will set m_writer = 0 in order to avoid any new push data
    if (m_writer)
    {   
        close();
    }

    //2 -  inform the controller in order to stop acquisition & to set the state to FAULT
    m_controller->abort(ex.errors[0].desc);
    DEBUG_STREAM << "StreamNexus::OnNexusException() - [END]" << endl;        
};

/*******************************************************************
 * \brief Close the stream
 *******************************************************************/
void StreamNexus::close(void)
{
    Stream::close();

    DEBUG_STREAM << "StreamNexus::close() - [BEGIN]" << endl;

    yat::MutexLock scoped_lock(m_data_lock);            

    if (m_writer)
    {        
#if defined (USE_NX_FINALIZER) 
        //- Use NexusFinalizer to optimize the finalize which was extremely long !!!
        DEBUG_STREAM << "passing DataStreamer to the NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        nxcpp::NexusDataStreamerFinalizer::Entry *e = new nxcpp::NexusDataStreamerFinalizer::Entry();
        e->data_streamer = m_writer;
        m_writer = 0;
        StreamNexus::m_data_streamer_finalizer.push(e);
        DEBUG_STREAM << "passing DataStreamer to the NexusDataStreamerFinalizer - [END]" << std::endl;
#else  
        DEBUG_STREAM << "- Finalize() - [BEGIN]" << endl;
        m_writer->Finalize();
        delete m_writer;
        m_writer = NULL;
        DEBUG_STREAM << "- Finalize() - [END]" << endl;
#endif 
    }

    DEBUG_STREAM << "StreamNexus::close() - [END]" << endl;
}

/*******************************************************************
 * \brief Abort the stream
 *******************************************************************/
void StreamNexus::abort(void)
{
    DEBUG_STREAM << "StreamNexus::abort() - [BEGIN]" << endl;

    yat::MutexLock scoped_lock(m_data_lock);    

    if (m_writer)
    {        
        DEBUG_STREAM << "- Stop() - [BEGIN]" << endl;
        m_writer->Stop();
        delete m_writer;
        m_writer = 0;
        DEBUG_STREAM << "- Stop() - [END]" << endl;
    }

    DEBUG_STREAM << "StreamNexus::abort() - [END]" << endl;
}

/*******************************************************************
 * \brief Initialization
 *******************************************************************/
void StreamNexus::init(void)
{
    Stream::init();

    DEBUG_STREAM << "StreamNexus::init() - [BEGIN]" << endl;

    yat::MutexLock scoped_lock(m_data_lock);
    
    INFO_STREAM << "- Create DataStreamer :" << endl;
    INFO_STREAM << "\t- target path     = "  << m_target_path     << endl;
    INFO_STREAM << "\t- file name       = "  << m_target_file     << endl;
    INFO_STREAM << "\t- write mode      = "  << m_write_mode      << endl;
    INFO_STREAM << "\t- nb data per acq = "  << m_nb_data_per_acq << endl;
    INFO_STREAM << "\t- nb acq per file = "  << m_nb_acq_per_file << endl;
    INFO_STREAM << "\t- write data for : ";

    m_frame_names.clear();

    uint32_t items = get_items();

    // to activate the frame data in the stream
    if(items & STREAM_ITEM_FRAME) 
    {
        INFO_STREAM << "\t\t. " << "STREAM_ITEM_FRAME" << endl;
    }

    DEBUG_STREAM << endl;

    try
    {
		m_writer = new nxcpp::DataStreamer(m_target_file, (std::size_t)m_nb_data_per_acq, (std::size_t)m_nb_acq_per_file);

        DEBUG_STREAM << "- Initialize()" << endl;
        m_writer->Initialize(m_target_path);

        DEBUG_STREAM << "- SetExceptionHnadler()" << endl;
        m_writer->SetExceptionHandler(this);

        DEBUG_STREAM << "- Prepare Data Items :" << endl;

        int32_t nb_modules = m_controller->get_nb_modules();

        for (int32_t modules_index = 0 ; modules_index < nb_modules ; modules_index++)
        {
            // frames
            if(items & STREAM_ITEM_FRAME) 
            {
                int32_t module_channels_nb = m_controller->get_module_channels(modules_index);

                m_frame_names.push_back(yat::String::str_format("frame%02d", modules_index));
                DEBUG_STREAM << "\t- AddDataItem1D(" << m_frame_names[modules_index] << "," << module_channels_nb << ")" << endl;
                m_writer->AddDataItem1D(m_frame_names[modules_index], module_channels_nb);
            }
        }

        // configure the Writer mode 
        INFO_STREAM << "- Configure the Writer mode :" << endl;

        //by default is ASYNCHRONOUS			
        if (m_write_mode == WriteModes::SYNCHRONOUS )
        {
            INFO_STREAM << "\t- SYNCHRONOUS" << endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::SYNCHRONOUS);
        }
        else
        if (m_write_mode == WriteModes::DELAYED )
        {
            INFO_STREAM << "\t- DELAYED" << endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::DELAYED);
        }
        else
        if (m_write_mode == WriteModes::ASYNCHRONOUS )
        {
            INFO_STREAM << "\t- ASYNCHRONOUS" << endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::ASYNCHRONOUS);
        }
    }
    catch(const nxcpp::NexusException &ex)
    {
        Tango::Except::throw_exception((ex.errors[0].reason).c_str(),
                                       (ex.errors[0].desc).c_str(),
                                       (ex.errors[0].origin).c_str());
    }

    DEBUG_STREAM << "StreamNexus::init() - [END]" << endl;
}

//###########################################################################
}