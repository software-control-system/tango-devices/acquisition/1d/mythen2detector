/*************************************************************************/
/*! 
 *  \file   StreamNexus.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_STREAM_NEXUS_H_
#define MYTHEN_STREAM_NEXUS_H_

// TANGO
#include <tango.h>

// YAT
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>

// NEXUS
#include <nexuscpp/nexuscpp.h>

// LOCAL
#include "Stream.h"

namespace Mythen2Detector_ns
{
/*************************************************************************/
class StreamNexus : public Stream, public nxcpp::IExceptionHandler
{
    public:
        /// constructor
        StreamNexus(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~StreamNexus();

        /// get the stream type
        virtual enum Stream::Types get_type();

        /// Update the stream with new acquisition data
        virtual void update(const AcquisitionData & in_acquisition_data);

        /// Reset the file index
        virtual void reset_index(void);

        /// Manage a nexus exception
        void OnNexusException(const nxcpp::NexusException &ex);

        /// Initialization
        virtual void init(void);

        /// Close
        virtual void close(void);
        
        /// Abort the stream
        void abort(void);

    private :
		nxcpp::DataStreamer* m_writer     ;
        std::vector<string>  m_frame_names;
        yat::Mutex           m_data_lock  ;

        //- Nexus stuff	
    #if defined(USE_NX_FINALIZER)
        static nxcpp::NexusDataStreamerFinalizer m_data_streamer_finalizer;
        static bool m_is_data_streamer_finalizer_started;
    #endif
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_NEXUS_H_

//###########################################################################
