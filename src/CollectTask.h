/*************************************************************************/
/*! 
 *  \file   CollectTask.h
 *  \brief  class which calls Mythen commands
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_COLLECT_TASK_H_
#define MYTHEN_COLLECT_TASK_H_

// TANGO
#include <tango.h>

// YAT

// GLOBAL
#include "Config.h"

// LOCAL
#include "Com.h"
#include "ComNet.h"
#include "ComSim.h"
#include "AcquisitionData.h"

/*************************************************************************/
/* Macros used to log on cout or a tango logger.                         */
/* The const_get_logger method is used to keep CollectTask const methods */
/* MYTHEN_TANGO_INDEPENDANT needs to be defined in the pom.xml for an    */
/* integration in a tango independant software (like automatic testing). */
/*************************************************************************/
#ifdef MYTHEN_TANGO_INDEPENDANT
    #define MYTHEN_TASK_INFO_STREAM  std::cout
    #define MYTHEN_TASK_DEBUG_STREAM std::cout
    #define MYTHEN_TASK_ERROR_STREAM std::cout
#else
    #define MYTHEN_TASK_INFO_STREAM \
        if (const_get_logger()->is_info_enabled()) \
            const_get_logger()->info_stream() << log4tango::LogInitiator::_begin_log

    #define MYTHEN_TASK_DEBUG_STREAM \
        if (const_get_logger()->is_debug_enabled()) \
            const_get_logger()->debug_stream() << log4tango::LogInitiator::_begin_log

    #define MYTHEN_TASK_ERROR_STREAM \
        if (const_get_logger()->is_error_enabled()) \
            const_get_logger()->error_stream() << log4tango::LogInitiator::_begin_log
#endif
/*************************************************************************/

namespace Mythen2Detector_ns
{
    /// To avoid the lost of frames during the acquisition, we apply a coefficient to
    /// the computed frames number by readout call.
    static const double  FRAMES_NUMBER_BY_READOUT_CALL_COEFFICIENT = 10.0;

    static const int32_t FRAMES_NUMBER_MAXIMUM_VALUE               = 0x7FFFFFFF; // value used for start acquisition

    static const int32_t MAX_CHANNELS_BY_MODULES                   = 5120;

    static const float   UNAVAILABLE_ESTIMATED_FRAME_RATE          = -1.0f; // in gate mode, the estimated frame rate cannot be computed

    static const int32_t FRAME_RATE_LIMITATION_DISABLED            = -1; // expertFrameRateLimitation property can be set with this value to disable the functionnality

/*************************************************************************/
// abstract class used to send commands to Mythen server
class CollectTask : public Tango::LogAdapter
{
    public:
        ///----------------------------------------------------------------------------------
        /// Constructor
        explicit CollectTask(Tango::DeviceImpl * dev, const std::string& detector_type);

        /// Non virtual destructor
        ~CollectTask();

        ///----------------------------------------------------------------------------------
        /// Connection to server
        void connect(const std::string & in_hostname, int in_port);

        /// Disconnection from server
        void disconnect(void);

        ///----------------------------------------------------------------------------------
        /// Getter of the state
        Tango::DevState get_state(void);

        /// Getter of the status
        std::string get_status(void);

        ///----------------------------------------------------------------------------------
        /// Reset the detector and all active modules
        void reset(void) const;

        /// Start the acquisition
        void start_acquisition(bool in_snap_mode, uint32_t in_time_between_readout_calls_ms);

        /// Stop the acquisition
        void stop_acquisition(void);

        /// collect the data from the acquisition
        AcquisitionData collect(void);

        /// check if there is a saved frames number to restore
        void restore_saved_frames_number(void);

        /// Check if the acquisition is completed and if we can stop
        bool acquisition_is_completed(void) const;

        /// force the acquisition to be completed
        void force_acquisition_completed(void);

        /// get the last frame received from the hardware
        AcquisitionData get_last_frame(void);

        /// Get the current spectrum of a module
        std::vector<int32_t> get_module_frame(const int32_t & in_module_index);

        /// Get the number of channels of a module
        int32_t get_module_channels(const int32_t & in_module_index);

        ///----------------------------------------------------------------------------------
        /// Get the number of channels of all active modules
        int32_t get_nb_channels(void) const;

        ///----------------------------------------------------------------------------------
        /// Get the assembly date of the system
        std::string get_assembly_date(void) const;

        /// get the firmware version
        std::string get_firmware_version(void) const;

        /// get the software version
        std::string get_software_version(void) const;

        /// get the firmware version of active modules
        std::vector<std::string> get_modules_firmware_version(void) const;

        /// get the serial number of active modules
        std::vector<std::string> get_modules_serial_number(void) const;

        /// Get the serial number of a module
        std::string get_module_serial_number(const int32_t & in_module_index) const;

        /// get the DCS serial number
        int32_t get_serial_number(void) const;

        ///----------------------------------------------------------------------------------
        /// Get the DCS temperature in Kelvin degrees
        float get_dcs_temperature_kelvin(void) const;

        /// Get the DCS temperature in Celcius degrees
        float get_dcs_temperature_celcius(void) const;

        /// Get the temperature in kelvin degrees of activated modules
        std::vector<float> get_modules_temperature_kelvin(void) const;

        /// Get the temperature in celcius degrees of activated modules
        std::vector<float> get_modules_temperature_celcius(void) const;

        /// Get the temperature in kelvin degrees of a module
        float get_module_temperature_kelvin(const int32_t & in_module_index) const;

        /// Get the temperature in celcius degrees of a module
        float get_module_temperature_celcius(const int32_t & in_module_index) const;

        /// Get a string which contains all modules temperature values
        std::string get_string_modules_temperatures_celcius(void) const;

        /// Get the humidity of activated modules
        std::vector<float> get_modules_humidity(void) const;

        /// Get the humidity of a module
        float get_module_humidity(const int32_t & in_module_index) const;

        /// Get a string which contains all modules humidity values
        std::string get_string_modules_humidities(void) const;

        /// get the high voltage of activated modules
        std::vector<int32_t> get_modules_high_voltage(void) const;

        /// Get the high voltage of a module
        int32_t get_module_high_voltage(const int32_t & in_module_index) const;

        /// Get a string which contains all modules high voltage values
        std::string get_string_modules_high_voltages(void) const;

        ///----------------------------------------------------------------------------------
        /// Get the frame rate limitation value
        int32_t get_frame_rate_limitation(void) const;

        /// Set the frame rate limitation value 
        void set_frame_rate_limitation(const uint32_t in_new_value);
        
        /// Get the maximum frame rate for active detector modules
        float get_frame_rate_max(void) const;

        /// Log the max computed frame rate after a data change
        void log_max_computed_frame_rate(void) const;

        /// Compute the frame rate with differents informations
        float get_computed_frame_rate(void) const;

        /// Get the current frame number to be acquired
        uint32_t get_acquired_frames_number(void);

        ///----------------------------------------------------------------------------------
        /// Get the bad channels for each module
        void get_modules_bad_channels(std::vector< vector<bool> > & out_badChannelsByModule      , 
                                      std::vector<int32_t>        & out_badChannelsNumberByModule,
                                      int32_t                     & out_allBadChannelsNumber     ) const;

        /// Get the number of bad channels for all active modules
        int32_t get_nb_bad_channels(void) const;

        ///----------------------------------------------------------------------------------
        /// get the selected modules indexes (g_selectAllModules if all selected).
        int32_t get_selected_modules(void) const;

        /// Get the maximum modules number
        int32_t get_max_nb_modules(void) const;

        /// Get the activated modules number 
        // can not be const because it sets a class member (m_nb_modules)
        int32_t get_nb_modules(void);

        /// Select an active module by index (starts at 0)
        void select_module(const int32_t & in_selectedModule) const;

        /// Select all active modules
        void select_all_modules(void) const;

        /// get the number of channels for each module
        void get_modules_number_of_channels(std::vector<int32_t> & out_modulesChannels, int32_t & out_allModulesChannels) const;

        ///----------------------------------------------------------------------------------
        /// Load predefined settings for Cu
        void load_predefined_setting_Cu(void) const;

        /// Load predefined settings for Mo
        void load_predefined_setting_Mo(void) const;

        /// Load predefined settings for Cr
        void load_predefined_setting_Cr(void) const;

        /// Load predefined settings for Ag
        void load_predefined_setting_Ag(void) const;

        ///----------------------------------------------------------------------------------
        /// Set the exposure time
        void set_exposure_time(const int64_t & in_exposureTime_100ns) const;
        
        /// Get the exposure time
        int64_t get_exposure_time(void) const;

        /// Set the delay after a frame
        void set_delay_after_frame(const int64_t & in_delay_100ns) const;

        /// Get the delay after a frame
        int64_t get_delay_after_frame(void) const;

        /// Set the delay before frame (between trigger and start of measure)
        void set_delay_before_frame(const int64_t & in_delay_100ns) const;

        /// Get the delay before frame (between trigger and start of measure)
        int64_t get_delay_before_frame(void) const;

        /// Set the frames number within an acquisition
        void set_frames_number(const int32_t & in_framesNumber) const;

        /// Get the frames number within an acquisition
        int32_t get_frames_number(void) const;

        /// Check if the frames number corresponds to a start acquisition
        /// When we use a start acquisition, we need to change the frame number of the hardware to the maximum value
        bool is_start_acquisition_frames_number(const int32_t & in_framesNumber) const;

        /// Set the gates number for one frame
        void set_gates_number_by_frame(const int32_t & in_gatesNumber) const;

        /// Get the gates number for one frame
        int32_t get_gates_number_by_frame(void) const;

        /// Set the number of bits resolution (4, 8, 16, 24)
        void set_number_of_bits(const int32_t & in_bitsMode) const;
        
        /// Get the number of bits resolution (4, 8, 16, 24)
        int32_t get_number_of_bits(void) const;

        /// Get the readout time of the current bits mode
        int64_t get_readout_time(void) const;

        /// Get the readout time of a bits mode (4, 8, 16, 24)
        int64_t get_readout_time_of_bits_mode(const int32_t in_bitsMode) const;

        /// Set the gate activation state
        void set_gate(const bool in_IsEnabled) const;

        /// Get the gate state
        bool get_gate(void) const;

        /// Set the trigger activation state
        void set_trigger(const bool in_IsEnabled) const;

        /// Get the trigger state
        bool get_trigger(void) const;

        /// Set the continuous trigger activation state;
        void set_continuous_trigger(const bool in_IsEnabled) const;

        /// Get the continuous trigger state
        bool get_continuous_trigger(void) const;

        /// Set the trigger mode
        void set_trigger_mode(const Com::TriggerMode & in_triggerMode) const;

        /// Get the trigger mode
        Com::TriggerMode get_trigger_mode(void) const;

        ///----------------------------------------------------------------------------------
        /// Get the status
        int32_t get_dcs_status(void) const;

        /// Get the number of detected photons for each channels
        std::vector< std::vector<int32_t> > get_readout_data(int32_t in_nbFrames) const;

        ///----------------------------------------------------------------------------------
        /// Set the bad channel interpolation activation state
        void set_bad_channel_interpolation(const bool & in_IsEnabled) const;

        /// Get the bad channel interpolation activation state
        bool get_bad_channel_interpolation(void) const;

        /// Set the rate correction activation state
        void set_rate_correction(const bool & in_IsEnabled) const;

        /// Get the rate correction activation state
        bool get_rate_correction(void) const; 

        ///----------------------------------------------------------------------------------
        /// Set the energy for activated modules
        void set_modules_energy(const float & in_energy) const;

        /// Set the energy for only one module
        void set_module_energy(const int32_t & in_module_index, const float & in_energy) const;

        /// Get the energy for activated modules
        std::vector<float> get_modules_energy(void) const;

        /// Get the energy of a module
        float get_module_energy(const int32_t & in_module_index) const;

        /// Get the minimum energy for activated modules
        std::vector<float> get_modules_energy_min(void) const;

        /// Get the minimum energy of a module
        float get_module_energy_min(const int32_t & in_module_index) const;

        /// Get the maximum energy for activated modules
        std::vector<float> get_modules_energy_max(void) const;

        /// Get the maximum energy of a module
        float get_module_energy_max(const int32_t & in_module_index) const;

        /// Set the threshold energy for activated modules
        void set_modules_threshold(const float & in_threshold) const;

        /// Set the threshold energy  for only one module
        void set_module_threshold(const int32_t & in_module_index, const float & in_threshold) const;

        /// Get the threshold energy for activated modules
        std::vector<float> get_modules_threshold(void) const;

        /// Get the threshold energy of a module
        float get_module_threshold(const int32_t & in_module_index) const;

        /// Get the minimum threshold energy for activated modules
        std::vector<float> get_modules_threshold_min(void) const;

        /// Get the minimum threshold energy of a module
        float get_module_threshold_min(const int32_t & in_module_index) const;

        /// Get the maximum threshold energy for activated modules
        std::vector<float> get_modules_threshold_max(void) const;

        /// Get the maximum threshold energy of a module
        float get_module_threshold_max(const int32_t & in_module_index) const;

        ///----------------------------------------------------------------------------------
        /// Set the flatfield correction activation state
        void set_flatfield_correction(const bool & in_IsEnabled) const;

        /// Get the flatfield correction activation state
        bool get_flatfield_correction(void) const;

        /// Store the flatfield in a slot
        void store_flatfield(const int32_t              & in_slot_index  ,
                             const std::vector<int32_t> & in_flatfield   ) const;

        /// load a stored flatfield of a slot
        void set_loaded_flatfield(const int32_t & in_slot_index) const;

        /// Refresh the current loaded flatfield in the cache
        void refresh_loaded_flatfield(void);

        /// Get the current loaded flatfield
        std::vector<int32_t> get_loaded_flatfield(void); // can not be const because it can calls refresh_loaded_flatfield which is not.

        /// Get the current flatfield of a module
        std::vector<int32_t> get_module_loaded_flatfield(const int32_t & in_module_index);

        /// Set the flatfield path
        void        set_flatfield_path(const std::string & in_flatfield_path);

        /// Get the flatfield path
        std::string get_flatfield_path(void);

        /// Set the flatfield name
        void        set_flatfield_name(const std::string & in_name);

        /// Get the flatfield name
        std::string get_flatfield_name(void);

        ///----------------------------------------------------------------------------------
        /// Get the error string
        std::string get_error_string(int32_t in_ErrorId) const;

        /// Get informations about DCS4 and active modules
        std::vector<std::pair<std::string, std::string> > get_versions_informations(void) const;

        /// Compute the frame rate with differents informations
        float get_computed_frame_rate(const Com::TriggerMode in_trigger_mode      , 
                                      const int64_t          in_bitsMode          , 
                                      const int64_t          in_exposure_time     , 
                                      const int64_t          in_delay_after_frame , 
                                      const int64_t          in_delay_before_frame) const;

        /// Check if the framerate will be higher than the maximum allowed frame rate.
        void check_frame_rate(const Com::TriggerMode in_trigger_mode      ,        // the frame rate depends on the trigger mode
                              const int64_t          in_bitsMode          ,        // the readout time of the bits mode is necessary to compute the frame time
                              const int64_t          in_exposure_time     ,        // the exposure time is necessary to compute the frame time
                              const int64_t          in_delay_after_frame ,        // the delay after a frame
                              const int64_t          in_delay_before_frame) const; // the delay before a frame (between the trigger and the start of exposure)

    private:
        ///----------------------------------------------------------------------------------
        /// set the last frame received from the hardware
        void set_last_frame(const AcquisitionData & in_last_acquisition_data);

        ///----------------------------------------------------------------------------------
        /// get the DCS temperature in default scale
        float get_dcs_temperature(void) const;

        /// Get the temperature in default scale of activated modules
        std::vector<float> get_modules_temperature(void) const;

        ///----------------------------------------------------------------------------------
        /// Set a time
        void set_time(Com::ServerCmd      in_cmdId      ,
                      const int64_t     & in_delay_100ns,
                      const std::string & in_label      ,
                      const std::string & in_callerName ) const;

        /// Get a time
        int64_t get_time(Com::ServerCmd      in_cmdId       ,
                         const std::string & in_label       ,
                         const std::string & in_callerName  ) const;

        /// Set an activation boolean
        void set_activation_boolean(Com::ServerCmd      in_cmdId     ,
                                    const bool          in_IsEnabled ,
                                    const std::string & in_label     ,
                                    const std::string & in_callerName) const;

        /// Get an activation boolean
        bool get_activation_boolean(Com::ServerCmd      in_cmdId     ,
                                    const std::string & in_label     ,
                                    const std::string & in_callerName) const;

        /// Set a float
        void set_float(Com::ServerCmd      in_cmdId        ,
                       const float         in_data         ,
                       const bool          in_useSelection ,
                       const std::string & in_label        ,
                       const std::string & in_optionalScale,
                       const std::string & in_callerName   ) const;

        /// Get a float
        float get_float(Com::ServerCmd      in_cmdId        ,
                        const std::string & in_label        ,
                        const std::string & in_optionalScale,
                        const std::string & in_callerName   ) const;


        /// Set a 32 bits integer
        void set_int32(Com::ServerCmd      in_cmdId        ,
                       const int32_t     & in_data         ,
                       const std::string & in_label        ,
                       const std::string & in_optionalScale,
                       const std::string & in_callerName   ) const;

        /// Get a 32 bits integer
        int32_t get_int32(Com::ServerCmd      in_cmdId        ,
                          const std::string & in_label        ,
                          const std::string & in_optionalScale,
                          const std::string & in_callerName   ) const;


        /// Send a simple command (no parameters)
        void simple_command(Com::ServerCmd      in_cmdId       ,
                            const bool          in_useSelection,
                            const std::string & in_label       ,
                            const std::string & in_callerName  ) const;

        /// Get a float vector
        std::vector<float> get_float_vector(Com::ServerCmd       in_cmdId        ,
                                            const bool           in_useSelection ,
                                            const std::string  & in_label        ,
                                            const std::string  & in_optionalScale,
                                            const std::string  & in_callerName   ) const;

        ///----------------------------------------------------------------------------------
        /// get a string
        std::string get_string(Com::ServerCmd      in_cmdId      , 
                               const int32_t       in_finalSize  ,
                               const int32_t       in_networkSize,
                               const std::string & in_label      ,
                               const std::string & in_callerName ) const;

        ///----------------------------------------------------------------------------------
        /// get a vector of data for a number of elements
        template< typename T >
        std::vector<T> get_data_vector(Com::ServerCmd      in_cmdId         , 
                                       const int32_t       in_elementsNumber,
                                       const std::string & in_callerName    ) const;

        ///----------------------------------------------------------------------------------
        /// show the information stored in a vector for active modules
        template<typename T>
        void show_vector_information(const std::vector<T> & in_data          ,
                                     const std::string    & in_label         ,
                                     const std::string    & in_optionalScale = "") const;

         /// show the information stored in a vector for active modules
        template<typename T>
        void show_vector_proportion_information(const std::vector<T> & in_data          ,
                                                const std::vector<T> & in_maxdata       ,
                                                const std::string    & in_label         ,
                                                const std::string    & in_optionalScale = "") const;

        /// show the information of several types of data
        template<typename T>
        void show_information(const T           & in_data          ,
                              const std::string & in_label         ,
                              const std::string & in_optionalScale = "") const;

        /// show the action
        void show_action(const std::string & in_label) const;

        /// send a tango exception
        void throw_tango_exception(const char * reason,
                                   const char * desc  ,
                                   const char * origin) const;

        /// convert an int to an hexa string
        static std::string int_to_hex( int32_t in_value );

        //! \brief Gets the associated logger.
        inline log4tango::Logger * const_get_logger() const;

        ///----------------------------------------------------------------------------------
        /// Setter of the status
        void set_status(const std::string& status);

        /// Setter of the state
        void set_state(Tango::DevState state);

        /// Manage the FAULT status with an exception
        void on_fault(const Tango::DevFailed & df);

        /// Manage the FAULT status with an error message
        void on_fault(const std::string& status_message);

        /// Compute the state and status 
        void compute_state_status();

        /// Set the current acquiried frame number 
        void set_acquired_frames_number(const uint32_t in_new_value);

    protected:
        ///----------------------------------------------------------------------------------
        
        Tango::DeviceImpl    * m_device; /// Owner Device server object
        Com                  * m_Com   ; /// Com - to send commands to the device 

        int32_t                m_nb_modules                  ; /// Store the number of active modules
        std::vector<int32_t>   m_modules_channels            ; /// Store the number of channels for each modules
        int32_t                m_all_modules_channels        ; /// Store the number of channels for all modules
        std::vector<int32_t>   m_modules_channels_start_index; /// Store the channels start index for each modules

        Tango::DevState        m_state             ; /// current state
        std::stringstream      m_status            ; /// current status
        yat::Mutex             m_state_lock        ; /// mutex used to protect state access
        yat::Mutex             m_last_frame_lock   ; /// mutex used to protect m_last_acquisition_data access
        yat::Mutex             m_acquisition_lock  ; /// mutex used to protect start-snap-stop access
        yat::Mutex             m_frames_number_lock; /// mutex used to protect the acquired frame number data

        int32_t                m_saved_frames_number          ; /// the frames number in the device before an acquisition
        uint32_t               m_acquired_frames_number       ; /// the current frames number which was acquired
        uint32_t               m_frames_number_to_be_acquired ; /// the frames number for an acquisition
        uint32_t               m_time_between_readout_calls_ms; /// time between two calls of readout
        uint32_t               m_frames_number_by_readout_call; /// number of frames we read with every readout call

        AcquisitionData        m_last_acquisition_data   ; /// last acquisition data received from the hardware
        bool                   m_acquisition_in_snap_mode; /// current acquisition is in snap (true) or start mode (false)

        std::string            m_flatfield_path     ; /// flatfield path to load and save flatfield file
        std::string            m_last_flatfield_name; /// last loaded flafield name
        std::vector<int32_t>   m_flatfield          ; /// last read flafield

        std::vector<std::string> m_modules_serial_number; /// modules serial numbers
        
        int32_t                m_frame_rate_limitation; /// max frame rate we allow to avoid a camera problem in higher speeds
    };

} // namespace Mythen2Detector_ns

#include "CollectTask.hpp"

#endif // MYTHEN_COLLECT_TASK_H_

//###########################################################################
