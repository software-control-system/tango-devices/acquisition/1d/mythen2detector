/*************************************************************************/
/*! 
 *  \file   Flatfield.cpp
 *  \brief  class used to manage the flatfield files
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "Flatfield.h"
#include "Controller.h"

#include <glob.h>

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
Flatfield::Flatfield(Tango::DeviceImpl* dev, Controller * in_controller) : 
                     Tango::LogAdapter(dev), m_device(dev), m_controller(in_controller)
{
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Flatfield::~Flatfield() 
{
}

/*******************************************************************
 * \brief Build a flatfield file name
 * \param[in] in_name          flatfield name (alias)
 * \param[in] in_module_index  module index [0...active modules max[
 *******************************************************************/
std::string Flatfield::build_file_name(const std::string & in_name        ,
                                       const int32_t     & in_module_index)
{
    std::string serial   = m_controller->get_module_serial_number(in_module_index);
    std::string file_name;

    file_name = in_name + FLATFIELD_DELIMITER + serial + FLATFIELD_FILE_EXTENSION;
    return file_name;
}

/*******************************************************************
 * \brief Get the flatfield files path
 * \return flatfield files path (with '/' at end)
 *******************************************************************/
std::string Flatfield::get_path(void)
{
    std::string path = m_controller->get_flatfield_path();

    if(path.size() > 0)
    {
        if(path[path.size() - 1] != '/')
            path += "/";
    }

    return path;
}

/*******************************************************************
 * \brief Build a complete flatfield file path
 * \param[in] in_name          flatfield name (alias)
 * \param[in] in_module_index  module index [0...active modules max[
 *******************************************************************/
std::string Flatfield::build_complete_path(const std::string & in_name        ,
                                           const int32_t     & in_module_index)
{
    std::string file_name     = build_file_name(in_name, in_module_index);
    std::string path          = get_path();
    std::string complete_path = path + file_name;

    return complete_path;
}

/*******************************************************************
 * \brief Save a module flatfield into a file
 * \param[in] in_name          flatfield name
 * \param[in] in_module_index  module index [0...active modules max[
 * \param[in] in_flatfield     flatfield data
 *******************************************************************/
void Flatfield::save(const std::string          & in_name        ,
                     const int32_t              & in_module_index,
                     const std::vector<int32_t> & in_flatfield   )
{
    std::ostringstream MsgErr;  

    size_t      channels_nb = in_flatfield.size();
    std::string file_name   = build_complete_path(in_name, in_module_index);

    DEBUG_STREAM << "Flatfield::save : " << file_name << std::endl;

    // check the file non-existence
    if(exists(in_name, in_module_index))
    {
        MsgErr << "Error during saving of flatfield file:" << std::endl;
        MsgErr << file_name << std::endl;
        MsgErr << "File already exists!" << std::endl;
    }
    else
    {
        std::ofstream output_file(file_name.c_str(), std::ios::out | std::ofstream::binary);

        for(std::size_t index = 0 ; index < channels_nb ; index++)
        {
            output_file << in_flatfield[index];
            output_file << std::endl;
        }

        output_file.flush();
        output_file.close();
    }

    // throw an exception ?
    if(!MsgErr.str().empty())
    {
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       MsgErr.str().c_str(),
                                       "Flatfield::save");
    }
}

/*******************************************************************
 * \brief Check if a flatfield file exists
 * \param[in]  in_name          flatfield name
 * \param[in]  in_module_index  module index [0...active modules max[
 * \return     true is the flatfield exists, false either
 *******************************************************************/
bool Flatfield::exists(const std::string & in_name        ,
                       const int32_t     & in_module_index)
{
    std::string file_name = build_complete_path(in_name, in_module_index);
    bool        result    = true;

    std::ifstream input_file(file_name.c_str(), std::ios::in | std::ifstream::binary);

    if ( input_file.fail() ) 
    {
        result = false;
    }
    else
    {
        // should not be necessary with the destructor
        input_file.close();
    }

    return result;
}

/*******************************************************************
 * \brief Load the module flatfield of a file
 * \param[in]  in_name          flatfield name
 * \param[in]  in_module_index  module index [0...active modules max[
 * \param[out] out_flatfield    container filled with the flatfield
 *******************************************************************/
void Flatfield::load(const std::string    & in_name        ,
                     const int32_t        & in_module_index,
                     std::vector<int32_t> & out_flatfield  )
{
    std::ostringstream MsgErr;

    std::string file_name     = build_complete_path(in_name, in_module_index);
    int32_t     channels_nb   = m_controller->get_module_channels(in_module_index);
    string      channel_string;

    INFO_STREAM << "Flatfield::load : " << file_name << std::endl;

    std::ifstream input_file(file_name.c_str(), std::ios::in | std::ifstream::binary);

    if ( input_file.fail() ) 
    {
        MsgErr << "Failed to open the flatfield file:";
        MsgErr << file_name << std::endl;
        MsgErr << "Error: " << strerror(errno) << std::endl;
    }
    else
    {
        int32_t     channel_value ;

        for(int32_t index = 0 ; index < channels_nb ; index++)
        {
            if((getline(input_file, channel_string)) && (channel_string.size() > 0))
            {
                std::istringstream ss(channel_string);
                ss >> channel_value;

                out_flatfield.push_back(channel_value);
            }
            else
            // incoherent number of channels in the flatfield file
            {       
                MsgErr << "Error during the loading of flatfield file:" << std::endl;
                MsgErr << file_name << std::endl;
                MsgErr << "Find an incoherent number of channels!" << std::endl;
                MsgErr << "Needed " << channels_nb << " channels." << std::endl;
                break;
            }
        }

        input_file.close();
    }

    // throw an exception ?
    if(!MsgErr.str().empty())
    {
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       MsgErr.str().c_str(),
                                       "Flatfield::load");
    }
}

/*******************************************************************
 * \brief Load the flatfields of all modules
 * \param[in]  in_name          flatfield name
 * \return     container filled with the flatfield
 *******************************************************************/
std::vector<int32_t> Flatfield::load(const std::string & in_name)
{
    std::vector<int32_t> flatfield;

    int32_t nb_modules  = m_controller->get_nb_modules();
    int32_t module_index;

    // firstly, check the existence of all needed flatfield files
    {
        std::ostringstream MsgErr;
    
        for(module_index = 0; module_index < nb_modules; module_index++)
        {
            if(!exists(in_name, module_index))
            {
                if(MsgErr.str().empty())
                {
                    MsgErr << "Error during the loading of flatfield files!" << std::endl;
                    MsgErr << "Missing files:" << std::endl;
                }

                MsgErr << build_complete_path(in_name, module_index) << std::endl;
            }
        }

        // throw an exception ?
        if(!MsgErr.str().empty())
        {
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           MsgErr.str().c_str(),
                                           "Flatfield::load");
        }
    }

    // secondly, load the flatfield files and fill the complete flatfield
    for(module_index = 0; module_index < nb_modules; module_index++)
    {
        load(in_name, module_index, flatfield);
    }

    return flatfield;
}

/*******************************************************************
 * \brief List the flatfield files in the directory
 * \return container filled with the flatfield files names
 *******************************************************************/
std::vector<std::string> Flatfield::get_list(void)
{
    std::string              path      = get_path();
    std::string              mask      = path + "*" + FLATFIELD_FILE_EXTENSION;
    std::vector<std::string> files_list;

    glob_t globbuf;
    
    if(glob(mask.c_str(), 0, NULL, &globbuf) == 0)
    {
        for(size_t i = 0; i < globbuf.gl_pathc; i++)
        {
            files_list.push_back(string(globbuf.gl_pathv[i]));
        }
    }

    globfree(&globbuf);

    return files_list;
}

//###########################################################################
}