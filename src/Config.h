/*************************************************************************/
/*! 
 *  \file   Config.h
 *  \brief  class which allow to set the global configuration of 
 *          tango independant classes (MythemTask, Com, ComNet,
 *          ComSimulator)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_CONFIG_H_
#define MYTHEN_CONFIG_H_

// SYSTEM
#include <map>
#include <string>
#include <stdarg.h>
#include <iomanip>
#include <algorithm>

#endif /* MYTHEN_CONFIG_H */

//###########################################################################
