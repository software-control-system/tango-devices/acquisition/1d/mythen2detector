/*************************************************************************/
/*! 
 *  \file   StreamNo.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_STREAM_NO_H_
#define MYTHEN_STREAM_NO_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Stream.h"

namespace Mythen2Detector_ns
{
/*************************************************************************/
class StreamNo : public Stream
{
    public:
        /// constructor
        StreamNo(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~StreamNo();

        /// get the stream type
        virtual enum Stream::Types get_type();

        /// Update the stream with new acquisition data
        virtual void update(const AcquisitionData & in_acquisition_data);

        /// Reset the file index
        virtual void reset_index(void);

    private :
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_NO_H_

//###########################################################################
