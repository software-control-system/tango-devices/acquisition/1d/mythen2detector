/*************************************************************************/
/*! 
 *  \file   Stream.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "Stream.h"
#include "Controller.h"

#include <yat/utils/String.h>

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
Stream::Stream(Tango::DeviceImpl* dev, Controller * in_controller) : 
               Tango::LogAdapter(dev), m_controller(in_controller), m_device(dev)
{
    DEBUG_STREAM << "Stream::Stream() - [BEGIN]" << std::endl;

    // default status management
    set_status("Starting...");
    set_state (Tango::INIT  );

    m_target_path     = "";
    m_target_file     = "";
    m_nb_data_per_acq = 0;
    m_nb_acq_per_file = 0;
    m_write_mode      = WriteModes::UNDEFINED;
    m_items           = 0;

    DEBUG_STREAM << "Stream::Stream() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Stream::~Stream() 
{
}

/*******************************************************************
 * \brief Get the target path
 * \return the target path
 *******************************************************************/
std::string Stream::get_target_path(void) 
{
    return m_target_path;
}

/*******************************************************************
 * \brief Get the target file
 * \return the target file
 *******************************************************************/
std::string Stream::get_target_file(void) 
{
    return m_target_file;
}

/*******************************************************************
 * \brief Get the number of data per acquisition
 * \return number of data per acquisition
 *******************************************************************/
uint32_t Stream::get_nb_data_per_acq(void) 
{
    return m_nb_data_per_acq;
}

/*******************************************************************
 * \brief Get the number of acquisition per file
 * \return number of acquisition per file
 *******************************************************************/
uint32_t Stream::get_nb_acq_per_file(void) 
{
    return m_nb_acq_per_file;
}

/*******************************************************************
 * \brief Get the write mode
 * \return the write mode
 *******************************************************************/
enum Stream::WriteModes Stream::get_write_mode(void)
{
    return m_write_mode;
}

/*******************************************************************
 * \brief Get the items configuration (bit field)
 * \return the items configuration
 *******************************************************************/
uint32_t Stream::get_items(void) 
{
    return m_items;
}

/*******************************************************************
 * \brief Set the target path
 * \param[in] in_target_path target path
 *******************************************************************/
void Stream::set_target_path(const std::string & in_target_path) 
{
    m_target_path = in_target_path;
}

/*******************************************************************
 * \brief Set the target file
 * \param[in] in_target_file target file
 *******************************************************************/
void Stream::set_target_file(const std::string & in_target_file) 
{
    m_target_file = in_target_file;
}

/*******************************************************************
 * \brief Set the number of data per acquisition
 * \param[in] in_nb_data_per_acq number of data per acquisition
 *******************************************************************/
void Stream::set_nb_data_per_acq(const uint32_t & in_nb_data_per_acq) 
{
    m_nb_data_per_acq = in_nb_data_per_acq;
}

/*******************************************************************
 * \brief Set the number of acquisition per file
 * \param[in] in_nb_data_per_acq number of acquisition per file
 *******************************************************************/
void Stream::set_nb_acq_per_file(const uint32_t & in_nb_acq_per_file) 
{
    m_nb_acq_per_file = in_nb_acq_per_file;
}

/*******************************************************************
 * \brief Set the write mode
 * \param[in] in_write_mode the write mode
 *******************************************************************/
void Stream::set_write_mode(const enum Stream::WriteModes & in_write_mode)
{
    m_write_mode = in_write_mode;
}

/*******************************************************************
 * \brief Set the items configuration (bit field)
 * \param[in] in_items the items configuration
 *******************************************************************/
void Stream::set_items(const uint32_t & in_items) 
{
    m_items = in_items;
}

/*******************************************************************
 * \brief Get the data stream string
 * \return the data stream string (stream file and data items)
 *******************************************************************/
std::string Stream::get_data_streams(void)
{
    std::string data_streams = m_target_file + ":";

    int32_t nb_modules = m_controller->get_nb_modules();

    for (int32_t modules_index = 0 ; modules_index < nb_modules ; modules_index++)
    {
        // frames
        if(m_items & STREAM_ITEM_FRAME) 
        {
            data_streams += yat::String::str_format("frame%02d,", modules_index);
        }
    }

    return data_streams;
}

//============================================================================================================
/*******************************************************************
 * \brief Init the stream after a new configuration
*******************************************************************/
void Stream::init(void)
{
}

/*******************************************************************
 * \brief Close the stream
 *******************************************************************/
void Stream::close(void)
{
}

/*******************************************************************
 * \brief Setter of the state
*******************************************************************/
void Stream::set_state(Tango::DevState state)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState Stream::get_state(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    compute_state_status();
    return m_state;
}

/*******************************************************************
 * \brief Setter of the status
*******************************************************************/
void Stream::set_status(const std::string& status)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string Stream::get_status(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an exception
*******************************************************************/
void Stream::on_fault(Tango::DevFailed df)
{
    DEBUG_STREAM << "Stream::on_fault()" << std::endl;

    std::stringstream status;
    status.str("");
    status << "Origin\t: " << df.errors[0].origin << std::endl;
    status << "Desc\t: "   << df.errors[0].desc   << std::endl;
    status << "Reason\t: " << df.errors[0].reason << std::endl;

    on_fault(status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an error message
*******************************************************************/
void Stream::on_fault(const std::string& status_message)
{
    set_state (Tango::FAULT  );
    set_status(status_message);
}

/*******************************************************************
 * \brief Compute the state and status 
*******************************************************************/
void Stream::compute_state_status()
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    
    if(m_state != Tango::FAULT)
    {
        set_state (Tango::RUNNING);
        set_status("Stream is running...");
    }
}

//###########################################################################
}