/*************************************************************************/
/*! 
 *  \file   ComSim.hpp
 *  \brief  mythen communication simulator class (templates part)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_COM_SIM_HPP_
#define MYTHEN_COM_SIM_HPP_

//TANGO
#include <tango.h>

namespace Mythen2Detector_ns
{

/*******************************************************************
 * \brief Write a value in the reception buffer
 *******************************************************************/
template<typename T>
void ComSim::write_in_reception_buffer(T in_value, uint8_t * out_recv_buffer, int in_recv_lenght)
{
    if(in_recv_lenght != sizeof(in_value))
    {
        COM_ERROR_STREAM << "ComSim::write_in_receive_buffer : size problem! " 
                         << "value size ("  << sizeof(in_value) << ") " 
                         << "buffer size (" << in_recv_lenght   << ")" 
                         << endl;

        std::ostringstream MsgErr;

        MsgErr << "bad buffer size!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::write_in_reception_buffer()");
	}
    
    memcpy(out_recv_buffer, &in_value, sizeof(in_value));
}

/*******************************************************************
 * \brief Read a value in the sent buffer
 *******************************************************************/
template<typename T>
void ComSim::read_in_sent_buffer(T & out_value, const uint8_t * in_send_buffer, int in_send_lenght)
{
    if(in_send_lenght != sizeof(out_value))
    {
        COM_ERROR_STREAM << "ComSim::read_in_sent_buffer : size problem! " 
                         << "value size ("  << sizeof(out_value) << ") " 
                         << "buffer size (" << in_send_lenght    << ")" 
                         << endl;

        std::ostringstream MsgErr;

        MsgErr << "bad buffer size!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::read_in_sent_buffer()");
	}
    
    memcpy(&out_value, in_send_buffer, in_send_lenght);
}

/*******************************************************************
 * \brief Read a value in the args buffer
 *******************************************************************/
template<typename T>
void ComSim::read_in_args(T & out_value, const std::string in_format, va_list in_args)
{
    std::string value = Com::string_format_arg(in_format, in_args);
    std::istringstream ( value ) >> out_value;
}

/*******************************************************************
 * \brief Write an array of values in the reception buffer
 *******************************************************************/
template<typename T>
void ComSim::write_array_in_reception_buffer(std::vector<T> in_values, uint8_t * out_recv_buffer, int in_recv_lenght)
{
    if(static_cast<std::size_t>(in_recv_lenght) != (sizeof(T) * in_values.size()))
    {
        COM_ERROR_STREAM << "ComSim::write_array_in_reception_buffer : size problem! " 
                         << "values size ("  << (sizeof(T) * in_values.size()) << ") " 
                         << "buffer size (" << in_recv_lenght   << ")" 
                         << endl;

        std::ostringstream MsgErr;

        MsgErr << "bad buffer size!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::write_array_in_reception_buffer()");
	}

    memcpy(out_recv_buffer, in_values.data(), (sizeof(T) * in_values.size()));
}

} // namespace Mythen2Detector_ns

#endif /* MYTHEN_COM_SIM_HPP_ */

//###########################################################################
