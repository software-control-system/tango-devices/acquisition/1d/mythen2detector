/*************************************************************************/
/*! 
 *  \file   Com.hpp
 *  \brief  mythen communication abstract class (templates part)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_COM_HPP_
#define MYTHEN_COM_HPP_

//TANGO
#include <tango.h>

namespace Mythen2Detector_ns
{

/*******************************************************************
 * \brief Send a get command to the DCS and receive its response
 *        Simple data returned version
 *******************************************************************/
template<typename T>
bool Com::get_command(Com::ServerCmd in_CmdId, T & out_Buffer, int32_t * out_Error) 
{
    return command(in_CmdId, NULL, 0, reinterpret_cast<uint8_t *>(&out_Buffer), sizeof(T), out_Error, NULL); // no args for get command
}

/*******************************************************************
 * \brief Send a get command to the DCS and receive its response
 *        data array returned version
 *******************************************************************/
template<typename T>
bool Com::get_command(Com::ServerCmd in_CmdId, T * out_Buffer, int in_Length, int32_t * out_Error) 
{
    return command(in_CmdId, NULL, 0, reinterpret_cast<uint8_t *>(out_Buffer), in_Length * sizeof(T), out_Error, NULL);
}

/*******************************************************************
 * \brief Send an action command to the DCS and receive its response
 *        data array returned version
 *******************************************************************/
// Using reference 'out_Error' as parameter for va_start() results in undefined behaviour, so I added in_dummy parameter.
template<typename T>
bool Com::action_command(Com::ServerCmd in_CmdId, T * out_Buffer, int in_Length, int32_t * out_Error, ...) 
{
    va_list args  ;
    bool    result;

    va_start(args, out_Error);
    result = command(in_CmdId, NULL, 0, reinterpret_cast<uint8_t *>(out_Buffer), in_Length * sizeof(T), out_Error, args);
    va_end(args);

    return result;
}

} // namespace Mythen2Detector_ns

#endif /* MYTHEN_COM_HPP_ */

//###########################################################################
