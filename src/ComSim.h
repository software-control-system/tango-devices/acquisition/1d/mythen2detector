/*************************************************************************/
/*! 
 *  \file   ComSim.h
 *  \brief  mythen communication simulator class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef MYTHEN_COM_SIM_H_
#define MYTHEN_COM_SIM_H_

//TANGO
#include <tango.h>

// GLOBAL
#include "Config.h"

// LOCAL
#include "Com.h"


namespace Mythen2Detector_ns 
{

class ComSim : public Com
{
    public:
        explicit ComSim(Tango::DeviceImpl * dev);
        virtual ~ComSim();

        virtual void connect(const std::string & in_hostname, int in_port);
        virtual void disconnect(void);

    protected:
        virtual bool command(Com::ServerCmd   in_cmd_id      , 
                             const uint8_t  * in_send_buffer ,
                             int              in_send_lenght ,
                             uint8_t        * out_recv_buffer,
                             int              in_recv_lenght ,
                             int32_t        * out_error      ,
                             va_list          in_args        );

    private:
        template<typename T>
        void write_in_reception_buffer(T in_value, uint8_t * out_recv_buffer, int in_recv_lenght);

        void write_string_in_reception_buffer(const std::string & in_value, uint8_t * out_recv_buffer, int in_recv_lenght);

        template<typename T>
        void read_in_sent_buffer(T & out_value, const uint8_t * in_send_buffer, int in_send_lenght);

        template<typename T>
        void read_in_args(T & out_value, const std::string in_format, va_list in_args);

        template<typename T>
        void write_array_in_reception_buffer(std::vector<T> in_values, uint8_t * out_recv_buffer, int in_recv_lenght);

        /// Convert a delay in units of 100 ns to ms
        double convert_delay_100ns_units_to_ms(const int64_t & in_delay_100ns) const;

        /// Convert a delay in ms to units of 100 ns
        int64_t convert_delay_ms_to_100_ns_units(const double & in_delay_ms) const;

        int32_t generate_random_number(const int32_t min, const int32_t max);

        void slide_last_frame(void);

        std::vector<int32_t> compute_spectrum (const int32_t in_number_of_frames);

        std::vector<int32_t> compute_flatfield(void);

    private:
        int32_t m_nb_max_modules      ;
        int32_t m_nb_modules          ;
        int32_t m_selected_modules    ;
        int32_t m_all_modules_channels;
        int32_t m_status              ;
        int32_t m_frames_number       ;
        int32_t m_current_frame_index ;
        int32_t m_nb_bits             ;
        int32_t m_gates_nb            ;

        bool    m_cont_trigger_mode   ;
        bool    m_trigger_mode        ;
        bool    m_gate_measure_mode   ;

        bool    m_bad_chan_interp     ;
        bool    m_flatfield_correction;
        bool    m_rate_correction     ;

        bool    m_acquisition_running ;

        double  m_time_ms             ;
        double  m_delay_after_ms      ;
        double  m_delay_before_ms     ;
        
        std::vector<int32_t> m_modules_channels     ;
        std::vector<int32_t> m_modules_serial_number;
        std::vector<float>   m_modules_temperature  ;
        std::vector<int32_t> m_bad_channels         ;
        std::vector<float>   m_modules_humidity     ;
        std::vector<int32_t> m_modules_hv           ;
        std::vector<int64_t> m_readout_times        ;

        std::vector<int32_t> m_last_frame           ;
        std::vector<int32_t> m_flatfield            ;

        std::vector<float>   m_modules_energy       ;
        std::vector<float>   m_modules_energy_min   ;
        std::vector<float>   m_modules_energy_max   ;

        std::vector<float>   m_modules_threshold    ;
        std::vector<float>   m_modules_threshold_min;
        std::vector<float>   m_modules_threshold_max;
};

} // namespace Mythen2Detector_ns

#include "ComSim.hpp"

#endif // MYTHEN_COM_SIM_H_

//###########################################################################
