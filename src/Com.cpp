/*************************************************************************/
/*! 
 *  \file   Com.cpp
 *  \brief  mythen communication abstract class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "Com.h"

namespace Mythen2Detector_ns
{

/*******************************************************************/
/* Visual Studios 2012 and earlier don't have va_copy() */
#if defined(_MSC_VER) && _MSC_VER <= 1700
    #define va_copy(dest, src) ((dest) = (src))
#endif

/*******************************************************************
 * \brief constructor
 *******************************************************************/
Com::Com(Tango::DeviceImpl * dev) : Tango::LogAdapter(dev), m_device(dev)
{
    COM_DEBUG_STREAM << "Com::Com - [BEGIN]" << endl;

    // fill the error codes container
    Insert_new_errors(  0, "No error"                                             );
    Insert_new_errors( -1, "Unknown command"                                      );
    Insert_new_errors( -2, "Invalid argument"                                     );
    Insert_new_errors( -3, "Unknown settings"                                     );
    Insert_new_errors( -4, "Out of memory"                                        );
    Insert_new_errors( -5, "Module calibration files not found"                   );
    Insert_new_errors( -6, "Readout failed"                                       );
    Insert_new_errors( -7, "Acquisition not finished"                             );
    Insert_new_errors( -8, "Failure while reading temperature and humidity sensor");
    Insert_new_errors( -9, "Invalid license key"                                  );
    Insert_new_errors(-10, "Flatfield file not found"                             );
    Insert_new_errors(-11, "Bad channel file not found"                           );
    Insert_new_errors(-12, "Energy calibration file not found"                    );
    Insert_new_errors(-13, "Noise file not found"                                 );
    Insert_new_errors(-14, "Trimbit file not found"                               );
    Insert_new_errors(-15, "Invalid format of the flatfield file"                 );
    Insert_new_errors(-16, "Invalid format of the bad channel file"               );
    Insert_new_errors(-17, "Invalid format of the energy calibration file"        );
    Insert_new_errors(-18, "Invalid format of the noise file"                     );
    Insert_new_errors(-19, "Invalid format of the trimbit file"                   );
    Insert_new_errors(-20, "Version file not found"                               );
    Insert_new_errors(-21, "Invalid format of the version file"                   );
    Insert_new_errors(-22, "Gain calibration file not found"                      );
    Insert_new_errors(-23, "Invalid format of the gain calibration file"          );
    Insert_new_errors(-24, "Dead time file not found"                             );
    Insert_new_errors(-25, "Invalid format of the dead time file"                 );
    Insert_new_errors(-26, "High voltage file not found"                          );
    Insert_new_errors(-27, "Invalid format of high voltage file"                  );
    Insert_new_errors(-28, "Energy threshold relation file not found"             );
    Insert_new_errors(-29, "Invalid format of the energy threshold relation file" );
    Insert_new_errors(-30, "Could not create log file"                            );
    Insert_new_errors(-31, "Could not close log file"                             );
    Insert_new_errors(-32, "Could not read log file"                              );
    Insert_new_errors(-50, "No modules connected"                                 );
    Insert_new_errors(-51, "Error during module communication"                    );
    Insert_new_errors(-52, "DCS initialization failed"                            );
    Insert_new_errors(-53, "Could not store customer flatfield"                   );

    // application specific errors
    Insert_new_errors(NETWORK_ERROR, "Network communication error");
    
    // fill the server commands container
    // Table 1 - General commands
    insert_new_command(false, GET_ASSEMBLY_DATE       , "-get assemblydate"           , "Returns the assembly date of the system."                                                           );
    insert_new_command(false, GET_BAD_CHANNELS        , "-get badchannels"            , "Returns the status of each channel."                                                                );
    insert_new_command(false, GET_COMMAND_ID          , "-get commandid"              , "Returns the current command identifier."                                                            );
    insert_new_command(false, GET_COMMAND_SET_ID      , "-get commandsetid"           , "Returns the current set command identifier."                                                        );
    insert_new_command(true , GET_DCS_TEMPERATURE     , "-get dcstemperature"         , "Returns the temperature of the SoC die on the DCS4 in degrees Kelvin."                              );
    insert_new_command(true , GET_FRAMERATE_MAX       , "-get frameratemax"           , "Returns the maximum allowed frame rate for the active detector modules."                            );
    insert_new_command(false, GET_FW_VERSION          , "-get fwversion"              , "Returns the version string of the FPGA firmware running on the DCS4."                               );
    insert_new_command(true , GET_HUMIDITY            , "-get humidity"               , "Returns the relative humidity measured on each module."                                             );
    insert_new_command(false, GET_HV                  , "-get hv"                     , "Returns the high voltage at which the modules are operated."                                        );
    insert_new_command(false, GET_MOD_CHANNELS        , "-get modchannels"            , "Returns the number of channels for each module."                                                    );
    insert_new_command(false, GET_MOD_FW_VERSION      , "-get modfwversion"           , "Returns the concatenated version strings of the FPGA firmware running on the modules."              );
    insert_new_command(false, GET_MOD_NUM             , "-get modnum"                 , "Returns the serial numbers of the modules."                                                         );
    insert_new_command(false, GET_MODULE              , "-get module"                 , "Returns the selected module(s)."                                                                    );
    insert_new_command(false, GET_N_MAX_MODULES       , "-get nmaxmodules"            , "Returns the maximal number of modules that can be connected to this DCS4."                          );
    insert_new_command(false, GET_N_MODULES           , "-get nmodules"               , "Returns the number of active modules that are connected to DCS4."                                   );
    insert_new_command(false, GET_SENSOR_MATERIAL     , "-get sensormaterial"         , "Returns an identifier for the sensor material of each module."                                      );
    insert_new_command(false, GET_SENSOR_THICKNESS    , "-get sensorthickness"        , "Returns the nominal sensor thickness of each module in �m."                                         );
    insert_new_command(false, GET_SENSOR_WIDTH        , "-get sensorwidth"            , "Returns the nominal sensor width of each module in �m."                                             );
    insert_new_command(false, GET_SYSTEM_NUM          , "-get systemnum"              , "Returns the serial number of the MYTHEN2 system."                                                   );
    insert_new_command(true , GET_TEMPERATURE         , "-get temperature"            , "Returns the temperature of all active modules in degrees Kelvin."                                   );
    insert_new_command(false, GET_TEST_VERSION        , "-get testversion"            , "In case of a development version returns an additional identifier to the software version."         );
    insert_new_command(false, GET_VERSION             , "-get version"                , "Returns the software version of the socket server."                                                 );
    insert_new_command(false, SEL_MODULE              , "-module %d"                  , "Selects the module(s)."                                                                             );
    insert_new_command(false, SET_N_MODULES           , "-nmodules %d"                , "Sets the number of active modules. All modules are set back to default settings."                   );
    insert_new_command(false, CMD_RESET               , "-reset"                      , "Sets the detector back to default settings. A possibly running acquisition is stopped."             );

    // Table 2 - Acquisition control
    insert_new_command(false, SET_DEL_AFTER           , "-delafter %lld"              , "Sets the delay between two subsequent frames."                                                      );
    insert_new_command(false, SET_FRAMES              , "-frames %d"                  , "Sets the number of frames within an acquisition."                                                   );
    insert_new_command(false, GET_DEL_AFTER           , "-get delafter"               , "Returns the programmed delay between two subsequent frames."                                        );
    insert_new_command(false, GET_FRAMES              , "-get frames"                 , "Returns the number of frames."                                                                      );
    insert_new_command(false, GET_N_BITS              , "-get nbits"                  , "Returns the number of bits to be read out."                                                         );
    insert_new_command(false, GET_READ_OUT_TIMES      , "-get readouttimes"           , "Returns the readout times for the four different bit modes in units of 100 ns."                     );
    insert_new_command(false, GET_STATUS              , "-get status"                 , "Returns the status of the DCS4 as a bit pattern."                                                   );
    insert_new_command(false, GET_TIME                , "-get time"                   , "Returns the programmed exposure time of one frame in units of 100 ns."                              );
    insert_new_command(false, SET_N_BITS              , "-nbits %d"                   , "Sets the number of bits to be read out, thereby determining the dynamic range and the readout time.");
    insert_new_command(false, CMD_READ_OUT_ONE_FRAME  , "-readout"                    , "Returns the number of detected photons in each detector channel for one frame."                     );
    insert_new_command(false, CMD_READ_OUT_NB_FRAME   , "-readout %d"                 , "Returns the number of detected photons in each detector channel for several frames."                );
    insert_new_command(false, CMD_START               , "-start"                      , "Starts an acquisition with the programmed number of frames or gates."                               );
    insert_new_command(false, CMD_STOP                , "-stop"                       , "Stops the current acquisition."                                                                     );
    insert_new_command(false, SET_TIME                , "-time %lld"                  , "Sets the exposure time of one frame."                                                               );

    // Table 3 - Detector settings
    insert_new_command(false, SET_ENERGY              , "-energy %f"                  , "Sets the X-ray energy for the selected modules while keeping the current threshold energy."         );
    insert_new_command(true , GET_ENERGY              , "-get energy"                 , "Returns the X-ray energies of the selected modules."                                                );
    insert_new_command(true , GET_ENERGY_MAX          , "-get energymax"              , "Returns the maximum supported X-ray energy of the detector modules in units of keV."                );
    insert_new_command(true , GET_ENERGY_MIN          , "-get energymin"              , "Returns the minimum supported X-ray energy of the detector modules in units of keV."                );
    insert_new_command(true , GET_K_THRESH            , "-get kthresh"                , "Returns the threshold of the modules in keV."                                                       );
    insert_new_command(true , GET_K_THRESH_MAX        , "-get kthreshmax"             , "Returns the maximum supported threshold energy of the detector modules in units of keV."            );
    insert_new_command(true , GET_K_THRESH_MIN        , "-get kthreshmin"             , "Returns the minimum supported threshold energy of the detector modules in units of keV."            );
    insert_new_command(false, SET_K_THRESH            , "-kthresh %f"                 , "Sets the energy threshold for the selected modules while keeping the current X-ray energy."         );
    insert_new_command(false, SET_K_THRESH_ENERGY     , "-kthreshenergy %f %f"        , "Sets the energy threshold and the X-ray energy for the selected modules."                           );
    insert_new_command(false, CMD_LOAD_SETTINGS_CU    , "-settings Cu"                , "Loads predefined settings for the selected modules and the specified X-ray radiation (Cu)."         );
    insert_new_command(false, CMD_LOAD_SETTINGS_MO    , "-settings Mo"                , "Loads predefined settings for the selected modules and the specified X-ray radiation (Mo)."         );
    insert_new_command(false, CMD_LOAD_SETTINGS_CR    , "-settings Cr"                , "Loads predefined settings for the selected modules and the specified X-ray radiation (Cr)."         );
    insert_new_command(false, CMD_LOAD_SETTINGS_AG    , "-settings Ag"                , "Loads predefined settings for the selected modules and the specified X-ray radiation (Ag)."         );

    // Table 4 - Data correction commands
    insert_new_command(false, SET_BAD_CHAN_INTERP     , "-badchannelinterpolation %d" , "Turns on or off the interpolation routine for bad channels."                                        );
    insert_new_command(false, SET_FLATFIELD           , "-flatfield %d "              , "Stores a customer specific flatfield to the given slot."                                            ); // the binary block is concatened
    insert_new_command(false, SET_FLATFIELD_CORRECTION, "-flatfieldcorrection %d"     , "Enables or disables the flatfield correction."                                                      );
    insert_new_command(false, GET_BAD_CHAN_INTERP     , "-get badchannelinterpolation", "Returns whether the bad channel interpolation is enabled."                                          );
    insert_new_command(false, GET_CUT_OFF             , "-get cutoff"                 , "Returns the maximal count value before flatfield correction."                                       );
    insert_new_command(false, GET_FLATFIELD           , "-get flatfield"              , "Returns the currently loaded flatfield calibration of all modules."                                 );
    insert_new_command(false, GET_FLATFIELD_CORRECTION, "-get flatfieldcorrection"    , "Returns whether the flatfield correction is enabled."                                               );
    insert_new_command(false, GET_RATE_CORRECTION     , "-get ratecorrection"         , "Returns whether the rate correction is enabled."                                                    );
    insert_new_command(true , GET_TAU                 , "-get tau"                    , "Returns the dead time constant used by the rate correction for all active modules in units of ns."  );
    insert_new_command(false, CMD_LOAD_FLATFIELD      , "-loadflatfield %d"           , "Activates a customer specific flatfield stored in the given slot."                                  );
    insert_new_command(false, SET_RATE_CORRECTION     , "-ratecorrection %d"          , "Enables or disables the rate correction."                                                           );
    insert_new_command(false, SET_TAU                 , "-tau %f"                     , "Sets the dead time constant (used by the rate correction) for the selected modules."                );

    // Table 5 - Trigger and Gating control commands
    insert_new_command(false, SET_CONT_TRIGGER_MODE   , "-conttrigen %d"              , "Enables or disables the continuous trigger mode."                                                   );
    insert_new_command(false, SET_DEL_BEFORE          , "-delbef %lld"                , "Sets the delay between a trigger signal and the start of the measurement."                          );
    insert_new_command(false, SET_GATED_MEASURE_MODE  , "-gateen %d"                  , "Enables or disables the gated measurement mode."                                                    );
    insert_new_command(false, SET_GATES_NB            , "-gates %d"                   , "Sets the number of gate signals within one frame."                                                  );
    insert_new_command(false, GET_CONT_TRIGGER_MODE   , "-get conttrig"               , "Returns whether the continuous trigger mode is enabled."                                            );
    insert_new_command(false, GET_DEL_BEFORE          , "-get delbef"                 , "Returns the delay between a trigger signal and the start of the measurement."                       );
    insert_new_command(false, GET_GATED_MEASURE_MODE  , "-get gate"                   , "Returns whether the gated mode is enabled"                                                          );
    insert_new_command(false, GET_GATES_NB            , "-get gates"                  , "Returns the number of gates."                                                                       );
    insert_new_command(false, GET_TRIGGER_MODE        , "-get trig"                   , "Returns whether the single trigger mode is enabled."                                                );
    insert_new_command(false, SET_TRIGGER_MODE        , "-trigen %d"                  , "Enables or disables the single trigger mode."                                                       );

    // Table 6 - Debugging commands
    insert_new_command(false, CMD_START_LOG           , "-log start"                  , "Instructs the socket server to log its activities into a file on the DCS4."                         );
    insert_new_command(false, GET_LOG_STATUS          , "-log status"                 , "Returns whether the logging is running."                                                            );
    insert_new_command(false, CMD_STOP_LOG            , "-log stop"                   , "Stops the logging functionality of the socket server. The command returns the size of the log file.");
    insert_new_command(false, CMD_GET_LOG             , "-log read"                   , "Instructs the socket server to send back the content of the log file."                              );
    insert_new_command(false, CMD_TEST_DATA_SET       , "-testpattern"                , "Returns a test data set with the number of counts for each channel equal to the channel number."    );

    m_connected = false;

    COM_DEBUG_STREAM << "Com::Com - [END]" << endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Com::~Com() 
{
}

/*******************************************************************
 * \brief Fill the commands container
 *******************************************************************/
void Com::insert_new_command(bool in_FloatReturnType, ServerCmd in_CmdId, std::string in_cmd, std::string in_description)
{
    m_server_cmds.insert(ServerCmdPair(in_CmdId, Cmd(in_cmd, in_description, in_FloatReturnType)));
}

/*******************************************************************
 * \brief Fill the errors container
 *******************************************************************/
void Com::Insert_new_errors(int32_t in_ErrorId, std::string in_description)
{
    m_error_codes.insert(ErrorCodePair(in_ErrorId, in_description));
}

/*******************************************************************
 * \brief Get the error string
 *******************************************************************/
std::string Com::get_error_string(int32_t in_ErrorId)
{
    ErrorCodeMap::iterator it;

	if ((it = m_error_codes.find(in_ErrorId)) != m_error_codes.end()) 
    {
		return(it->second);
	} 
    else 
    {
        std::ostringstream MsgErr;

        MsgErr << "Unknown server error:" << in_ErrorId <<std::endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "Com::get_error_string()");
        return("Unknown server error"); // never be called - to avoid a warning
	}
}

/*******************************************************************
 * \brief Get the command string
 *******************************************************************/
std::string Com::get_command_string(ServerCmd in_CmdId)
{
    ServerCmdMap::iterator it;

	if ((it = m_server_cmds.find(in_CmdId)) != m_server_cmds.end()) 
    {
		Cmd CmdObj = it->second;
        return CmdObj.get_command_string();
	} 
    else 
    {
        std::ostringstream MsgErr;

        MsgErr << "Unknown server cmd:" << in_CmdId <<std::endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "Com::get_command_string()");
        return("Exception"); // never be called - to avoid a warning
	}
}

/*******************************************************************
 * \brief Get the command description
 *******************************************************************/
std::string Com::get_command_description(ServerCmd in_CmdId)
{
    ServerCmdMap::iterator it;

	if ((it = m_server_cmds.find(in_CmdId)) != m_server_cmds.end()) 
    {
		Cmd CmdObj = it->second;
        return CmdObj.get_command_description();
	} 
    else 
    {
        std::ostringstream MsgErr;

        MsgErr << "Unknown server cmd:" << in_CmdId <<std::endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "Com::get_command_description()");
        return("Exception"); // never be called - to avoid a warning
	}
}

/*******************************************************************
 * \brief Check if the return type of the command is a float
 *******************************************************************/
bool Com::is_float_return_type_for_command(ServerCmd in_CmdId)
{
    ServerCmdMap::iterator it;

	if ((it = m_server_cmds.find(in_CmdId)) != m_server_cmds.end()) 
    {
		Cmd CmdObj = it->second;
        return CmdObj.is_float_return_type();
	} 
    else 
    {
        std::ostringstream MsgErr;

        MsgErr << "Unknown server cmd:" << in_CmdId <<std::endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "Com::is_float_return_type_for_command()");
        return(false); // never be called - to avoid a warning
	}
}

/************************************************************************
 * \brief fill a standard string with printf format (va_list way)
 * @param in_format      printf format string
 * @param in_args        optional args
 ************************************************************************/
std::string Com::string_format_arg(const std::string & in_format, 
                                   va_list             in_args  )
{
    std::string result_string;

    // managing the args (printf style)
    if(in_args != NULL)
    {
        va_list     args_copy;
        int         size     ;

        va_copy( args_copy, in_args ); // we make a copy to avoid memory problems
        size = vsnprintf(NULL, 0, in_format.c_str(), args_copy); 
        va_end(args_copy);

        std::string temp(++size, 0);
        vsnprintf((char*)temp.data(), size, in_format.c_str(), in_args);

        // removing the '\0' char
        temp.resize(size-1);

        result_string = temp;
    }
    else
    {
        result_string = in_format;
    }

    return result_string;
}

/*******************************************************************
 * \brief throw an exception
 * @param reason The exception DevError object reason field
 * @param desc   The exception DevError object desc   field
 * @param origin The exception DevError object origin field
 *******************************************************************/
void Com::throw_tango_exception(const char * reason,
                                const char * desc  ,
                                const char * origin)
{
#ifdef MYTHEN_TANGO_INDEPENDANT
    COM_ERROR_STREAM << "EXCEPTION:" << reason << " - " << desc << " - " << origin << endl;

    throw std::string( "throw_tango_exception" ); 
#else
    Tango::Except::throw_exception(reason, desc, origin);
#endif
}

/*******************************************************************
 * \brief Send a set command to the DCS and receive its response
 *******************************************************************/
bool Com::set_command(Com::ServerCmd in_CmdId, int32_t * out_Error, ...) 
{
    va_list args  ;
    bool    result;

    va_start(args, out_Error);
    result = command(in_CmdId, NULL, 0, NULL, 0, out_Error, args);
    va_end(args);

    return result;
}

/*******************************************************************
 * \brief Send an action command to the DCS and receive its response
 * No returned values version.
 *******************************************************************/
// Using reference 'out_Error' as parameter for va_start() results in undefined behaviour, so I added in_dummy parameter.
bool Com::action_command(Com::ServerCmd in_CmdId, int32_t * out_Error, ...) 
{
    va_list args  ;
    bool    result;

    va_start(args, out_Error);
    result = command(in_CmdId, NULL, 0, NULL, 0, out_Error, args);
    va_end(args);

    return result;
}

/*******************************************************************
 * \brief Send a command to the DCS passing a binary block and 
 *        receive its response
 *        data array returned version
 *******************************************************************/
// Using reference 'out_Error' as parameter for va_start() results in undefined behaviour, so I added in_dummy parameter.
bool Com::binary_command(Com::ServerCmd in_CmdId, const uint8_t * in_Buffer, int in_Length, int32_t * out_Error, ...) 
{
    va_list args  ;
    bool    result;

    va_start(args, out_Error);
    result = command(in_CmdId, in_Buffer, in_Length, NULL, 0, out_Error, args);
    va_end(args);

    return result;
}

//###########################################################################
}