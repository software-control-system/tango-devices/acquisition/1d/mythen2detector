/*************************************************************************/
/*! 
 *  \file   ComNet.h
 *  \brief  Socket communication class
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef MYTHEN_COM_NET_H_
#define MYTHEN_COM_NET_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>

// GLOBAL
#include "Config.h"

// LOCAL
#include "Com.h"

// SYSTEM
#include <netinet/in.h>


namespace Mythen2Detector_ns 
{

class ComNet : public Com 
{
    public:
        explicit ComNet(Tango::DeviceImpl * dev);
        virtual ~ComNet();

        virtual void connect(const std::string & in_hostname, int in_port);
        virtual void disconnect(void);

    protected:
        virtual bool command(Com::ServerCmd   in_cmd_id      , 
                             const uint8_t  * in_send_buffer ,
                             int              in_send_lenght ,
                             uint8_t        * out_recv_buffer,
                             int              in_recv_lenght ,
                             int32_t        * out_error      ,
                             va_list          in_args        );

        bool net_send(const std::string          & in_Command   ,
                      const std::vector<uint8_t> & in_net_buffer,
                      int32_t                    * out_error    );

        bool net_receive(uint8_t * out_recvBuf, int in_recvLength, int32_t * out_Error, bool in_isFloatType);

    private:
        yat::Mutex         m_network_lock;
        int                m_sock        ; // socket for commands */
        struct sockaddr_in m_server_name ; // address of remote server */
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_COM_NET_H_
