/*************************************************************************/
/*! 
 *  \file   Stream.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_STREAM_H_
#define MYTHEN_STREAM_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>

// LOCAL
#include "AcquisitionData.h"

namespace Mythen2Detector_ns
{
/*************************************************************************/
/// the masks used to manage the stream items 
static const uint32_t STREAM_ITEM_FRAME = 0x00000001; // to activate the frame data in the stream

/*************************************************************************/
/// we can not include controller header because DataTask and Controller 
/// are interdependant.
/// Controller uses methods of DataTask to add new data
/// DataTask uses methods of Controller to give data to the stream manager.
class Controller;

/*************************************************************************/
class Stream : public Tango::LogAdapter
{
    public:
        // TYPES
        enum Types 
        { 
            NO_STREAM   = 0,
            LOG_STREAM  ,
            CSV_STREAM  ,
            NEXUS_STREAM,
        };

        // WRITE MODES
        enum WriteModes 
        { 
            ASYNCHRONOUS = 0,
            SYNCHRONOUS     ,
            DELAYED         ,
            UNDEFINED       ,
        };

    public:
        /// constructor
        Stream(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~Stream();

        /// get the stream type
        virtual enum Stream::Types get_type() = 0;

        /// Update the stream with new acquisition data
        virtual void update(const AcquisitionData & in_acquisition_data) = 0;

        /// Reset the file index
        virtual void reset_index(void) = 0;

        /// Init the stream after a new configuration
        virtual void init(void);

        /// Close
        virtual void close(void);

        ///----------------------------------------------------------------------------------
        /// Setter of the state
        void set_state(Tango::DevState state);

        /// Getter of the state
        Tango::DevState get_state(void);

        /// Setter of the status
        void set_status(const std::string& status);

        /// Getter of the status
        std::string get_status(void);

        ///----------------------------------------------------------------------------------
        /// Get the target path
        std::string get_target_path(void);

        /// Get the target file
        std::string get_target_file(void); 

        /// Get the number of data per acquisition
        uint32_t get_nb_data_per_acq(void);

        /// Get the number of acquisition per file
        uint32_t get_nb_acq_per_file(void); 

        /// Get the write mode
        enum Stream::WriteModes get_write_mode(void);

        /// Get the items configuration (bit field)
        uint32_t get_items(void);

        /// Set the target path
        void set_target_path(const std::string & in_target_path); 

        /// Set the target file
        void set_target_file(const std::string & in_target_file); 

        /// Set the number of data per acquisition
        void set_nb_data_per_acq(const uint32_t & in_nb_data_per_acq); 

        /// Set the number of acquisition per file
        void set_nb_acq_per_file(const uint32_t & in_nb_acq_per_file); 

        /// Set the write mode
        void set_write_mode(const enum Stream::WriteModes & in_write_mode);

        /// Set the items configuration (bit field)
        void set_items(const uint32_t & in_items);

        /// Get the data stream string
        std::string get_data_streams(void);
    
    protected:
        /// Manage the FAULT status with an exception
        void on_fault(Tango::DevFailed df);

        /// Manage the FAULT status with an error message
        void on_fault(const std::string& status_message);

    private:
        /// Compute the state and status 
        void compute_state_status();

    protected:
	    std::string         m_target_path    ;
        std::string         m_target_file    ;
	    uint32_t            m_nb_data_per_acq;
	    uint32_t            m_nb_acq_per_file;
        WriteModes          m_write_mode     ;
        uint32_t            m_items          ; // activation bit field

        Tango::DevState     m_state     ; /// state
        std::stringstream   m_status    ; /// status
        yat::Mutex          m_state_lock; /// mutex used to protect state access

        Controller        * m_controller; /// Controller object used for calling methods to abort the acquisition

    private :
        /// Owner Device server object
        Tango::DeviceImpl * m_device;
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_H_

//###########################################################################
