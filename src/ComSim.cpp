/*************************************************************************/
/*! 
 *  \file   ComSim.cpp
 *  \brief  mythen communication simulator class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "ComSim.h"

#include <netdb.h>
#include <unistd.h>
#include <limits>

namespace Mythen2Detector_ns
{

/*******************************************************************
 * \brief constructor
 *******************************************************************/
ComSim::ComSim(Tango::DeviceImpl * dev) : Com(dev)
{
    COM_DEBUG_STREAM << "ComSim::ComSim" << endl;

    int32_t module_index ;
    int32_t channel_index;

    m_selected_modules     = SELECT_ALL_MODULES;
    m_nb_max_modules       = 4;
    m_nb_modules           = 4;
    m_all_modules_channels = 0;

    m_modules_channels.resize     (m_nb_modules);
    m_modules_serial_number.resize(m_nb_modules);
    m_modules_temperature.resize  (m_nb_modules);
    m_modules_humidity.resize     (m_nb_modules);
    m_modules_hv.resize           (m_nb_modules);

    m_modules_energy.resize       (m_nb_modules);
    m_modules_energy_min.resize   (m_nb_modules);
    m_modules_energy_max.resize   (m_nb_modules);

    m_modules_threshold.resize    (m_nb_modules);
    m_modules_threshold_min.resize(m_nb_modules);
    m_modules_threshold_max.resize(m_nb_modules);

    for(module_index = 0 ; module_index < m_nb_modules ; module_index++)
    {
        m_modules_channels     [module_index] = 1280;
        m_modules_serial_number[module_index] = 0x12345678 + module_index;
        m_modules_temperature  [module_index] = 310.65f    + module_index; // 37.5� celcius
        m_modules_humidity     [module_index] = 60.5f;
        m_modules_hv           [module_index] = 250  ;

        m_modules_energy       [module_index] = 8.05f;
        m_modules_energy_min   [module_index] = 7.39f;
        m_modules_energy_max   [module_index] = 40.0f;

        m_modules_threshold    [module_index] = 6.4f ;
        m_modules_threshold_min[module_index] = 6.0f ;
        m_modules_threshold_max[module_index] = 20.0f;

        m_all_modules_channels += m_modules_channels[module_index];
    }

    m_bad_channels.resize(m_all_modules_channels);

    int32_t first_channel_index = 0;

    for(module_index = 0 ; module_index < m_nb_modules ; module_index++)
    {
        for(channel_index = 0 ;  channel_index < m_modules_channels[module_index] ; channel_index++)
        {
            int32_t status = ((channel_index == 0) || (channel_index == (m_modules_channels[module_index] - 1))) ? 1 : 0;

            m_bad_channels[first_channel_index + channel_index] = status;
        }

        first_channel_index += m_modules_channels[module_index];
    }

    m_status              = STATUS_NO_DATA_FOR_READOUT;
    m_frames_number       = 1;
    m_current_frame_index = 0;

    m_cont_trigger_mode    = false;
    m_trigger_mode         = true ;
    m_gate_measure_mode    = false;
    m_nb_bits              = 16   ;
    m_gates_nb             = 1    ;
    m_time_ms              = 1000 ;
    m_delay_after_ms       = 0    ;
    m_delay_before_ms      = 0    ;
    m_bad_chan_interp      = true ;
    m_flatfield_correction = false;
    m_rate_correction      = false;
    m_acquisition_running  = false;

    m_readout_times.resize(4);

    m_readout_times[0] = convert_delay_ms_to_100_ns_units(0.890);
    m_readout_times[1] = convert_delay_ms_to_100_ns_units(0.890);
    m_readout_times[2] = convert_delay_ms_to_100_ns_units(0.890);
    m_readout_times[3] = convert_delay_ms_to_100_ns_units(0.890);

    srand(time(NULL)); // initialize random number generator

    // compute the first frame
    m_last_frame.resize(m_all_modules_channels);

    int32_t goal_number = generate_random_number(0, std::numeric_limits<int32_t>::max());

    m_last_frame[0] = 0;

    for(channel_index = 1 ;  channel_index < m_all_modules_channels ; channel_index++)
    {
        // another goal number
        if(m_last_frame[channel_index - 1] == goal_number)
        {
            goal_number = generate_random_number(0, std::numeric_limits<int32_t>::max());
        }

        int32_t distance = (goal_number - m_last_frame[channel_index - 1]);
        if(abs(distance) > 1)
        {
            distance /= 2;
        }

        m_last_frame[channel_index] = m_last_frame[channel_index - 1] + distance;
    }

    // flatfield by default
    m_flatfield.resize(m_all_modules_channels);
    m_flatfield = m_last_frame;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
ComSim::~ComSim() 
{
    COM_DEBUG_STREAM << "ComSim::~ComSim - [BEGIN]" << endl;
    disconnect();
    COM_DEBUG_STREAM << "ComSim::~ComSim - [END]" << endl;
}

/*******************************************************************
 * \brief Connection to Mythen tcpip server
 *******************************************************************/
void ComSim::connect(const std::string & in_hostname, int in_port) 
{
    COM_INFO_STREAM << "ComSim::connect -> " << in_hostname << ":" << in_port << endl;

	if (m_connected) 
    {
        std::ostringstream MsgErr;

        MsgErr << "Already connected to server" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::connect()");
	}

	m_connected = true;
}

/*******************************************************************
 * \brief Disconnection from Mythen tcpip server
 *******************************************************************/
void ComSim::disconnect(void) 
{
    COM_INFO_STREAM << "ComSim::disconnect" << endl;
	m_connected = false;
}

/************************************************************************
 * \brief Send a command to the DCS and receive its response
 * @param in_cmd_id       command id
 * @param in_send_buffer  optional binary buffer which will be concatened 
 * @param in_send_lenght  optional binary buffer lenght in bytes
 * @param out_recv_buffer optional response buffer
 * @param in_recv_lenght  expected size of the response
 * @param out_error       error value
 * @param in_args         optional arguments
 ************************************************************************/
bool ComSim::command(Com::ServerCmd   in_cmd_id      ,
                     const uint8_t  * in_send_buffer ,
                     int              in_send_lenght ,
                     uint8_t        * out_recv_buffer,
                     int              in_recv_lenght ,
                     int32_t        * out_error      ,
                     va_list          in_args        )
{
    COM_DEBUG_STREAM << "ComSim::command(" << in_cmd_id << ") : " << Com::get_command_description(in_cmd_id) << endl;
	
	if (!m_connected) 
    {
        std::ostringstream MsgErr;

        MsgErr << "not connected!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::command()");
	}

    // no error by default
    *out_error = NO_ERROR;

    switch (in_cmd_id)
    {
        //----------------------------------------------------------------------------
        case ServerCmd::GET_N_MAX_MODULES:
        {
            int32_t value = m_nb_max_modules;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_N_MODULES:
        {
            int32_t value = m_nb_modules;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_MOD_CHANNELS:
        {
            write_array_in_reception_buffer(m_modules_channels, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_MOD_NUM:
        {
            write_array_in_reception_buffer(m_modules_serial_number, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_ASSEMBLY_DATE:
        {
            std::string value = "12345678911234567892123456789312345678941234567895";
            write_string_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_VERSION:
        {
            std::string value = "M3.0.0";
            write_string_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_FW_VERSION:
        {
            std::string value = "01.03.00";
            write_string_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_DCS_TEMPERATURE:
        {
            float value = 303.15f; // 30� celcius
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_FRAMERATE_MAX:
        {
            float value = 1000.0f; 
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_RESET:
        {
            sleep(1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SEL_MODULE:
        {
            read_in_args(m_selected_modules, "%d", in_args);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_MODULE:
        {
            write_in_reception_buffer(m_selected_modules, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_TEMPERATURE:
        {
            write_array_in_reception_buffer(m_modules_temperature, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_BAD_CHANNELS:
        {
            write_array_in_reception_buffer(m_bad_channels, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_HUMIDITY:
        {
            write_array_in_reception_buffer(m_modules_humidity, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_HV:
        {
            write_array_in_reception_buffer(m_modules_hv, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_MOD_FW_VERSION:
        {
            std::string value       ;
            int32_t     module_index;

            for(module_index = 0 ; module_index < m_nb_modules ; module_index++)
            {
                value += "01.04.00";
            }

            write_string_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_SYSTEM_NUM:
        {
            int32_t value = 666;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_STATUS:
        {
            m_status = 0;
            
            if(m_acquisition_running)
            {
                m_status |= STATUS_ACQUISITION_RUNNING;
                
                if(m_current_frame_index >= m_frames_number)
                {
                    m_status |= STATUS_NO_DATA_FOR_READOUT;
                }
            }
            else
            {
                m_status = STATUS_NO_DATA_FOR_READOUT;
            }
            
            write_in_reception_buffer(m_status, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_FRAMES:
        {
            write_in_reception_buffer(m_frames_number, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_FRAMES:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_frames_number = value;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_CONT_TRIGGER_MODE:
        {
            int32_t value = m_cont_trigger_mode ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_TRIGGER_MODE:
        {
            int32_t value = m_trigger_mode ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_GATED_MEASURE_MODE:
        {
            int32_t value = m_gate_measure_mode ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_CONT_TRIGGER_MODE:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_cont_trigger_mode = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_TRIGGER_MODE:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_trigger_mode = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_GATED_MEASURE_MODE:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_gate_measure_mode = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_N_BITS:
        {
            write_in_reception_buffer(m_nb_bits, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_N_BITS:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);

            if((value != 4) && (value != 8) && (value != 16) && (value != 32))
            {
                *out_error = -2; // bad argument
            }
            else
            {
                m_nb_bits = value;
            }
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_TIME:
        {
            int64_t value = convert_delay_ms_to_100_ns_units(m_time_ms);
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_DEL_AFTER:
        {
            int64_t value = convert_delay_ms_to_100_ns_units(m_delay_after_ms);
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_DEL_BEFORE:
        {
            int64_t value = convert_delay_ms_to_100_ns_units(m_delay_before_ms);
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_TIME:
        {
            int64_t value;
            read_in_args(value, "%lld", in_args);
            m_time_ms = convert_delay_100ns_units_to_ms(value);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_DEL_AFTER:
        {
            int64_t value;
            read_in_args(value, "%lld", in_args);
            m_delay_after_ms = convert_delay_100ns_units_to_ms(value);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_DEL_BEFORE:
        {
            int64_t value;
            read_in_args(value, "%lld", in_args);
            m_delay_before_ms = convert_delay_100ns_units_to_ms(value);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_READ_OUT_TIMES:
        {
            write_array_in_reception_buffer(m_readout_times, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_GATES_NB:
        {
            write_in_reception_buffer(m_gates_nb, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_GATES_NB:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_gates_nb = value;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_ENERGY:
        {
            float   value       ;
            int32_t module_index;

            read_in_args(value, "%f", in_args);

            for(module_index = 0 ; module_index < m_nb_modules ; module_index++)
            {
                if((m_selected_modules == SELECT_ALL_MODULES) || (module_index == m_selected_modules))
                {
                    m_modules_energy[module_index] = value;
                }
            }
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_K_THRESH:
        {
            float   value       ;
            int32_t module_index;

            read_in_args(value, "%f", in_args);

            for(module_index = 0 ; module_index < m_nb_modules ; module_index++)
            {
                if((m_selected_modules == SELECT_ALL_MODULES) || (module_index == m_selected_modules))
                {
                    m_modules_threshold[module_index] = value;
                }
            }
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_ENERGY:
        {
            write_array_in_reception_buffer(m_modules_energy, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_K_THRESH:
        {
            write_array_in_reception_buffer(m_modules_threshold, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_ENERGY_MIN:
        {
            write_array_in_reception_buffer(m_modules_energy_min, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_K_THRESH_MIN:
        {
            write_array_in_reception_buffer(m_modules_threshold_min, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_ENERGY_MAX:
        {
            write_array_in_reception_buffer(m_modules_energy_max, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_K_THRESH_MAX:
        {
            write_array_in_reception_buffer(m_modules_threshold_max, out_recv_buffer, in_recv_lenght);
        }
        break;


        //----------------------------------------------------------------------------
        case ServerCmd::SET_BAD_CHAN_INTERP:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_bad_chan_interp = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_BAD_CHAN_INTERP:
        {
            int32_t value = m_bad_chan_interp ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_FLATFIELD_CORRECTION:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_flatfield_correction = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_FLATFIELD_CORRECTION:
        {
            int32_t value = m_flatfield_correction ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_RATE_CORRECTION:
        {
            int32_t value;
            read_in_args(value, "%d", in_args);
            m_rate_correction = (value == 1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_RATE_CORRECTION:
        {
            int32_t value = m_rate_correction ? 1 : 0;
            write_in_reception_buffer(value, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_START:
        {
            std::cout << "======== CMD_START ========" << std::endl;

            m_current_frame_index = 0   ;
            m_acquisition_running = true;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_STOP:
        {
            std::cout << "======== CMD_STOP ========" << std::endl;

            m_current_frame_index = 0    ;
            m_acquisition_running = false;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_READ_OUT_ONE_FRAME:
        {
            std::vector<int32_t> spectrum = compute_spectrum(1);
            
            write_array_in_reception_buffer(spectrum, out_recv_buffer, in_recv_lenght);

            m_current_frame_index++;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_READ_OUT_NB_FRAME:
        {
            int32_t value; // number of frames
            read_in_args(value, "%d", in_args);

            std::vector<int32_t> spectrum = compute_spectrum(value);

            write_array_in_reception_buffer(spectrum, out_recv_buffer, in_recv_lenght);

            m_current_frame_index += value;
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::CMD_LOAD_SETTINGS_CU:
        case ServerCmd::CMD_LOAD_SETTINGS_MO:
        case ServerCmd::CMD_LOAD_SETTINGS_CR:
        case ServerCmd::CMD_LOAD_SETTINGS_AG:
        {
            sleep(1);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::GET_FLATFIELD:
        {
            std::vector<int32_t> spectrum = compute_flatfield();
            
            write_array_in_reception_buffer(spectrum, out_recv_buffer, in_recv_lenght);
        }
        break;

        //----------------------------------------------------------------------------
        case ServerCmd::SET_FLATFIELD:
        case ServerCmd::CMD_LOAD_FLATFIELD:
        {
        }
        break;

        //----------------------------------------------------------------------------
        default:
        {
            COM_ERROR_STREAM << "ComSim::command : This command (" << in_cmd_id << ") is not implemented by the simulator!" << endl;

            std::ostringstream MsgErr;

            MsgErr << "command not implemented in simulator!" << endl;
            throw_tango_exception("LOGIC_ERROR",
                                  MsgErr.str().c_str(),
                                  "ComSim::command()");
        }
        break;
    }

    return true;
}

/*******************************************************************
 * \brief Write a char string value in the receive buffer
 *******************************************************************/
void ComSim::write_string_in_reception_buffer(const std::string & in_value, uint8_t * out_recv_buffer, int in_recv_lenght)
{
    std::size_t in_value_size = in_value.length();

    // check if we need to remove or let the end of string char
    if(static_cast<std::size_t>(in_recv_lenght) == (in_value_size + 1))
    {
        in_value_size++;
    }
    
    if(static_cast<std::size_t>(in_recv_lenght) != in_value_size)
    {
        COM_ERROR_STREAM << "ComSim::write_in_reception_buffer : size problem! " 
                         << "value size ("  << in_value_size  << ") " 
                         << "buffer size (" << in_recv_lenght << ")" 
                         << endl;

        std::ostringstream MsgErr;

        MsgErr << "bad buffer size!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComSim::write_in_reception_buffer()");
	}
    
    memcpy(out_recv_buffer, in_value.c_str(), in_value_size );
}

/*******************************************************************
 * \brief Convert a delay in units of 100 ns to ms
 * \param[in]  in_delay_100ns delay in units of 100 ns
 * \return delay in ms
 *******************************************************************/
double ComSim::convert_delay_100ns_units_to_ms(const int64_t & in_delay_100ns) const
{
    // 1ms = 10000 units of 100ns
    double delay_ms = static_cast<double>(in_delay_100ns) / 10000.0;
    return delay_ms;
}

/*******************************************************************
 * \brief Convert a delay in ms to units of 100 ns
 * \param[in]  in_delay_ms delay in ms
 * \return delay in ms
 *******************************************************************/
int64_t ComSim::convert_delay_ms_to_100_ns_units(const double & in_delay_ms) const
{
    // 1ms = 10000 units of 100ns
    int64_t delay_100ns = static_cast<int64_t>(in_delay_ms * 10000LL);
    return delay_100ns;
}

/*******************************************************************
 * \brief generate a random number
 *******************************************************************/
int32_t ComSim::generate_random_number(const int32_t min, const int32_t max)
{
    int32_t n = (rand() % (max - min + 1)) + min;
    return n;
}

/*******************************************************************
 * \brief slide the content of the last frame vecor
 *******************************************************************/
void ComSim::slide_last_frame(void)
{
    int32_t channel_index;
    int32_t first_value  ;

    first_value = m_last_frame[0];

    for(channel_index = 1 ;  channel_index < m_all_modules_channels ; channel_index++)
    {
        m_last_frame[channel_index - 1] = m_last_frame[channel_index];
    }

    m_last_frame[m_all_modules_channels - 1] = first_value;
}

/*******************************************************************
 * \brief compute the spectrum for several frames
 *******************************************************************/
std::vector<int32_t> ComSim::compute_spectrum(const int32_t in_number_of_frames)
{
    std::vector<int32_t> spectrum     ;
    int32_t              frame_index  ;
    int32_t              channel_index;
    int32_t              first_channel_index = 0;

    spectrum.resize(in_number_of_frames * m_all_modules_channels);

    for(frame_index = 0 ; frame_index < in_number_of_frames ; frame_index++)
    {
        // slide the values
        slide_last_frame();

        for(channel_index = 0 ; channel_index < m_all_modules_channels ; channel_index++)
        {
            int32_t value = m_last_frame[channel_index] >> (32 - m_nb_bits); // compute the precision
            spectrum[first_channel_index + channel_index] = value;
        }

        first_channel_index += m_all_modules_channels;
    }

    return spectrum;
}

/*******************************************************************
 * \brief compute the flatfield
 *******************************************************************/
std::vector<int32_t> ComSim::compute_flatfield(void)
{
    std::vector<int32_t> spectrum     ;
    int32_t              channel_index;

    spectrum.resize(m_all_modules_channels);

    for(channel_index = 0 ; channel_index < m_all_modules_channels ; channel_index++)
    {
        int32_t value = m_flatfield[channel_index] >> (32 - m_nb_bits); // compute the precision
        spectrum[channel_index] = value;
    }

    return spectrum;
}

//###########################################################################
}