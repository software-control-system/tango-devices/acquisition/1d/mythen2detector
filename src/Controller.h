/*************************************************************************/
/*! 
 *  \file   Controller.h
 *  \brief  Interface class which calls Task methods.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_CONTROLLER_H_
#define MYTHEN_CONTROLLER_H_

// TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>

// PROJECT
#include "CollectTask.h"
#include "DataTask.h"
#include "Com.h"
#include "Stream.h"

/*************************************************************************/

namespace Mythen2Detector_ns
{
    //-------------------------------------------------------------------------
    // TRIGGERS
    //-------------------------------------------------------------------------
    // labels of trigger modes in the TANGO device
    static const std::vector<string> TANGO_TRIGGER_MODES_LABELS{"INTERNAL", "EXTERNAL_SINGLE", "EXTERNAL_MULTIPLE", "EXTERNAL_GATE" };

    static const size_t TANGO_TRIGGER_MODES_LABELS_SIZE_MAX = 18; // label size max

    // used to convert a tango trigger mode (from TANGO_TRIGGER_MODES_LABELS) to a hardware trigger mode
    static const std::vector<enum Com::TriggerMode> TANGO_TO_HARDWARE_TRIGGER_MODE{Com::TriggerMode::TRIGGER_INTERNAL         ,
                                                                                   Com::TriggerMode::TRIGGER_EXTERNAL_SINGLE  ,
                                                                                   Com::TriggerMode::TRIGGER_EXTERNAL_MULTIPLE,
                                                                                   Com::TriggerMode::TRIGGER_EXTERNAL_GATE    };

    //-------------------------------------------------------------------------
    // DEVICE TASK
    //-------------------------------------------------------------------------
    static const size_t CONTROLLER_TASK_PERIODIC_MS = 100;
    static const size_t CONTROLLER_TASK_STOP_MSG    = yat::FIRST_USER_MSG + 300;
    static const size_t CONTROLLER_ABORT_MSG        = yat::FIRST_USER_MSG + 301;

    //-------------------------------------------------------------------------
    // STREAM
    //-------------------------------------------------------------------------
    static const size_t STREAM_TYPE_SIZE_MAX        = 20 ; // size max
    static const size_t STREAM_TARGET_PATH_SIZE_MAX = 255; // size max
    static const size_t STREAM_TARGET_FILE_SIZE_MAX = 255; // size max

    static const std::vector<std::string> TANGO_STREAM_TYPE_LABELS{"NO_STREAM", "LOG_STREAM", "CSV_STREAM", "NEXUS_STREAM" }; // labels of stream type

    // used to convert a tango stream type label (from STREAM_TYPE_LABELS) to a stream type
    static const std::vector<enum Stream::Types> TANGO_STREAM_TYPE_LABELS_TO_TYPE{Stream::Types::NO_STREAM   ,
                                                                                  Stream::Types::LOG_STREAM  ,
                                                                                  Stream::Types::CSV_STREAM  ,
                                                                                  Stream::Types::NEXUS_STREAM};

    static const std::vector<std::string> TANGO_STREAM_WRITE_MODE_LABELS{"ASYNCHRONOUS", "SYNCHRONOUS", "DELAYED"}; // labels of stream write mode

    // used to convert a tango stream write mode (from TANGO_STREAM_WRITE_MODE_LABELS) to a stream write mode
    static const std::vector<enum Stream::WriteModes> TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE{Stream::WriteModes::ASYNCHRONOUS,
                                                                                                   Stream::WriteModes::SYNCHRONOUS ,
                                                                                                   Stream::WriteModes::DELAYED     };

    static const std::vector<std::string> TANGO_STREAM_ITEMS_LABELS{"Frame"}; // labels of stream items
    
    // used to convert a stream item label to the corresponding stream item bit
    static const uint32_t TANGO_STREAM_ITEM_LABEL_TO_ITEM_BIT[] = {STREAM_ITEM_FRAME}; 

    //-------------------------------------------------------------------------
    // FLATFIELD
    //-------------------------------------------------------------------------
    static const size_t FLATFIELD_PATH_SIZE_MAX = 255; // size max
    static const size_t FLATFIELD_NAME_SIZE_MAX = 255; // size max

/*************************************************************************/
class Controller : public yat4tango::DeviceTask
{
    public:
        /*\brief Class used to to pass the needed data for the controller initialization*/
        class InitParameters
        {
            friend class Controller;
            friend class Stream    ;
        
            public:
                /// constructor
                InitParameters(const std::string & in_socket_server_host_address,
                               const int           in_socket_server_port        ,
                               const std::string & in_stream_type               ,
                               const std::string & in_stream_target_path        ,
                               const std::string & in_stream_target_file        ,
                               const uint32_t      in_stream_nb_acq_per_file    ,
                               const std::string & in_stream_write_mode         ,
                               const std::vector<std::string> & in_stream_items );

            private :
                string m_socket_server_host_address; /// ip address of the socket server
                int    m_socket_server_port        ; /// ip port    of the socket server 

	            std::string    m_stream_type           ; /// type of stream
	            std::string    m_stream_target_path    ; /// path for the stream files
                std::string    m_stream_target_file    ; /// name for the stream file
	            uint32_t       m_stream_nb_acq_per_file; /// number of files per acquisition
                std::string    m_stream_write_mode     ; /// stream write mode
                vector<string> m_stream_items          ; /// defines the content to stream
        };
        
    public:
        /// constructor 
        explicit Controller(Tango::DeviceImpl * dev, const std::string& detector_type);

        /// destructor
        virtual ~Controller();

        /// initializer - connection to the device
        void init(const Controller::InitParameters & in_init_parameters);
        
        /// releaser - disconnection from the device
        void release(void);

        /// Setter of the state
        void set_state(Tango::DevState state);

        /// Getter of the state
        Tango::DevState get_state(void);

        /// Setter of the status
        void set_status(const std::string& status);

        /// Getter of the status
        std::string get_status(void);

        /// Reset the detector and all active modules
        void reset(void) const;

        /// Manage the abort command
        void abort(std::string status);

        /// Manage the start of acquisition
        void start_acquisition(bool in_snap_mode);

        /// Manage the acquisition stop
        void stop_acquisition(bool in_sync);

        /// collect the data from the acquisition
        AcquisitionData collect(void);

        /// get the last frame received from the hardware
        std::vector<int32_t> get_last_frame(void) const;

        /// Get informations about DCS4 and active modules
        std::vector<std::pair<std::string, std::string> > get_versions_informations(void);

        /// Get informations of the hardware
        int32_t  get_max_nb_modules           (void);
        int32_t  get_nb_modules               (void);
        int32_t  get_nb_channels              (void);
        int32_t  get_nb_bad_channels          (void);
        float    get_temperature              (void);
        float    get_frame_rate_max           (void);
        bool     get_bad_channel_interpolation(void);
        bool     get_rate_correction          (void);
        int32_t  get_number_of_bits           (void);
        string   get_trigger_mode             (void);
        double   get_readout_time             (void);
        double   get_exposure_time            (void);
        double   get_delay_after_frame        (void);
        double   get_delay_before_frame       (void);
        int32_t  get_frames_number            (void);
        int32_t  get_gates_number_by_frame    (void);
        float    get_computed_frame_rate      (void);
        uint32_t get_acquired_frames_number   (void);

        /// Check if the frames number corresponds to a start acquisition
        /// When we use a start acquisition, we need to change the frame number of the hardware to the maximum value
        bool is_start_acquisition_frames_number(const int32_t & in_framesNumber) const;

        /// Set informations in the hardware
        void      set_number_of_bits           (const int32_t & in_bitsMode       );
        void      set_trigger_mode             (const string  & in_triggerMode    );
        void      set_bad_channel_interpolation(const bool    & in_IsEnabled      );
        void      set_rate_correction          (const bool    & in_IsEnabled      );
        void      set_exposure_time            (const double  & in_exposureTime_ms);
        void      set_delay_after_frame        (const double  & in_delay_ms       );
        void      set_delay_before_frame       (const double  & in_delay_ms       );
        void      set_frames_number            (const int32_t & in_framesNumber   );
        void      set_gates_number_by_frame    (const int32_t & in_gatesNumber    );

        /// Get informations of a module of the hardware
        float     get_module_temperature       (const int32_t & in_module_index);
        float     get_module_energy            (const int32_t & in_module_index);
        float     get_module_energy_min        (const int32_t & in_module_index);
        float     get_module_energy_max        (const int32_t & in_module_index);
        float     get_module_threshold         (const int32_t & in_module_index);
        float     get_module_threshold_min     (const int32_t & in_module_index);
        float     get_module_threshold_max     (const int32_t & in_module_index);
        float     get_module_humidity          (const int32_t & in_module_index);
        int32_t   get_module_high_voltage      (const int32_t & in_module_index);

        /// get informations for all modules of the hardware
        std::string get_string_modules_humidities   (void);
        std::string get_string_modules_temperatures (void);
        std::string get_string_modules_high_voltages(void);

        /// frame rate limitation management
        int32_t   get_frame_rate_limitation(void) const;
        void      set_frame_rate_limitation(const uint32_t in_new_value);
        
        // flatfield
        void                 store_flatfield     (const int32_t & in_slot_index, const std::vector<int32_t> & in_flatfield);
        void                 set_loaded_flatfield(const int32_t & in_slot_index);
        std::vector<int32_t> get_loaded_flatfield(void);
        std::vector<int32_t> get_module_loaded_flatfield(const int32_t & in_module_index);

        void set_flatfield_correction(const bool & in_IsEnabled);
        bool get_flatfield_correction(void);

        void        set_flatfield_path(const std::string & in_flatfield_path);
        std::string get_flatfield_path(void);
        std::string get_flatfield_name(void);

        void                     create_flatfield  (const std::string & in_name, const int32_t & in_module_index);
        void                     choose_flatfield  (const std::string & in_name);
        std::vector<std::string> get_flatfield_list(void);
        void                     refresh_flatfield (void);

        std::string get_module_serial_number(const int32_t & in_module_index);

        /// informations in a module of the hardware
        void      set_module_energy   (const int32_t & in_module_index, const float & in_energy   );
        void      set_module_threshold(const int32_t & in_module_index, const float & in_threshold);

        std::vector<int32_t> get_module_frame   (const int32_t & in_module_index);
        int32_t              get_module_channels(const int32_t & in_module_index);

        /// Stream management
        void        set_stream_type(const std::string & in_stream_type);
        std::string get_stream_type(void);

        void        set_stream_target_path(const std::string & in_stream_target_path);
        std::string get_stream_target_path(void);

        void        set_stream_target_file(const std::string & in_stream_target_file);
        std::string get_stream_target_file(void);

        void        set_stream_nb_acq_per_file(const uint32_t & in_stream_nb_acq_per_file);
        uint32_t    get_stream_nb_acq_per_file(void);

        /// Getter of the stream number of data per acquisition
        void update_stream(const AcquisitionData & in_acquisition_data);

        /// Reset the stream file index
        void stream_reset_index(void);

        /// Start the stream object for a new acquisition
        void start_stream_for_acquisition(void);

        /// Stop the stream object after a new acquisition
        void stop_stream_after_acquisition(void);

        /// Get the data stream string
        std::string get_data_streams(void);

		// Checks if an acquisition generates data files 
		bool get_file_generation(void);
		
    protected:
        /// [yat4tango::DeviceTask implementation]
        virtual void process_message(yat::Message& msg) throw(Tango::DevFailed);

    private:
        /// Manage the FAULT status with an exception
        void on_fault(Tango::DevFailed df);

        /// Manage the FAULT status with an error message
        void on_fault(const std::string& status_message);

        /// Compute the state and status 
        void compute_state_status();

        /// Convert a delay in units of 100 ns to ms
        double convert_delay_100ns_units_to_ms(const int64_t & in_delay_100ns) const;

        /// Convert a delay in ms to units of 100 ns
        int64_t convert_delay_ms_to_100_ns_units(const double & in_delay_ms) const;

        /// Instanciates the correct stream object
        void build_stream(const Controller::InitParameters & in_init_parameters);

        /// Convert a tango stream type label to a stream type enum
        enum Stream::Types convert_tango_stream_type_to_stream_type(const std::string & in_tango_stream_type);

        /// Convert a stream type enum to a tango stream type label
        std::string convert_stream_type_to_tango_stream_type(const enum Stream::Types & in_stream_type);

        /// Convert a tango stream write mode label to a stream write mode
        enum Stream::WriteModes convert_tango_stream_write_mode_to_stream_write_mode(const std::string & in_tango_stream_write_mode);

        /// Convert a stream write mode enum to a tango stream write mode label
        std::string convert_stream_write_mode_to_tango_stream_write_mode(const enum Stream::WriteModes & in_stream_write_mode);

        /// Convert a tango stream items vector to a stream items bit field
        uint32_t convert_tango_stream_items_to_stream_items_bit_field(const std::vector<std::string> & in_tango_stream_items);

        /// Convert a stream items bit field to a tango stream items vector
        std::vector<std::string> convert_stream_items_bit_field_to_tango_stream_items(const uint32_t & in_stream_items);

        /// Create an init parameters object using current stream configuration
        Controller::InitParameters create_stream_init_parameters(void);

    private:
        Tango::DeviceImpl           * m_device      ; /// Owner Device server object
        yat::SharedPtr<CollectTask>   m_collect_task; /// Module to execute tasks on the device and collect acquisition data
        yat::SharedPtr<DataTask>      m_data_task   ; /// Module to treat acquisition data
        yat::SharedPtr<Stream>        m_stream      ; /// Module for data stream mamangement (save in files, in cvs, just log, nothing) 

        Tango::DevState   m_state     ; /// state
        std::stringstream m_status    ; /// status
        yat::Mutex        m_state_lock; /// mutex to used protect state access
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_CONTROLLER_H_