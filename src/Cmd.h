/*************************************************************************/
/*! 
 *  \file   Cmd.h
 *  \brief  mythen command information class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_CMD_H_
#define MYTHEN_CMD_H_

// GLOBAL
#include "Config.h"


namespace Mythen2Detector_ns
{

// class used to store command information 
class Cmd
{ 
    public: 
        // constructor 
        Cmd(const std::string & in_cmd, const std::string & in_description, bool in_FloatReturnType);

        // destructor
        ~Cmd();

        // Accessor to cmd string
        std::string get_command_string();
        
        // Accessor to cmd description
        std::string get_command_description();

        // Accessor to cmd return type
        bool        is_float_return_type();

    protected: 
        std::string m_cmd              ; // command string to send to device
        std::string m_description      ; // command description for logging
        bool        m_float_return_type; // true if the return type is a float - usefull for the error managing
}; 

} // namespace Mythen2Detector_ns

#endif // MYTHEN_CMD_H_

//###########################################################################
