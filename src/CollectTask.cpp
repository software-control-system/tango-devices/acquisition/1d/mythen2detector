/*************************************************************************/
/*! 
 *  \file   CollectTask.cpp
 *  \brief  class which calls Mythen commands
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "CollectTask.h"
#include <yat/time/Time.h>

namespace Mythen2Detector_ns
{

#define KELVIN_TO_CELCIUS(K) (K - 273.15)

//============================================================================================================
/*******************************************************************
 * \brief constructor
 * \param[in] dev acces to tango device for log management
 *******************************************************************/
CollectTask::CollectTask(Tango::DeviceImpl * dev, const std::string& detector_type) : Tango::LogAdapter(dev), m_device(dev)
{
    MYTHEN_TASK_DEBUG_STREAM << "CollectTask::CollectTask - [BEGIN]" << std::endl;

    // default status management
    set_status("Starting ...");
    set_state (Tango::INIT  );

    // create the communication manager
	if(detector_type == "SIMULATOR")
	{
		m_Com = new ComSim(dev);
	}
	else if(detector_type == "MYTHEN2")
	{
		m_Com = new ComNet(dev);
	}
    else
    {
        //only MYTHEN2/SIMULATOR
        std::ostringstream ossMsgErr;
        ossMsgErr   <<  "Wrong DetectorType :\n"
        "Possibles values are:\n"
        "MYTHEN2\n"
        "SIMULATOR\n"
        <<std::endl;
        Tango::Except::throw_exception("LOGIC_ERROR",
                                       ossMsgErr.str().c_str(),
                                       "CollectTask::CollectTask()");
    }		


    m_nb_modules           = 0; // by default
    m_all_modules_channels = 0; // by default

    m_saved_frames_number           = 0; // by default
    m_acquired_frames_number        = 0; // by default
    m_frames_number_to_be_acquired  = 0; // by default
    m_time_between_readout_calls_ms = 0; // by default
    m_frames_number_by_readout_call = 0; // by default

    m_acquisition_in_snap_mode = false;  // by default

    m_flatfield_path      = ""; // by default
    m_last_flatfield_name = ""; // by default

    m_frame_rate_limitation = FRAME_RATE_LIMITATION_DISABLED; // by default
    
    MYTHEN_TASK_DEBUG_STREAM << "CollectTask::CollectTask - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
CollectTask::~CollectTask() 
{
    MYTHEN_TASK_INFO_STREAM << "CollectTask::~CollectTask - [BEGIN]" << std::endl;

    // delete the communication manager
    delete m_Com;

    MYTHEN_TASK_INFO_STREAM << "CollectTask::~CollectTask - [END]" << std::endl;
}

//============================================================================================================
/*******************************************************************
 * \brief Connection to server
 * \param[in] in_hostname server address
 * \param[in] in_port     server port
 *******************************************************************/
void CollectTask::connect(const std::string & in_hostname, int in_port)
{
    MYTHEN_TASK_INFO_STREAM << "CollectTask::connect()" << std::endl;

    m_Com->connect(in_hostname, in_port);

    // we need to store the activated modules number for further commands
    int32_t max_nb_modules = get_max_nb_modules();

    m_nb_modules = get_nb_modules();

    if(m_nb_modules == 0)
    {
        std::ostringstream MsgErr;

        MsgErr << "No activated module found for the DCS." << std::endl;

        throw_tango_exception("TANGO_DEVICE_ERROR",
                              MsgErr.str().c_str(),
                              "CollectTask::connect()");
    }

    MYTHEN_TASK_INFO_STREAM << "number of modules : " 
                            << std::setfill ('0') << std::setw(2) << m_nb_modules   << "/" 
                            << std::setfill ('0') << std::setw(2) << max_nb_modules << std::endl;

    // we need to store the number of channels
    get_modules_number_of_channels(m_modules_channels, m_all_modules_channels);

    // we need to store the channels start index of each module
    {
        int first_channel_Index = 0;
        int module_index        = 0;

        while(module_index < m_nb_modules)
        {
            m_modules_channels_start_index.push_back(first_channel_Index);
            first_channel_Index += m_modules_channels[module_index];
            module_index++;
        }
    }

    // we store the module serial number - will be useful for flatfield management
    m_modules_serial_number = get_modules_serial_number();

    // we initialize the last acquisition data with an empty spectrum
    std::vector<int32_t> readout_data_frame;
    readout_data_frame.assign(m_all_modules_channels, 0); // resizes the vector to the number of channels and initializes all cases with 0 value

    m_last_acquisition_data = AcquisitionData(readout_data_frame, 0);
}

/*******************************************************************
 * \brief Disconnection from server
 *******************************************************************/
void CollectTask::disconnect(void)
{
    MYTHEN_TASK_INFO_STREAM << "CollectTask::disconnect()" << std::endl;
    stop_acquisition();
    m_Com->disconnect();
}

//============================================================================================================
/*******************************************************************
 * \brief Setter of the state
*******************************************************************/
void CollectTask::set_state(Tango::DevState state)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState CollectTask::get_state(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    compute_state_status();
    return m_state;
}

/*******************************************************************
 * \brief Setter of the status
*******************************************************************/
void CollectTask::set_status(const std::string& status)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string CollectTask::get_status(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

/*******************************************************************
 * \brief Compute the state and status 
*******************************************************************/
void CollectTask::compute_state_status()
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);

    try
    {
        if(m_state != Tango::FAULT)
        {
            if(!acquisition_is_completed())
            {
                set_state (Tango::RUNNING);
                set_status("CollectTask is running an acquisition...");
            }
            else
            {
                int32_t status = get_dcs_status();

                if((status & STATUS_ACQUISITION_MASK) == 0)
                {
                    set_state (Tango::STANDBY);
                    set_status("CollectTask is in standby...");
                }
                else
                {
                    set_state (Tango::RUNNING);
                    set_status("CollectTask is running an acquisition...");
                }
            }
        }
    }
    catch (Tango::DevFailed & df)
    {
        on_fault(df);
        throw; // rethrow exception ? 
    }
    catch(...)
    {
        on_fault("Unknown exception occured.");
    }
}

/*******************************************************************
 * \brief Manage the FAULT status with an exception
*******************************************************************/
void CollectTask::on_fault(const Tango::DevFailed & df)
{
    DEBUG_STREAM << "Controller::on_fault()" << std::endl;

    std::stringstream status;
    status.str("");
    status << "Origin\t: " << df.errors[0].origin << std::endl;
    status << "Desc\t: "   << df.errors[0].desc   << std::endl;
    status << "Reason\t: " << df.errors[0].reason << std::endl;

    on_fault(status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an error message
*******************************************************************/
void CollectTask::on_fault(const std::string& status_message)
{
    set_state (Tango::FAULT  );
    set_status(status_message);
}

//============================================================================================================
/*******************************************************************
 * \brief Get the number of channels of all active modules
 * \return The number of channels of all active modules
 *******************************************************************/
int32_t CollectTask::get_nb_channels(void) const
{
    return m_all_modules_channels;
}

//============================================================================================================
/*******************************************************************
 * \brief Get the assembly date of the system
 * \return assembly date returned by the mythen
 *******************************************************************/
std::string CollectTask::get_assembly_date(void) const
{
    return get_string(Com::ServerCmd::GET_ASSEMBLY_DATE,
                      ASSEMBLY_DATE_BYTE_SIZE + 1      , 
                      ASSEMBLY_DATE_BYTE_SIZE          ,
                      "Assembly date"                  ,
                      "CollectTask::get_assembly_date" );
}

/*******************************************************************
 * \brief Get the firmware version
 * \return the firmware version
*******************************************************************/
std::string CollectTask::get_firmware_version(void) const
{
    return get_string(Com::ServerCmd::GET_FW_VERSION     ,
                      FIRMWARE_VERSION_BYTE_SIZE         ,
                      FIRMWARE_VERSION_BYTE_SIZE         ,
                      "Firmware version"                 ,
                      "CollectTask::get_firmware_version");
}

/*******************************************************************
 * \brief Get the software version
 * \return the software version of socket server
*******************************************************************/
std::string CollectTask::get_software_version(void) const
{
    return get_string(Com::ServerCmd::GET_VERSION        ,
                      SOFTWARE_VERSION_BYTE_SIZE         ,
                      SOFTWARE_VERSION_BYTE_SIZE         ,
                      "Software version"                 ,
                      "CollectTask::get_software_version");
}

/*******************************************************************
 * \brief Get the firmware version of active modules
 * \return the firmware version of active modules
*******************************************************************/
std::vector<std::string> CollectTask::get_modules_firmware_version(void) const
{
    std::vector<std::string> modulesFirmwareVersion;

    modulesFirmwareVersion.clear();

    if(m_nb_modules > 0)
    {
        std::string temp;

        temp = get_string(Com::ServerCmd::GET_MOD_FW_VERSION,
                          (MODULES_FIRMWARE_VERSION_BYTE_SIZE * m_nb_modules) + 1,
                          (MODULES_FIRMWARE_VERSION_BYTE_SIZE * m_nb_modules) + 1,
                          "", // no automatic logging by the called method
                          "CollectTask::get_modules_firmware_version"); 

        for(int moduleIndex = 0 ; moduleIndex < m_nb_modules ; moduleIndex++)
        {
            modulesFirmwareVersion.push_back(temp.substr(moduleIndex * MODULES_FIRMWARE_VERSION_BYTE_SIZE, 
                                                         MODULES_FIRMWARE_VERSION_BYTE_SIZE));
        }

        show_vector_information(modulesFirmwareVersion, "Active modules firmware version");
    }

    return modulesFirmwareVersion;
}

/*******************************************************************
 * \brief Get the serial number of active modules
 * \return serial number of modules
*******************************************************************/
std::vector<std::string> CollectTask::get_modules_serial_number(void) const
{
    std::vector<std::string> modulesSerialNumber;

    modulesSerialNumber.clear();

    if(m_nb_modules > 0)
    {
        std::vector<int32_t> temp;

        temp = get_data_vector<int32_t>(Com::ServerCmd::GET_MOD_NUM, 
                                        m_nb_modules,
                                        "CollectTask::get_modules_serial_number");

        for(int moduleIndex = 0 ; moduleIndex < m_nb_modules ; moduleIndex++)
        {
            modulesSerialNumber.push_back(int_to_hex(temp[moduleIndex]));
        }

        show_vector_information(modulesSerialNumber, "Active modules serial number");
    }

    return modulesSerialNumber;
}

/*******************************************************************
 * \brief Get the serial number of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return module serial number
*******************************************************************/
std::string CollectTask::get_module_serial_number(const int32_t & in_module_index) const
{
    return m_modules_serial_number[in_module_index];
}

/*******************************************************************
 * \brief Get the DCS serial number
 * \return the DCS serial number
*******************************************************************/
int32_t CollectTask::get_serial_number(void) const
{
    return get_int32(Com::ServerCmd::GET_SYSTEM_NUM, "Serial number", "", "CollectTask::get_serial_number");
}

//============================================================================================================
/*******************************************************************
 * \brief Get the DCS temperature in default scale
 * \param[out] out_temperature  temperature returned by the mythen
 * \param[out] out_error        Error returned by the command
 * \return true in case of success, false in case of error
 *******************************************************************/
float CollectTask::get_dcs_temperature(void) const
{
    return get_float(Com::ServerCmd::GET_DCS_TEMPERATURE, "", "", "CollectTask::get_dcs_temperature");
}

/*******************************************************************
 * \brief Get the DCS temperature in Kelvin degrees
 * \return the DCS temperature in Kelvin degrees
*******************************************************************/
float CollectTask::get_dcs_temperature_kelvin(void) const
{
    float temperature = get_dcs_temperature();

    show_information(temperature, "DCS temperature", "K");

    return temperature;
}

/*******************************************************************
 * \brief Get the DCS temperature in Celcius degrees
 * \return the DCS temperature in Celcius degrees
 *******************************************************************/
float CollectTask::get_dcs_temperature_celcius(void) const
{
    float temperature = get_dcs_temperature();

    temperature = KELVIN_TO_CELCIUS(temperature);
    show_information(temperature, "DCS temperature", "�C");

    return temperature;
}

/*******************************************************************
 * \brief Get the temperature in default scale of activated modules
 * \return the temperature in default scale of activated modules
 *******************************************************************/
std::vector<float> CollectTask::get_modules_temperature(void) const
{
    return get_data_vector<float>(Com::ServerCmd::GET_TEMPERATURE, 
                                  m_nb_modules,
                                  "CollectTask::get_modules_temperature");
}

/*******************************************************************
 * \brief Get the temperature in kelvin degrees of activated modules
 * \return the temperature in kelvin degrees of activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_temperature_kelvin(void) const
{
    std::vector<float> temperatures = get_modules_temperature();

    show_vector_information(temperatures, "Active modules temperatures", "K");

    return temperatures;
}

/*******************************************************************
 * \brief Get the temperature in celcius degrees of activated modules
 * \return the temperature in celcius degrees of activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_temperature_celcius(void) const
{
    std::vector<float> temperatures = get_modules_temperature();

    for(std::size_t moduleIndex = 0 ; moduleIndex < temperatures.size() ; moduleIndex++)
    {
        temperatures[moduleIndex] = KELVIN_TO_CELCIUS(temperatures[moduleIndex]);
    }

    show_vector_information(temperatures, "Active modules temperatures", "�C");

    return temperatures;
}

/*******************************************************************
 * \brief Get the temperature in kelvin degrees of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return the temperature in kelvin degrees of a module
*******************************************************************/
float CollectTask::get_module_temperature_kelvin(const int32_t & in_module_index) const
{
    std::vector<float> temperatures = get_modules_temperature_kelvin();
    return temperatures[in_module_index];
}

/*******************************************************************
 * \brief Get the temperature in celcius degrees of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return the temperature in celcius degrees of a module
*******************************************************************/
float CollectTask::get_module_temperature_celcius(const int32_t & in_module_index) const
{
    std::vector<float> temperatures = get_modules_temperature_celcius();
    return temperatures[in_module_index];
}

/*******************************************************************
 * \brief Get a string which contains all modules temperature values
 * format of result for i modules is : 
 * temperature01=x.xx;...;temperature0i=x.xx
 * \return A formated string with all modules temperature values
*******************************************************************/
std::string CollectTask::get_string_modules_temperatures_celcius(void) const
{
    std::vector<float> temperatures = get_modules_temperature_celcius();
    int32_t            nb_modules = temperatures.size();
    int32_t            i     ;
    std::string        result;

    for(i = 0; i < nb_modules; i++)
    {
        if(i > 0)
        {
            result += ";";
        }
        
        stringstream label("");
        label << "temperature" << std::setfill('0') << std::setw(2) << (i+1);

        stringstream value("");
        value << setprecision(2) << fixed << temperatures[i];

        result = result + label.str() + "=" + value.str();
    }

    return result;
}

/*******************************************************************
 * \brief Get the humidity of activated modules
 * \return The humidity of activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_humidity(void) const
{
    return get_float_vector(Com::ServerCmd::GET_HUMIDITY, false, "Active modules humidity", "", "CollectTask::get_modules_humidity");
}

/*******************************************************************
 * \brief Get the humidity of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return The humidity of a module
*******************************************************************/
float CollectTask::get_module_humidity(const int32_t & in_module_index) const
{
    std::vector<float> humidities = get_modules_humidity();
    return humidities[in_module_index];
}

/*******************************************************************
 * \brief Get a string which contains all modules humidity values
 * format of result for i modules is : 
 * humidity01=x.xx;...;humidity0i=x.xx
 * \return A formated string with all modules humidity values
*******************************************************************/
std::string CollectTask::get_string_modules_humidities(void) const
{
    std::vector<float> humidities = get_modules_humidity();
    int32_t            nb_modules = humidities.size();
    int32_t            i     ;
    std::string        result;

    for(i = 0; i < nb_modules; i++)
    {
        if(i > 0)
        {
            result += ";";
        }
        
        stringstream label("");
        label << "humidity" << std::setfill('0') << std::setw(2) << (i+1);

        stringstream value("");
        value << setprecision(2) << fixed << humidities[i];

        result = result + label.str() + "=" + value.str();
    }

    return result;
}

/*******************************************************************
 * \brief Get the high voltage of activated modules
 * \return The high voltage of activated modules
*******************************************************************/
std::vector<int32_t> CollectTask::get_modules_high_voltage(void) const
{
    std::vector<int32_t> highVoltage = get_data_vector<int32_t>(Com::ServerCmd::GET_HV    , 
                                                                m_nb_modules,
                                                                "CollectTask::get_modules_high_voltage");

    show_vector_information(highVoltage, "Active modules high voltage");

    return highVoltage;
}

/*******************************************************************
 * \brief Get the high voltage of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return The high voltage of a module
*******************************************************************/
int32_t CollectTask::get_module_high_voltage(const int32_t & in_module_index) const
{
    std::vector<int32_t> high_voltages = get_modules_high_voltage();
    return high_voltages[in_module_index];
}

/*******************************************************************
 * \brief Get a string which contains all modules high voltage values
 * format of result for i modules is : 
 * temperature01=x.xx;...;temperature0i=x.xx
 * \return A formated string with all modules high voltage values
*******************************************************************/
std::string CollectTask::get_string_modules_high_voltages(void) const
{
    std::vector<int32_t> highVoltages = get_modules_high_voltage();
    int32_t              nb_modules   = highVoltages.size();
    int32_t              i     ;
    std::string          result;

    for(i = 0; i < nb_modules; i++)
    {
        if(i > 0)
        {
            result += ";";
        }
        
        stringstream text("");
        text << "highVoltage" << std::setfill('0') << std::setw(2) << (i+1) << "=" << highVoltages[i];
        result = result + text.str();
    }

    return result;
}

/*******************************************************************
 * \brief Get the current spectrum of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return The current spectrum of a module
*******************************************************************/
std::vector<int32_t> CollectTask::get_module_frame(const int32_t & in_module_index)
{
    AcquisitionData last_frame = get_last_frame();
    std::vector<int32_t> total_spectrum = last_frame.get_frame();

    // we must cut the spectrum to get only the module spectrum
    std::vector<int32_t> module_spectrum(total_spectrum.begin() + m_modules_channels_start_index[in_module_index], 
                                         total_spectrum.begin() + m_modules_channels_start_index[in_module_index] + m_modules_channels[in_module_index]);

    return module_spectrum; // return a copy of the vector
}

/*******************************************************************
 * \brief Get the number of channels of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return The number of channels of a module
*******************************************************************/
int32_t CollectTask::get_module_channels(const int32_t & in_module_index)
{
    return m_modules_channels[in_module_index];
}

//============================================================================================================
/*******************************************************************
 * \brief Get the frame rate limitation value
 * \return the frame rate limitation value
 *******************************************************************/
int32_t CollectTask::get_frame_rate_limitation(void) const
{
    return m_frame_rate_limitation;
}

/*******************************************************************
 * \brief Set the frame rate limitation value 
 * \param[in] in_new_value frame rate limitation value 
*******************************************************************/
void CollectTask::set_frame_rate_limitation(const uint32_t in_new_value)
{
    m_frame_rate_limitation = in_new_value;
    
    if( m_frame_rate_limitation != FRAME_RATE_LIMITATION_DISABLED)
    {
        MYTHEN_TASK_INFO_STREAM << "frame rate limitation : enabled - must be <= " << m_frame_rate_limitation << std::endl;
    }
    else
    {
        MYTHEN_TASK_INFO_STREAM << "frame rate limitation : disabled" << std::endl;
    }
}

/*******************************************************************
 * \brief Get the maximum frame rate for active detector modules
 * \return The maximum frame rate for active detector modules
 *******************************************************************/
float CollectTask::get_frame_rate_max(void) const
{
    return get_float(Com::ServerCmd::GET_FRAMERATE_MAX, "Frame rate max", "", "CollectTask::get_frame_rate_max");
}

/*******************************************************************
 * \brief Log the max computed frame rate after a data change
 *******************************************************************/
void CollectTask::log_max_computed_frame_rate(void) const
{
    MYTHEN_TASK_INFO_STREAM << "max computed frame rate : " << get_computed_frame_rate() << std::endl;
}

/*******************************************************************
 * \brief Check if the framerate will be higher than the maximum
 *        allowed frame rate.
 * \param[in] in_trigger_mode       new trigger mode - set to
 *                                  TRIGGER_CURRENT to use current
 *                                  value 
 * \param[in] in_bitsMode           new bits mode - set to 
 *                                  BITS_MODE_CURRENT to use 
 *                                  current value
 * \param[in] in_exposure_time      exposure time - set to
 *                                  EXPOSURE_TIME_CURRENT to use 
 *                                  current value
 * \param[in] in_delay_after_frame  delay after a frame - set to
 *                                  DELAY_AFTER_FRAME_CURRENT to 
 *                                  use current value
 * \param[in] in_delay_before_frame delay before a frame - set to
 *                                  DELAY_BEFORE_FRAME_CURRENT to 
 *                                  use current value
 * \return nothing - can throw an exception if checking fails
*******************************************************************/
void CollectTask::check_frame_rate(const Com::TriggerMode in_trigger_mode      ,       // the frame rate depends on the trigger mode
                                   const int64_t          in_bitsMode          ,       // the readout time of the bits mode is necessary to compute the frame time
                                   const int64_t          in_exposure_time     ,       // the exposure time is necessary to compute the frame time
                                   const int64_t          in_delay_after_frame ,       // the delay after a frame
                                   const int64_t          in_delay_before_frame) const // the delay before a frame (between the trigger and the start of exposure)
{
    if( m_frame_rate_limitation != FRAME_RATE_LIMITATION_DISABLED)
    {
        float computed_frame_rate = get_computed_frame_rate(in_trigger_mode      , 
                                                            in_bitsMode          , 
                                                            in_exposure_time     , 
                                                            in_delay_after_frame , 
                                                            in_delay_before_frame);
                                                            
        if(computed_frame_rate != UNAVAILABLE_ESTIMATED_FRAME_RATE)
        {
            if(computed_frame_rate > static_cast<float>(m_frame_rate_limitation))
            {
                std::stringstream message;
                message.str("");
                message << "A frame rate limitation is enabled for this device." << std::endl << std::endl;
                message << "The current frame rate " << std::setprecision(2) << std::fixed << computed_frame_rate;
                message << " is above the maximum allowed " << m_frame_rate_limitation << "!" << std::endl << std::endl;
                message << "Please check your expertFrameRateLimitation property and the following attributes :" << std::endl;
                message << "exposure time, delay after frame, delay before frame, bits mode and trigger mode." << std::endl;

                throw_tango_exception("LOGIC_ERROR",
                                      message.str().c_str(),
                                      "CollectTask::check_frame_rate()");
            }
        }
    }
}

/*******************************************************************
 * \brief Compute the frame rate with differents informations
 *        (current trigger, delays, readout time, ...) 
 * \return the frame rate
*******************************************************************/
float CollectTask::get_computed_frame_rate(void) const
{ 
    return get_computed_frame_rate(Com::TriggerMode::TRIGGER_CURRENT,  // the frame rate depends on the trigger mode
                                   BITS_MODE_CURRENT                ,  // the readout time of the bits mode is necessary to compute the frame time
                                   EXPOSURE_TIME_CURRENT            ,  // the exposure time is necessary to compute the frame time
                                   DELAY_AFTER_FRAME_CURRENT        ,  // the delay after a frame
                                   DELAY_BEFORE_FRAME_CURRENT       ); // the delay before a frame (between the trigger and the start of exposure)
}

/*******************************************************************
 * \brief Compute the frame rate with differents informations
 *        (current trigger, delays, readout time, ...) 
 * \param[in] in_trigger_mode       new trigger mode - set to
 *                                  TRIGGER_CURRENT to use current
 *                                  value 
 * \param[in] in_bitsMode           new bits mode - set to 
 *                                  BITS_MODE_CURRENT to use 
 *                                  current value
 * \param[in] in_exposure_time      exposure time - set to
 *                                  EXPOSURE_TIME_CURRENT to use 
 *                                  current value
 * \param[in] in_delay_after_frame  delay after a frame - set to
 *                                  DELAY_AFTER_FRAME_CURRENT to 
 *                                  use current value
 * \param[in] in_delay_before_frame delay before a frame - set to
 *                                  DELAY_BEFORE_FRAME_CURRENT to 
 *                                  use current value
 * \return the computed frame rate
*******************************************************************/
float CollectTask::get_computed_frame_rate(const Com::TriggerMode in_trigger_mode      , // the frame rate depends on the trigger mode
                                           const int64_t          in_bitsMode          , // the readout time of the bits mode is necessary to compute the frame time
                                           const int64_t          in_exposure_time     , // the exposure time is necessary to compute the frame time
                                           const int64_t          in_delay_after_frame , // the delay after a frame
                                           const int64_t          in_delay_before_frame) const // the delay before a frame (between the trigger and the start of exposure)
{
    const double frame_rate_max = static_cast<double>(get_frame_rate_max()); // the computed frame rate cannot be above the maximum frame rate (device)

    double  computed_frame_rate = 0.0;
    int64_t frame_time_100ns    = 0LL; // time between the start of two frames

    // first, retrieve the generic values
    const Com::TriggerMode trigger_mode  = (in_trigger_mode  == Com::TriggerMode::TRIGGER_CURRENT) ? get_trigger_mode () : in_trigger_mode;
    const int64_t          readout_time  = (in_bitsMode      == BITS_MODE_CURRENT    ) ? get_readout_time () : get_readout_time_of_bits_mode(in_bitsMode);
    const int64_t          exposure_time = (in_exposure_time == EXPOSURE_TIME_CURRENT) ? get_exposure_time() : in_exposure_time;
    
    // now, compute the frame delay 
    if((trigger_mode == Com::TriggerMode::TRIGGER_INTERNAL       ) ||
       (trigger_mode == Com::TriggerMode::TRIGGER_EXTERNAL_SINGLE))
    {
        /// Get the delay after a frame
        int64_t delay_after_frame = (in_delay_after_frame == DELAY_AFTER_FRAME_CURRENT) ? get_delay_after_frame() : in_delay_after_frame;

        // the real delay after frame cannot be below the readout time
        frame_time_100ns = exposure_time + ((delay_after_frame < readout_time) ? readout_time : delay_after_frame);
    }
    else
    if(trigger_mode == Com::TriggerMode::TRIGGER_EXTERNAL_MULTIPLE)
    {
        /// Get the delay before a frame (between the trigger and the start of exposure)
        int64_t delay_before_frame = (in_delay_before_frame == DELAY_BEFORE_FRAME_CURRENT) ? get_delay_before_frame() : in_delay_before_frame;

        // the real delay after frame cannot be below the readout time
        frame_time_100ns = delay_before_frame + exposure_time + readout_time;
    }
    else
    if(trigger_mode != Com::TriggerMode::TRIGGER_EXTERNAL_GATE)
    {
        std::string msg_error = "Unknown trigger mode!";

        throw_tango_exception("LOGIC_ERROR" ,
                              msg_error.c_str()    ,
                              "CollectTask::get_computed_frame_rate");
    }

    // In the external gate mode, it is not possible to compute de frame rate
    if(trigger_mode == Com::TriggerMode::TRIGGER_EXTERNAL_GATE)
    {
        computed_frame_rate = UNAVAILABLE_ESTIMATED_FRAME_RATE;
    }
    else
    {
        if(frame_time_100ns == 0)
        {
            std::string msg_error = "Configuration problem! Please check exposure time, delay after frame, delay before frame, bits mode and trigger mode.";

            throw_tango_exception("LOGIC_ERROR" ,
                                  msg_error.c_str()    ,
                                  "CollectTask::get_computed_frame_rate");
        }
        else
        {
            // 1ms = 10000 units of 100ns, 1000ms = 1sec
            computed_frame_rate = (1000.0 * 10000.0) / static_cast<double>(frame_time_100ns);
            computed_frame_rate = (computed_frame_rate < frame_rate_max) ? computed_frame_rate : frame_rate_max;
        }
    }

    return static_cast<float>(computed_frame_rate); // no need to keep the double precision
}

/*******************************************************************
 * \brief Get the current acquiried frame number 
 * \return the current frame number
*******************************************************************/
uint32_t CollectTask::get_acquired_frames_number(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_frames_number_lock);
    return m_acquired_frames_number;
}

/*******************************************************************
 * \brief Set the current acquiried frame number 
 * \param[in] in_new_value the new acquiried frame number 
*******************************************************************/
void CollectTask::set_acquired_frames_number(const uint32_t in_new_value)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_frames_number_lock);
    m_acquired_frames_number = in_new_value;
}

//============================================================================================================
/**********************************************************************************
 * \brief Get the bad channels for each module
 * \param[out] out_badChannelsByModule       bad channels (true for bad channel)
 * \param[out] out_badChannelsNumberByModule number of bad channels for each module
 * \param[out] out_allBadChannelsNumber      total number of bad channels
 **********************************************************************************/
void CollectTask::get_modules_bad_channels(std::vector< std::vector<bool> > & out_badChannelsByModule      , 
                                           std::vector<int32_t>             & out_badChannelsNumberByModule,
                                           int32_t                          & out_allBadChannelsNumber     ) const
{
    out_badChannelsByModule.clear();
    out_badChannelsNumberByModule.clear();
    out_allBadChannelsNumber = 0;

    if(m_all_modules_channels > 0)
    {
        int32_t   error  ;                    
        bool      result ;
        int32_t * temp   = new int32_t[m_all_modules_channels];

        result = m_Com->get_command(Com::ServerCmd::GET_BAD_CHANNELS, temp, m_all_modules_channels, &error);

        if(result)
        {
            std::size_t channelIndex      = 0;
            int         moduleIndex       ;
            int         channelModuleIndex;
            
            for(moduleIndex = 0 ; moduleIndex < m_nb_modules ; moduleIndex++)
            {
                out_badChannelsNumberByModule.push_back(0); // add a new module to the vector
                out_badChannelsByModule.push_back(std::vector<bool>()); // add a new module to the vector

                for(channelModuleIndex = 0 ; channelModuleIndex < m_modules_channels[moduleIndex] ; channelModuleIndex++)
                {
                    out_badChannelsByModule[moduleIndex].push_back(temp[channelIndex] == 1); // 1 is for defective channel - conversion to boolean
                    out_badChannelsNumberByModule[moduleIndex] += temp[channelIndex];
                    out_allBadChannelsNumber += temp[channelIndex];

                    channelIndex++;
                }
            }

            show_vector_proportion_information(out_badChannelsNumberByModule, m_modules_channels, "Number of bad channels for active modules");
            show_information(out_allBadChannelsNumber, "Total number of bad channels for active modules");
        }

        delete [] temp;

        // exception management
        if(!result)
        {
            std::string msg_error = get_error_string(error);

            throw_tango_exception("TANGO_DEVICE_ERROR" ,
                                  msg_error.c_str()    ,
                                  "CollectTask::get_modules_bad_channels");
        }
    }
}

/**************************************************************************************
 * \brief Get the number of bad channels for all active modules
 * \return The number of bad channels for all active modules
 **************************************************************************************/
int32_t CollectTask::get_nb_bad_channels(void) const
{
    std::vector< std::vector<bool> > badChannelsByModule      ;
    std::vector<int32_t>             badChannelsNumberByModule;
    int32_t                          allBadChannelsNumber     ;

    get_modules_bad_channels(badChannelsByModule      , 
                             badChannelsNumberByModule,
                             allBadChannelsNumber     );

    return allBadChannelsNumber;
}

//============================================================================================================
/******************************************************************************
 * \brief Get the selected modules indexes
 * \return the selected modules indexes (SELECT_ALL_MODULES if all selected).
 *******************************************************************$**********/
int32_t CollectTask::get_selected_modules(void) const
{
    const int32_t selectedModules = get_int32(Com::ServerCmd::GET_MODULE, "", "", "CollectTask::get_selected_modules");

    if(selectedModules == SELECT_ALL_MODULES)
    {
        show_information("All", "Selected modules");
    }
    else
    {
        show_information(selectedModules, "Selected modules");
    }

    return selectedModules;
}

/*******************************************************************
 * \brief Get the maximum modules number
 * \return the maximum modules number
 *******************************************************************/
int32_t CollectTask::get_max_nb_modules(void) const
{
    return get_int32(Com::ServerCmd::GET_N_MAX_MODULES, "Maximum modules number", "", "CollectTask::get_maximum_modules_number");
}

/*******************************************************************
 * \brief Get the activated modules number
 * \return the activated modules number
 *******************************************************************/
int32_t CollectTask::get_nb_modules(void)
{
    // store the value, no need to send several times the command.
    // we can not change the number of connected modules without a device re-start.
    if(m_nb_modules == 0)
    {
        m_nb_modules = get_int32(Com::ServerCmd::GET_N_MODULES, "Activated modules number", "", "CollectTask::get_activated_modules_number");
    }

    return m_nb_modules;
}

/*******************************************************************
 * \brief Select an active module by index (starts at 0)
 * \param[out] in_selectedModule  selected module index
 * \param[out] out_error          Error returned by the command
 * \return true in case of success, false in case of error
 *******************************************************************/
void CollectTask::select_module(const int32_t & in_selectedModule) const
{
    set_int32(Com::ServerCmd::SEL_MODULE, in_selectedModule, "Selection of module", "", "CollectTask::select_module");
}

/*******************************************************************
 * \brief Select all active modules
 * \param[out] out_error Error returned by the command
 * \return true in case of success, false in case of error
 *******************************************************************/
void CollectTask::select_all_modules(void) const
{
    set_int32(Com::ServerCmd::SEL_MODULE, SELECT_ALL_MODULES, "", "", "CollectTask::select_all_modules");

    show_action("Selection of all modules");
}

/***********************************************************************
 * \brief Get the number of channels for each module
 * \param[out] out_modulesChannels    number of channels for each module
 * \param[out] out_allModulesChannels total number of channels
 ***********************************************************************/
void CollectTask::get_modules_number_of_channels(std::vector<int32_t> & out_modulesChannels, int32_t & out_allModulesChannels) const
{
    out_allModulesChannels = 0;
    out_modulesChannels    = get_data_vector<int32_t>(Com::ServerCmd::GET_MOD_CHANNELS, 
                                                      m_nb_modules,
                                                      "CollectTask::get_modules_number_of_channels");
    // store the total number of channels
    for(std::size_t moduleIndex = 0 ; moduleIndex < out_modulesChannels.size() ; moduleIndex++)
    {
        out_allModulesChannels += out_modulesChannels[moduleIndex];
    }

    show_vector_information(out_modulesChannels, "Number of channels of active modules");
    show_information(out_allModulesChannels, "Total number of channels of active modules");
}

//============================================================================================================
/*******************************************************************
 * \brief Reset the detector and all active modules
 *******************************************************************/
void CollectTask::reset(void) const
{
    simple_command(Com::ServerCmd::CMD_RESET, false, "All modules reset", "CollectTask::reset_modules");
}

/*******************************************************************************
 * \brief Start the acquisition
 * \param[in] in_snap_mode                     true in snap/stop mode, 
 *                                             false for start/stop mode
 * \param[in] in_time_between_readout_calls_ms To give the period which will be
 *                                             used between two calls of readout
 *******************************************************************************/
void CollectTask::start_acquisition(bool in_snap_mode, uint32_t in_time_between_readout_calls_ms)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_acquisition_lock);

    MYTHEN_TASK_INFO_STREAM << "CollectTask::start_acquisition - [BEGIN]" << std::endl;

    // check that there is no acquisition running
    int32_t status = get_dcs_status();

    if((status & STATUS_ACQUISITION_MASK) == 0)
    {
        // restart the frame index
        set_acquired_frames_number(0);

        // store the frames number
        m_saved_frames_number = get_frames_number();

        // store if the acquisition is in snap mode
        m_acquisition_in_snap_mode = in_snap_mode;

        // store the needed frames number
        if(m_acquisition_in_snap_mode)
        {
            m_frames_number_to_be_acquired = m_saved_frames_number;
        }
        else
        {
            m_frames_number_to_be_acquired = FRAMES_NUMBER_MAXIMUM_VALUE; // maximum frame number value
            set_frames_number(m_frames_number_to_be_acquired); // give the new value to the hardware
        }

        // store the time between two calls of readout
        m_time_between_readout_calls_ms = in_time_between_readout_calls_ms;
    
        // compute the number of frames we will read with every readout call
        double frame_rate = static_cast<double>(get_computed_frame_rate()); // frame by second

        const Com::TriggerMode trigger_mode = get_trigger_mode();

        // In gate mode, we cannot calculate the number of frames we will get per second
        if(trigger_mode == Com::TriggerMode::TRIGGER_EXTERNAL_GATE)
        {
            m_frames_number_by_readout_call = 1;
        }
        else
        {
            double frame_number_by_readout_call = (frame_rate * static_cast<double>(m_time_between_readout_calls_ms)) / 1000.0; // 1 sec = 1000 ms 

            /// To avoid the lost of frames during the acquisition, we apply a coefficient to the computed frames number by readout call.
            frame_number_by_readout_call *= FRAMES_NUMBER_BY_READOUT_CALL_COEFFICIENT;

            m_frames_number_by_readout_call = static_cast<uint32_t>(frame_number_by_readout_call);
            
            // read the minimum of one frame
            if(m_frames_number_by_readout_call < 1)
                m_frames_number_by_readout_call = 1;
        }
        
        MYTHEN_TASK_INFO_STREAM << "max computed frame rate : "         << frame_rate                      << std::endl;
        MYTHEN_TASK_INFO_STREAM << "m_frames_number_to_be_acquired  : " << m_frames_number_to_be_acquired  << std::endl;
        MYTHEN_TASK_INFO_STREAM << "m_frames_number_by_readout_call : " << m_frames_number_by_readout_call << std::endl;

        // starting the acquisition...
        simple_command(Com::ServerCmd::CMD_START, false, "Acquisition started", "CollectTask::start_acquisition");
    }

    MYTHEN_TASK_INFO_STREAM << "CollectTask::start_acquisition - [END]" << std::endl;
}

/*******************************************************************
 * \brief Stop the acquisition
 *******************************************************************/
void CollectTask::stop_acquisition(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_acquisition_lock);

    MYTHEN_TASK_INFO_STREAM << "CollectTask::stop_acquisition - [BEGIN]" << std::endl;

    int32_t status = get_dcs_status();

    // check that there is an acquisition
    if(status & STATUS_ACQUISITION_MASK)
    {
        simple_command(Com::ServerCmd::CMD_STOP, false, "Acquisition stopped", "CollectTask::stop_acquisition");
    }

    // check if there is a saved frames number to restore in the hardware
    restore_saved_frames_number();

    MYTHEN_TASK_INFO_STREAM << "CollectTask::stop_acquisition - [END]" << std::endl;
}

/*******************************************************************
 * \brief collect the data from the acquisition
 * \return the acquisition data
 *         (can be empty - must be checked by the caller)
 *******************************************************************/
AcquisitionData CollectTask::collect(void)
{
    if(m_frames_number_to_be_acquired > 0)
    {
        // check that there is an available data
        int32_t status = get_dcs_status();

        if(status & STATUS_NO_DATA_FOR_READOUT)
        {
            MYTHEN_TASK_DEBUG_STREAM << "CollectTask::collect - no available data for readout" << std::endl;
            return AcquisitionData(); // empty object
        }

        int32_t nb_Frames_to_read = 0;

        // we take the minimum value between m_frames_number_to_be_acquired and m_frames_number_by_readout_call
        nb_Frames_to_read = (m_frames_number_to_be_acquired > m_frames_number_by_readout_call) ? 
                               m_frames_number_by_readout_call : m_frames_number_to_be_acquired;

        // get the readout data
        std::vector< std::vector<int32_t> > readout_data;

        readout_data = get_readout_data(nb_Frames_to_read);

        const size_t nb_Frames_read = readout_data.size();

        // keep the value, to fill the Acquisition data object
        uint32_t first_frame_index = get_acquired_frames_number();

        if(m_acquisition_in_snap_mode)
        {
            INFO_STREAM << "CollectTask::collect - "
                        << "frames indexes [" << first_frame_index << "->" << (first_frame_index + nb_Frames_read -1) << "]) " 
                        << "number (" << nb_Frames_read << ") size (" << readout_data[0].size() << ")" << std::endl;
        }

        // compute the frames number to keep reading
        m_frames_number_to_be_acquired -= nb_Frames_read;

        // compute the acquired frames number, only in snap mode
        if(m_acquisition_in_snap_mode)
        {
            set_acquired_frames_number(first_frame_index + nb_Frames_read);
        }
    
        AcquisitionData acquisition_data = AcquisitionData(readout_data, first_frame_index);

        // save the last frame to fill the spectrum in TANGO
        set_last_frame(acquisition_data.get_last_frame());

        // in snap mode, we return the frames ; in start mode, we return an empty object because we never save the frames data in this case.   
        return (m_acquisition_in_snap_mode) ? acquisition_data : AcquisitionData();
    }
    else
    {
        restore_saved_frames_number();

        return AcquisitionData(); // empty object
    }
}

/*******************************************************************
 * \brief When an acquisition is finished, we need to restore
 *        the previous frames number value in the hardware because
 *        to manage the start command, the value was changed.
 *******************************************************************/
void CollectTask::restore_saved_frames_number(void)
{
    // check if there is a saved frames number to restore in the hardware after an acquisition
    if(m_saved_frames_number != 0)
    {
        // verify there is no acquisition running
        int32_t status = get_dcs_status();

        if((status & STATUS_ACQUISITION_MASK) == 0)
        {
            set_frames_number(m_saved_frames_number);
            m_saved_frames_number = 0;
        }
    }
}

/*******************************************************************
 * \brief Check if the acquisition is completed and if we can stop
 *        the acquisition.
 * \return true if the acquisition is completed
 *******************************************************************/
bool CollectTask::acquisition_is_completed(void) const
{
    return (m_frames_number_to_be_acquired == 0);
}

/*******************************************************************
 * \brief force the acquisition to be completed
 *******************************************************************/
void CollectTask::force_acquisition_completed(void)
{
    m_frames_number_to_be_acquired = 0;
}

/*******************************************************************
 * \brief get the last frame received from the hardware
 *        the acquisition.
 * \return the last frame
 *******************************************************************/
AcquisitionData CollectTask::get_last_frame(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_last_frame_lock);
    return m_last_acquisition_data;
}

/*******************************************************************
 * \brief set the last frame received from the hardware
 *        the acquisition.
 * \param[in] in_last_acquisition_data Last frame received
 *******************************************************************/
void CollectTask::set_last_frame(const AcquisitionData & in_last_acquisition_data)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_last_frame_lock);
    m_last_acquisition_data = in_last_acquisition_data;
}

/*******************************************************************
 * \brief Load predefined settings for Cu
 *******************************************************************/
void CollectTask::load_predefined_setting_Cu(void) const
{
    simple_command(Com::ServerCmd::CMD_LOAD_SETTINGS_CU, true, "Loaded Cu settings", "CollectTask::load_predefined_setting_Cu");
}

/*******************************************************************
 * \brief Load predefined settings for Mo
 *******************************************************************/
void CollectTask::load_predefined_setting_Mo(void) const
{
    simple_command(Com::ServerCmd::CMD_LOAD_SETTINGS_MO, true, "Loaded Mo settings", "CollectTask::load_predefined_setting_Mo");
}

/*******************************************************************
 * \brief Load predefined settings for Cr
 *******************************************************************/
void CollectTask::load_predefined_setting_Cr(void) const
{
    simple_command(Com::ServerCmd::CMD_LOAD_SETTINGS_CR, true, "Loaded Cr settings", "CollectTask::load_predefined_setting_Cr");
}

/*******************************************************************
 * \brief Load predefined settings for Ag
 *******************************************************************/
void CollectTask::load_predefined_setting_Ag(void) const
{
    simple_command(Com::ServerCmd::CMD_LOAD_SETTINGS_AG, true, "Loaded Ag settings", "CollectTask::load_predefined_setting_Ag");
}

//============================================================================================================
/*******************************************************************
 * \brief Set the exposure time
 * \param[in]  in_exposureTime_100ns exposure time in units of 100 ns
 *******************************************************************/
void CollectTask::set_exposure_time(const int64_t & in_exposureTime_100ns) const
{
    // check the framerate before changing the value
    check_frame_rate(Com::TriggerMode::TRIGGER_CURRENT, 
                     BITS_MODE_CURRENT                , 
                     in_exposureTime_100ns            , 
                     DELAY_AFTER_FRAME_CURRENT        , 
                     DELAY_BEFORE_FRAME_CURRENT       );

    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    set_time(Com::ServerCmd::SET_TIME, in_exposureTime_100ns, "Set exposure time", "CollectTask::set_exposure_time");
    log_max_computed_frame_rate();
}

/*******************************************************************
 * \brief Get the exposure time
 * \return exposure time in units of 100 ns
 *******************************************************************/
int64_t CollectTask::get_exposure_time(void) const
{
    return get_time(Com::ServerCmd::GET_TIME, "Exposure time", "CollectTask::get_exposure_time");
}

/*******************************************************************
 * \brief Set the delay after a frame
 * \param[in]  in_delay_100ns  delay in units of 100 ns
 *******************************************************************/
void CollectTask::set_delay_after_frame(const int64_t & in_delay_100ns) const
{
    // check the framerate before changing the value
    check_frame_rate(Com::TriggerMode::TRIGGER_CURRENT, 
                     BITS_MODE_CURRENT                , 
                     EXPOSURE_TIME_CURRENT            , 
                     in_delay_100ns                   , 
                     DELAY_BEFORE_FRAME_CURRENT       );

    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    set_time(Com::ServerCmd::SET_DEL_AFTER, in_delay_100ns, "Set delay between frames", "CollectTask::set_delay_after_frame");
    log_max_computed_frame_rate();
}

/*******************************************************************
 * \brief Get the delay after a frame
 * \return delay in units of 100 ns
 *******************************************************************/
int64_t CollectTask::get_delay_after_frame(void) const
{
    return get_time(Com::ServerCmd::GET_DEL_AFTER, "Delay between frames", "CollectTask::get_delay_after_frame");
}

/**************************************************************************
 * \brief Set the delay before frame (between trigger and start of measure)
 * \param[in]  in_delay_100ns  delay in units of 100 ns
 **************************************************************************/
void CollectTask::set_delay_before_frame(const int64_t & in_delay_100ns) const
{
    // check the framerate before changing the value
    check_frame_rate(Com::TriggerMode::TRIGGER_CURRENT, 
                     BITS_MODE_CURRENT                , 
                     EXPOSURE_TIME_CURRENT            , 
                     DELAY_AFTER_FRAME_CURRENT        , 
                     in_delay_100ns                   );

    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    set_time(Com::ServerCmd::SET_DEL_BEFORE, in_delay_100ns, "Set delay before frame", "CollectTask::set_delay_before_frame");
    log_max_computed_frame_rate();
}

/**************************************************************************
 * \brief Get the delay before frame (between trigger and start of measure)
 * \return delay in units of 100 ns
 **************************************************************************/
int64_t CollectTask::get_delay_before_frame(void) const
{
    return get_time(Com::ServerCmd::GET_DEL_BEFORE, "Delay before frame", "CollectTask::get_delay_before_frame");
}

/*******************************************************************
 * \brief Set the frames number within an acquisition
 * \param[in]  in_framesNumber frames number
 *******************************************************************/
void CollectTask::set_frames_number(const int32_t & in_framesNumber) const
{
    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    set_int32(Com::ServerCmd::SET_FRAMES, in_framesNumber, "Set frames number", "", "CollectTask::set_frames_number");
}

/*******************************************************************
 * \brief Get the frames number within an acquisition
 * \return the frames number within an acquisition
 *******************************************************************/
int32_t CollectTask::get_frames_number(void) const
{
    return get_int32(Com::ServerCmd::GET_FRAMES, "Frames number", "", "CollectTask::get_frames_number");
}

/**********************************************************************
 * \brief Check if the frames number correspond to a start acquisition
 * \param[in]  in_framesNumber frames number to check
 * \return true if the frame number was changed for a start acquisition
 **********************************************************************/
bool CollectTask::is_start_acquisition_frames_number(const int32_t & in_framesNumber) const
{
    return (in_framesNumber == FRAMES_NUMBER_MAXIMUM_VALUE);
}

/*******************************************************************
 * \brief Set the gates number for one frame
 * \param[in]  in_gatesNumber Gates number
 *******************************************************************/
void CollectTask::set_gates_number_by_frame(const int32_t & in_gatesNumber) const
{
    set_int32(Com::ServerCmd::SET_GATES_NB, in_gatesNumber, "Set gates number by frame", "", "CollectTask::set_gates_number_by_frame");
}

/*******************************************************************
 * \brief Get the gates number for one frame
 * \return Gates number
 *******************************************************************/
int32_t CollectTask::get_gates_number_by_frame(void) const
{
    return get_int32(Com::ServerCmd::GET_GATES_NB, "Gates number by frame", "", "CollectTask::get_gates_number_by_frame");
}

/*******************************************************************
 * \brief Set the number of bits resolution (4, 8, 16, 24)
 * \param[in]  in_bitsMode number of bits
 *******************************************************************/
void CollectTask::set_number_of_bits(const int32_t & in_bitsMode) const
{
    // check the framerate before changing the value
    check_frame_rate(Com::TriggerMode::TRIGGER_CURRENT, 
                     in_bitsMode                      , 
                     EXPOSURE_TIME_CURRENT            , 
                     DELAY_AFTER_FRAME_CURRENT        , 
                     DELAY_BEFORE_FRAME_CURRENT       );

    // check if the number of bits is a valid one - search the bits mode in the bits mode container
    const std::vector<int>::const_iterator iterator = find(BITS_MODES.begin(), BITS_MODES.end(), in_bitsMode);

    // found it
    if (iterator != BITS_MODES.end()) 
    {
        set_int32(Com::ServerCmd::SET_N_BITS, in_bitsMode, "Set number of bits", "", "CollectTask::set_number_of_bits");
        log_max_computed_frame_rate();
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Impossible to found the bits mode " << in_bitsMode << std::endl;

        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "CollectTask::set_number_of_bits()");
    }
}

/*******************************************************************
 * \brief Get the number of bits resolution (4, 8, 16, 24)
 * \return the number of bits resolution
 *******************************************************************/
int32_t CollectTask::get_number_of_bits(void) const
{
    return get_int32(Com::ServerCmd::GET_N_BITS, "Number of bits", "", "CollectTask::get_number_of_bits");
}

/*******************************************************************
 * \brief Get the readout time of a bits mode (4, 8, 16, 24)
 * \param[in]  in_bitsMode bits mode
 * \return readout time in units of 100 ns
 *******************************************************************/
int64_t CollectTask::get_readout_time_of_bits_mode(const int32_t in_bitsMode) const
{
    std::vector<int64_t> readoutTimes      ;
    int64_t              readoutTime_100ns = 0LL;

    readoutTimes = get_data_vector<int64_t>(Com::ServerCmd::GET_READ_OUT_TIMES, BITS_MODES_NUMBER, "CollectTask::get_readout_time_of_bits_mode");

    // search the bits mode
    const std::vector<int>::const_iterator iterator = find(READOUT_BITS_MODES.begin(), READOUT_BITS_MODES.end(), in_bitsMode);

    // found it
    if (iterator != READOUT_BITS_MODES.end()) 
    {
        readoutTime_100ns = readoutTimes[iterator - READOUT_BITS_MODES.begin()];

        show_information(in_bitsMode      , "Get readout time for bits mode");
        show_information(readoutTime_100ns, "Readout time", "(100 ns unit)" );
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Impossible to found the bits mode " << in_bitsMode << std::endl;

        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "CollectTask::get_readout_time_of_bits_mode()");
    }

    return readoutTime_100ns;
}

/*******************************************************************
 * \brief Get the readout time of the current bits mode
 * \return readout time in units of 100 ns
 *******************************************************************/
int64_t CollectTask::get_readout_time(void) const
{
    return get_readout_time_of_bits_mode(get_number_of_bits());
}

/**********************************************************************
 * \brief Set the gate activation state
 * \param[in]  in_IsEnabled functionnality needs to be turned on or off
 **********************************************************************/
void CollectTask::set_gate(const bool in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_GATED_MEASURE_MODE, in_IsEnabled, "Set gated mode activation", "CollectTask::set_gate");
}

/*******************************************************************
 * \brief Get the gate state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_gate(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_GATED_MEASURE_MODE, "Gated mode activation", "CollectTask::get_gate");
}

/*******************************************************************
 * \brief Set the trigger activation state
 * \param[in]  in_IsEnabled Must be turned on or off
 *******************************************************************/
void CollectTask::set_trigger(const bool in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_TRIGGER_MODE, in_IsEnabled, "Set trigger mode activation", "CollectTask::set_trigger");
}

/*******************************************************************
 * \brief Get the trigger state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_trigger(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_TRIGGER_MODE, "Trigger mode activation", "CollectTask::get_trigger");
}

/*******************************************************************
 * \brief Set the continuous trigger activation state
 * \param[in]  in_IsEnabled Must be turned on or off
 *******************************************************************/
void CollectTask::set_continuous_trigger(const bool in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_CONT_TRIGGER_MODE, in_IsEnabled, "Set continuous trigger mode activation", "CollectTask::set_continuous_trigger");
}

/*******************************************************************
 * \brief Get the continuous trigger state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_continuous_trigger(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_CONT_TRIGGER_MODE, "Continuous trigger mode activation", "CollectTask::get_continuous_trigger");
}

/*******************************************************************
 * \brief Set the trigger mode
 * \param[in] in_triggerMode Trigger mode
 *******************************************************************/
void CollectTask::set_trigger_mode(const Com::TriggerMode & in_triggerMode) const
{
    // check the framerate before changing the value
    check_frame_rate(in_triggerMode            , 
                     BITS_MODE_CURRENT         , 
                     EXPOSURE_TIME_CURRENT     , 
                     DELAY_AFTER_FRAME_CURRENT , 
                     DELAY_BEFORE_FRAME_CURRENT);

    const bool unactivated = false;
    const bool activated   = true ;

    // before, reset all boolean in the device because the call of commands can reset boolean values (bug in the hardware)
    set_gate(unactivated);
    set_trigger(unactivated);
    set_continuous_trigger(unactivated);

    // Now, change only the true values using device commands
    if(in_triggerMode != Com::TriggerMode::TRIGGER_INTERNAL)
    {
        if(in_triggerMode == Com::TriggerMode::TRIGGER_EXTERNAL_SINGLE  ) 
        {
            set_trigger(activated);
        }
        else
        if(in_triggerMode == Com::TriggerMode::TRIGGER_EXTERNAL_MULTIPLE)
        {
            set_continuous_trigger(activated);
        }
        else
        if(in_triggerMode == Com::TriggerMode::TRIGGER_EXTERNAL_GATE    )
        {
            set_gate(activated);
        }
    }    

    show_information(TRIGGER_MODES_LABELS[in_triggerMode], "Set trigger mode");

    log_max_computed_frame_rate();
}

/*******************************************************************
 * \brief Get the trigger mode
 * \return the trigger mode
 *******************************************************************/
Com::TriggerMode CollectTask::get_trigger_mode(void) const
{
    bool temp;
    Com::TriggerMode triggerMode;

    temp = get_gate();

    // GATE TRIGGER
    if(temp)
    {
        triggerMode = Com::TriggerMode::TRIGGER_EXTERNAL_GATE;
    }
    else
    {
        temp = get_continuous_trigger();

        // CONTINUOUS TRIGGER
        if(temp)
        {
            triggerMode = Com::TriggerMode::TRIGGER_EXTERNAL_MULTIPLE;
        }
        else
        {
            temp = get_trigger();

            // SINGLE TRIGGER
            if(temp)
            {
                triggerMode = Com::TriggerMode::TRIGGER_EXTERNAL_SINGLE;
            }
            else
            // INTERNAL TRIGGER
            {
                triggerMode = Com::TriggerMode::TRIGGER_INTERNAL;
            }
        }
    }

    show_information(TRIGGER_MODES_LABELS[triggerMode], "Get trigger mode");

    return triggerMode;
}

//============================================================================================================
/*******************************************************************
 * \brief Get the status
 * \return current status
 *******************************************************************/
int32_t CollectTask::get_dcs_status(void) const
{
    int32_t out_status;
    int32_t error     ;
    bool    result    = m_Com->get_command(Com::ServerCmd::GET_STATUS, out_status, &error);

    // log management
    if(result)
    {
        string StatusInBit;

        for(int bitnb = 31; bitnb >= 0 ; bitnb--)
        {
            StatusInBit += (out_status & (1 << bitnb)) ? "1" : "0";
        }

        show_information(StatusInBit, "Binary status");

        std::vector<string> statusLabels;

        if(out_status & STATUS_MASK)
        {
            if(out_status & STATUS_ACQUISITION_RUNNING)
            {
                statusLabels.push_back("Acquisition is running");
            }

            if(out_status & STATUS_ACQUISITION_EXPOSURE_INACTIVE)
            {
                statusLabels.push_back("Acquisition is running and exposure is inactive");
            }

            if(out_status & STATUS_NO_DATA_FOR_READOUT)
            {
                statusLabels.push_back("No data for readout (empty buffer)");
            }
        }
        else
        {
            statusLabels.push_back("Nothing is running");
        }

        show_vector_information(statusLabels, "Status in labels");
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              "CollectTask::get_dcs_status");
    }

    return out_status;
}

/**********************************************************************
 * \brief Get the number of detected photons for each channels
 * \param[in]  in_nbFrames     Number of frames to be processed
 * \return Readout data from the DCS for each frame
 **********************************************************************/
std::vector< std::vector<int32_t> > CollectTask::get_readout_data(int32_t in_nbFrames) const
{
    std::vector< std::vector<int32_t> > out_readoutData;

    int32_t   error           ;
    bool      result          = false;
    int       readoutDataSize = in_nbFrames * m_all_modules_channels;
    int32_t * readoutData     = new int32_t[readoutDataSize];

    out_readoutData.clear();

    if(in_nbFrames > 1)
    {
        result = m_Com->action_command(Com::ServerCmd::CMD_READ_OUT_NB_FRAME, readoutData, readoutDataSize, &error, in_nbFrames);
    }
    else
    {
        result = m_Com->action_command(Com::ServerCmd::CMD_READ_OUT_ONE_FRAME, readoutData, readoutDataSize, &error);
    }

    if(result)
    {
        for(int32_t frameIndex = 0 ; frameIndex < in_nbFrames ; frameIndex++)
        {
            out_readoutData.push_back(std::vector<int32_t>(readoutData + (frameIndex * m_all_modules_channels),
                                                           readoutData + (frameIndex * m_all_modules_channels) + m_all_modules_channels));
        }
        // show_vector_information(out_readoutData, "Readout");
    }

    delete [] readoutData;
    
    // exception management
    if(!result)
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              "CollectTask::get_dcs_status");
    }

    return out_readoutData;
}

//============================================================================================================
/*******************************************************************
 * \brief Set the bad channel interpolation activation state
 * \param[in]  in_IsEnabled Must be turned on or off
 *******************************************************************/
void CollectTask::set_bad_channel_interpolation(const bool & in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_BAD_CHAN_INTERP, in_IsEnabled, "Set bad channel interpolation activation", "CollectTask::set_bad_channel_interpolation");
}

/*******************************************************************
 * \brief Get the bad channel interpolation activation state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_bad_channel_interpolation(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_BAD_CHAN_INTERP, "Bad channel interpolation activation", "CollectTask::get_bad_channel_interpolation");
}

/*******************************************************************
 * \brief Set the rate correction activation state
 * \param[in]  in_IsEnabled Must be turned on or off
 *******************************************************************/
void CollectTask::set_rate_correction(const bool & in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_RATE_CORRECTION, in_IsEnabled, "Set rate correction activation", "CollectTask::set_rate_correction");
}

/*******************************************************************
 * \brief Get the rate correction activation state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_rate_correction(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_RATE_CORRECTION, "Rate correction activation", "CollectTask::get_rate_correction"); 
}

//============================================================================================================
/*******************************************************************
 * \brief Set the energy for activated modules
 * \param[in]  in_energy X-ray energy in keV
 *******************************************************************/
void CollectTask::set_modules_energy(const float & in_energy) const
{
    set_float(Com::ServerCmd::SET_ENERGY, in_energy, true, "Set energy for activated modules", "keV", "CollectTask::set_modules_energy");
}

/*******************************************************************
 * \brief Set the energy for only one module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \param[in]  in_energy       X-ray energy in keV
 *******************************************************************/
void CollectTask::set_module_energy(const int32_t & in_module_index, const float & in_energy) const
{
    // selecting the module
    select_module(in_module_index);
    set_float(Com::ServerCmd::SET_ENERGY, in_energy, false, "Set energy for one module", "keV", "CollectTask::set_module_energy");
}

/*******************************************************************
 * \brief Get the energy for activated modules
 * \return X-ray energy in keV
*******************************************************************/
std::vector<float> CollectTask::get_modules_energy(void) const
{
    return get_float_vector(Com::ServerCmd::GET_ENERGY, true, "Active modules energy", "keV", "CollectTask::get_modules_energy");
}

/*******************************************************************
 * \brief Get the energy for a module
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_energy(const int32_t & in_module_index) const
{
    std::vector<float> energies = get_modules_energy();
    return energies[in_module_index];
}

/*******************************************************************
 * \brief Get the minimum energy for activated modules
 * \return X-ray energy in keV
*******************************************************************/
std::vector<float> CollectTask::get_modules_energy_min(void) const
{
    return get_float_vector(Com::ServerCmd::GET_ENERGY_MIN, false, "Active modules minimum energy", "keV", "CollectTask::get_modules_energy_min");
}

/*******************************************************************
 * \brief Get the minimum energy of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_energy_min(const int32_t & in_module_index) const
{
    std::vector<float> energies = get_modules_energy_min();
    return energies[in_module_index];
}

/*******************************************************************
 * \brief Get the maximum energy for activated modules
 * \return X-ray energy in keV
*******************************************************************/
std::vector<float> CollectTask::get_modules_energy_max(void) const
{
    return get_float_vector(Com::ServerCmd::GET_ENERGY_MAX, false, "Active modules maximum energy", "keV", "CollectTask::get_modules_energy_max");
}

/*******************************************************************
 * \brief Get the maximum energy of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_energy_max(const int32_t & in_module_index) const
{
    std::vector<float> energies = get_modules_energy_max();
    return energies[in_module_index];
}

/*******************************************************************
 * \brief Set the threshold energy for activated modules
 * \param[in]  in_threshold Threshold energy in keV
 *******************************************************************/
void CollectTask::set_modules_threshold(const float & in_threshold) const
{
    set_float(Com::ServerCmd::SET_K_THRESH, in_threshold, true, "Set threshold energy for activated modules", "keV", "CollectTask::set_modules_threshold");
}

/*******************************************************************
 * \brief Set the threshold energy  for only one module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \param[in]  in_threshold    Threshold energy in keV
 *******************************************************************/
void CollectTask::set_module_threshold(const int32_t & in_module_index, const float & in_threshold) const
{
    // selecting the module
    select_module(in_module_index);
    set_float(Com::ServerCmd::SET_K_THRESH, in_threshold, false, "Set threshold energy for one module", "keV", "CollectTask::set_module_threshold");
}

/*******************************************************************
 * \brief Get the threshold energy for activated modules
 * \return the threshold energy for activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_threshold(void) const
{
    return get_float_vector(Com::ServerCmd::GET_K_THRESH, true, "Active modules threshold energy", "keV", "CollectTask::get_modules_threshold");
}

/*******************************************************************
 * \brief Get the threshold energy for a module
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_threshold(const int32_t & in_module_index) const
{
    std::vector<float> thresholds = get_modules_threshold();
    return thresholds[in_module_index];
}

/*******************************************************************
 * \brief Get the minimum threshold energy for activated modules
 * \return the threshold energy for activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_threshold_min(void) const
{
    return get_float_vector(Com::ServerCmd::GET_K_THRESH_MIN, false, "Active modules minimum threshold energy", "keV", "CollectTask::get_modules_threshold_min");
}

/*******************************************************************
 * \brief Get the minimum threshold energy of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_threshold_min(const int32_t & in_module_index) const
{
    std::vector<float> thresholds = get_modules_threshold_min();
    return thresholds[in_module_index];
}

/*******************************************************************
 * \brief Get the maximum threshold energy for activated modules
 * \return the threshold energy for activated modules
*******************************************************************/
std::vector<float> CollectTask::get_modules_threshold_max(void) const
{
    return get_float_vector(Com::ServerCmd::GET_K_THRESH_MAX, false, "Active modules maximum threshold energy", "keV", "CollectTask::get_modules_threshold_max");
}

/*******************************************************************
 * \brief Get the maximum threshold energy of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return X-ray energy in keV
*******************************************************************/
float CollectTask::get_module_threshold_max(const int32_t & in_module_index) const
{
    std::vector<float> thresholds = get_modules_threshold_max();
    return thresholds[in_module_index];
}

//============================================================================================================
// FLATFIELD
//============================================================================================================
/*******************************************************************
 * \brief Set the flatfield correction activation state
 * \param[in]  in_IsEnabled Must be turned on or off
 *******************************************************************/
void CollectTask::set_flatfield_correction(const bool & in_IsEnabled) const
{
    set_activation_boolean(Com::ServerCmd::SET_FLATFIELD_CORRECTION, in_IsEnabled, "Set flatfield correction activation", "CollectTask::set_flatfield_correction");
}

/*******************************************************************
 * \brief Get the flatfield correction activation state
 * \return is the functionnality turned on or off ?
 *******************************************************************/
bool CollectTask::get_flatfield_correction(void) const
{
    return get_activation_boolean(Com::ServerCmd::GET_FLATFIELD_CORRECTION, "Flatfield correction activation", "CollectTask::get_flatfield_correction");
}

/*******************************************************************
 * \brief Store the flatfield in a slot
 * \param[in]  in_slot_index   Slot index [0...3]
 * \param[in]  in_flatfield    flatfield (NCHAN * 32bits)
 *******************************************************************/
void CollectTask::store_flatfield(const int32_t              & in_slot_index,
                                  const std::vector<int32_t> & in_flatfield ) const
{
    select_all_modules();

    const string in_callerName = "CollectTask::store_flatfield";
    const string in_label      = "Set flatfield";
    int32_t error  ;

    // we need to create a vector of uint8 to use the binary command
    size_t nb_bytes = in_flatfield.size() * sizeof(int32_t); 
    
    std::vector<uint8_t> temp;
    temp.resize(nb_bytes);

    size_t bytes_index = 0;

    for(size_t channel_index = 0 ; channel_index < in_flatfield.size() ; channel_index++)
    {
        temp[bytes_index++] = (in_flatfield[channel_index]      ) & 0x000000FF;
        temp[bytes_index++] = (in_flatfield[channel_index] >> 8 ) & 0x000000FF;
        temp[bytes_index++] = (in_flatfield[channel_index] >> 16) & 0x000000FF;
        temp[bytes_index++] = (in_flatfield[channel_index] >> 24) & 0x000000FF;
    }

    bool result = m_Com->binary_command(Com::ServerCmd::SET_FLATFIELD, 
                                        temp.data()                  ,
                                        temp.size()                  , 
                                        &error                       ,
                                        in_slot_index                );
    if(result) 
    {
        MYTHEN_TASK_DEBUG_STREAM << in_label;
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief load a stored flatfield of a slot
 * \param[in]  in_slot_index   Slot index [0...3]
 *******************************************************************/
void CollectTask::set_loaded_flatfield(const int32_t & in_slot_index) const
{
    select_all_modules();

    int32_t      error         ;
    const string in_callerName = "CollectTask::set_loaded_flatfield";
    const string in_label      = "Load flatfield";
    bool         result        = m_Com->set_command(Com::ServerCmd::CMD_LOAD_FLATFIELD, &error, in_slot_index);

    // log management
    if(result)
    {
        show_information(in_slot_index, in_label);
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief Refresh the current loaded flatfield in the cache
 * \return nothing
 *******************************************************************/
void CollectTask::refresh_loaded_flatfield(void)
{
    select_all_modules();

    std::vector<int32_t> flatfield;

    int32_t   error    ;
    bool      result   ;
    int       DataSize = m_all_modules_channels * sizeof(int32_t);
    uint8_t * Data     = new uint8_t[DataSize];

    flatfield.clear();

    result = m_Com->get_command(Com::ServerCmd::GET_FLATFIELD, Data, DataSize, &error) ;

    if(result)
    {
        size_t nb_elements = DataSize / sizeof(int32_t);
        
        flatfield.resize(nb_elements);

        for(size_t index = 0 ; index < nb_elements ; index++)
        {
            uint8_t * FirstByte = Data + (index * sizeof(int32_t));

            flatfield[index] = ((*(FirstByte + 3)) << 24) | 
                               ((*(FirstByte + 2)) << 16) |
                               ((*(FirstByte + 1)) << 8 ) |
                               ((*(FirstByte    ))      ) ;
        }
    }

    delete [] Data;
    
    // exception management
    if(!result)
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              "CollectTask::get_loaded_flatfield");
    }

    m_flatfield = flatfield;
}

/*******************************************************************
 * \brief Get the current loaded flatfield
 * \return current loaded flatfield
 *******************************************************************/
std::vector<int32_t> CollectTask::get_loaded_flatfield(void)
{
    // need to force an update at start ?
    if(m_flatfield.size() == 0)
    {
        refresh_loaded_flatfield();
    }

    return m_flatfield;
}

/*******************************************************************
 * \brief Get the current flatfield of a module
 * \param[in]  in_module_index Module index [0...active modules max[
 * \return The current flatfield of a module
*******************************************************************/
std::vector<int32_t> CollectTask::get_module_loaded_flatfield(const int32_t & in_module_index)
{
    std::vector<int32_t> total_flatfield = get_loaded_flatfield();

    // we must cut the flatfield to get only the module flatfield
    std::vector<int32_t> module_flatfield(total_flatfield.begin() + m_modules_channels_start_index[in_module_index], 
                                          total_flatfield.begin() + m_modules_channels_start_index[in_module_index] + m_modules_channels[in_module_index]);

    return module_flatfield; // return a copy of the vector
}

/*******************************************************************
 * \brief Setter of the flatfield path
 * \param[in] in_flatfield_path flatfield path
*******************************************************************/
void CollectTask::set_flatfield_path(const std::string & in_flatfield_path)
{
    m_flatfield_path = in_flatfield_path;
}

/*******************************************************************
 * \brief Getter of the flatfield path
 * \return flatfield path
*******************************************************************/
std::string CollectTask::get_flatfield_path(void)
{
    return m_flatfield_path;
}

/*******************************************************************
 * \brief Getter of the flatfield name
 * \return flatfield name
*******************************************************************/
std::string CollectTask::get_flatfield_name(void)
{
    return m_last_flatfield_name;
}

/*******************************************************************
 * \brief Setter of the flatfield name
 * \param[in] in_name flatfield alias name
*******************************************************************/
void CollectTask::set_flatfield_name(const std::string & in_name)
{
    m_last_flatfield_name = in_name;
}

//============================================================================================================
/*******************************************************************
 * \brief Get the error string
 *******************************************************************/
std::string CollectTask::get_error_string(int32_t in_ErrorId) const
{
    return m_Com->get_error_string(in_ErrorId);
}

/*******************************************************************
 * \brief Get informations about DCS4 and active modules
 * \return informations about DCS4 and active modules
*******************************************************************/
std::vector<std::pair<std::string, std::string> > CollectTask::get_versions_informations(void) const
{
    std::vector<std::pair<std::string, std::string> > informations;

    std::string         tempString      ;
    vector<std::string> tempVectorString;
    std::size_t         index           ;

    informations.clear();

    // DCS4 serial number
    {
        std::stringstream tempStream;
        tempStream << get_serial_number();
        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_DCS4_SERIAL_NUMBER, tempStream.str()));
    }

    // DCS4 FPGA firmware
    {
	    tempString = get_firmware_version();
        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_DCS4_FPGA_FIRMWARE, tempString));
    }

    // DCS4 assembly date
    {
	    tempString = get_assembly_date();
        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_DCS4_ASSEMBLY_DATE, tempString));
    }

    // DCS4 software version
    {
	    tempString = get_software_version();
        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_DCS4_SOFTWARE_VERSION, tempString));
    }

    // Active modules FPGA firmwares
    {
        std::stringstream tempStream;

        tempVectorString = get_modules_firmware_version();

        for(index = 0 ; index < tempVectorString.size() ; index++)
        {
            if(index > 0)
            {
                tempStream << ", ";
            }

            tempStream << "(" << (index + 1) << ") " << tempVectorString[index];
        }

        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_MODULES_FIRMWARES, tempStream.str()));
    }

    // Active modules serial numbers
    {
        std::stringstream tempStream;

        tempVectorString = get_modules_serial_number();

        for(index = 0 ; index < tempVectorString.size() ; index++)
        {
            if(index > 0)
            {
                tempStream << ", ";
            }

            tempStream << "(" << (index + 1) << ") " << tempVectorString[index];
        }

        informations.push_back(std::pair<std::string, std::string>(INFO_LABEL_MODULES_SERIAL_NUMBERS, tempStream.str()));
    }

    return informations;
}

//============================================================================================================
/************************************************************************
 * \brief show the action
 * \param[in] in_label defines the action
*************************************************************************/
void CollectTask::show_action(const std::string & in_label) const
{
    MYTHEN_TASK_DEBUG_STREAM << "[" << in_label << "]" << std::endl;
}

/*******************************************************************
 * \brief throw an exception
 * \param reason The exception DevError object reason field
 * \param desc   The exception DevError object desc   field
 * \param origin The exception DevError object origin field
 *******************************************************************/
void CollectTask::throw_tango_exception(const char * reason,
                                        const char * desc  ,
                                        const char * origin) const
{
#ifdef MYTHEN_TANGO_INDEPENDANT
    MYTHEN_TASK_INFO_STREAM << "EXCEPTION:" << reason << " - " << desc << " - " << origin << std::endl;

    throw std::string( "throw_tango_exception" ); 
#else
    Tango::Except::throw_exception(reason, desc, origin);
#endif
}

//============================================================================================================
/*********************************************************************
 * \brief Get a string
 * \param[in]  in_cmdId       command to be send to the socket server
 * \param[in]  in_FinalSize   max string size
 * \param[in]  in_NetworkSize network string size (sometimes without
 *                            the final end of string)
 * \param[in]  in_label       label for logging management ("" is we
 *                            do not want the method to log the value)
 * \param[in]  in_callerName  Class and method name of the caller 
 *                            (for exception management)
 * \return the string
 *********************************************************************/
std::string CollectTask::get_string(Com::ServerCmd      in_cmdId      , 
                                    const int32_t       in_finalSize  ,
                                    const int32_t       in_networkSize,
                                    const std::string & in_label      ,
                                    const std::string & in_callerName ) const
{
    bool        result ;
    char *      temp   = new char[in_finalSize];
    int32_t     error  ;
    std::string data   ;

    memset(temp, '\0', in_finalSize);

    result = m_Com->get_command(in_cmdId, temp, in_networkSize, &error);

    if(result)
    {
        data = std::string(temp);
        
        // remove end of line character
        data.erase(std::remove(data.begin(), data.end(), '\n'), data.end());
    }

    delete [] temp;

    // log management
    if(result)
    {
        if(in_label.size() > 0)
        {
            show_information(data, in_label);
        }
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }

    return data;
}

/*******************************************************************
 * \brief Get a 32 bits integer
 * \param[in]  in_cmdId         command to be send to the socket server
 * \param[in]  in_label         label for logging management ("" is we
 *                              do not want the method to log the value)
 * \param[in]  in_optionalScale Defines an optional scale for the data
 * \param[in]  in_callerName    Class and method name of the caller 
 *                              (for exception management)
 * \return a 32 bits integer
 *******************************************************************/
int32_t CollectTask::get_int32(Com::ServerCmd      in_cmdId        ,
                               const std::string & in_label        ,
                               const std::string & in_optionalScale,
                               const std::string & in_callerName   ) const
{
    int32_t error  ;
    int32_t data   = 0;
    bool    result = m_Com->get_command(in_cmdId, data, &error);

    // log management
    if(result)
    {
        if(in_label.size() > 0)
        {
            show_information(data, in_label, in_optionalScale);
        }
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }

    return data;
}

/***************************************************************************
 * \brief Set a 32 bits integer
 * \param[in]  in_cmdId         Command to be send to the socket server
 * \param[in]  in_data          new value
 * \param[in]  in_label         label for logging management ("" is we
 *                              do not want the method to log the value)
 * \param[in]  in_optionalScale Defines an optional scale for the data
 * \param[in]  in_callerName    Class and method name of the caller 
 *                              (for exception management)
 ***************************************************************************/
void CollectTask::set_int32(Com::ServerCmd      in_cmdId        ,
                            const int32_t     & in_data         ,
                            const std::string & in_label        ,
                            const std::string & in_optionalScale,
                            const std::string & in_callerName   ) const
{
    int32_t error  ;
    bool    result = m_Com->set_command(in_cmdId, &error, in_data);

    if(result)
    {
        if(in_label.size() > 0)
        {
            show_information(in_data, in_label, in_optionalScale);
        }
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief Get a float
 * \param[in]  in_cmdId      Command to be send to the socket server
 * \param[in]  in_label      label for logging management ("" is we
 *                           do not want the method to log the value)
 * \param[in]  in_callerName Class and method name of the caller 
 *                           (for exception management)
 * \return a float
 *******************************************************************/
float CollectTask::get_float(Com::ServerCmd      in_cmdId        ,
                             const std::string & in_label        ,
                             const std::string & in_optionalScale,
                             const std::string & in_callerName   ) const
{
    int32_t error  ;
    float   data   = 0.0f;
    bool    result = m_Com->get_command(in_cmdId, data, &error);

    // log management
    if(result)
    {
        if(in_label.size() > 0)
        {
            show_information(data, in_label, in_optionalScale);
        }
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }

    return data;
}

/***************************************************************************
 * \brief Set a float
 * \param[in]  in_cmdId         Command to be send to the socket server
 * \param[in]  in_data          new value
 * \param[in]  in_useSelection  Select all the modules before the command
 * \param[in]  in_label         Label for logging management
 * \param[in]  in_optionalScale Defines an optional scale for the data
 * \param[in]  in_callerName    Class and method name of the caller 
 *                              (for exception management)
 ***************************************************************************/
void CollectTask::set_float(Com::ServerCmd      in_cmdId        ,
                            const float         in_data         ,
                            const bool          in_useSelection ,
                            const std::string & in_label        ,
                            const std::string & in_optionalScale,
                            const std::string & in_callerName   ) const
{
    int32_t error  ;
    bool    result = true;
    
    if(in_useSelection)
    {
        // selecting all modules
        select_all_modules();
    }

    result = m_Com->set_command(in_cmdId, &error, in_data);

    // log management
    if(result)
    {
        show_information(in_data, in_label, in_optionalScale);
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief Get a float vector
 * \param[in]  in_cmdId         command to be send to the socket server
 * \param[in]  in_useSelection  Select all the modules before the command
 * \param[in]  in_label         label for logging management
 * \param[in]  in_optionalScale Defines an optional scale for the data
 * \param[in]  in_callerName    Class and method name of the caller 
 *                              (for exception management)
 * \return a float vector
 *******************************************************************/
std::vector<float> CollectTask::get_float_vector(Com::ServerCmd       in_cmdId        ,
                                                 const bool           in_useSelection ,
                                                 const std::string  & in_label        ,
                                                 const std::string  & in_optionalScale,
                                                 const std::string  & in_callerName   ) const
{
    std::vector<float> data;
    
    if(in_useSelection)
    {
        // selecting all modules
        select_all_modules();
    }

    // we selected all activated modules so we can use m_nb_modules data
    data = get_data_vector<float>(in_cmdId, m_nb_modules, in_callerName);

    show_vector_information(data, in_label, in_optionalScale);

    return data;
}

/***************************************************************************
 * \brief Send a simple command (no parameters)
 * \param[in]  in_cmdId         Command to be send to the socket server
 * \param[in]  in_useSelection  Select all the modules before the command
 * \param[in]  in_label         Label for logging management
 * \param[in]  in_callerName    Class and method name of the caller 
 *                              (for exception management)
 ***************************************************************************/
void CollectTask::simple_command(Com::ServerCmd      in_cmdId       ,
                                 const bool          in_useSelection,
                                 const std::string & in_label       ,
                                 const std::string & in_callerName  ) const
{
    if(in_useSelection)
    {
        // selecting all modules
        select_all_modules();
    }

    int32_t error  ;
    bool    result = m_Com->set_command(in_cmdId, &error);

    // log management
    if(result)
    {
        show_action(in_label);
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/***************************************************************************
 * \brief Set a time
 * \param[in]  in_cmdId       command to be send to the socket server
 * \param[in]  in_delay_100ns Time in units of 100 ns
 * \param[in]  in_label       label for logging management
 * \param[in]  in_callerName  Class and method name of the caller 
 *                            (for exception management)
 * \return true in case of success, false in case of error
 ***************************************************************************/
void CollectTask::set_time(Com::ServerCmd      in_cmdId      ,
                           const int64_t     & in_delay_100ns,
                           const std::string & in_label      ,
                           const std::string & in_callerName ) const
{
    int32_t error  ;
    bool    result = m_Com->set_command(in_cmdId, &error, in_delay_100ns);

    // log management
    if(result)
    {
        show_information(in_delay_100ns, in_label, "(100 ns unit)");
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief Get a time
 * \param[in]  in_cmdId        command to be send to the socket server
 * \param[in]  in_label        label for logging management
 * \param[in]  in_callerName   Class and method name of the caller 
 *                             (for exception management)
 * \return Time in units of 100 ns
 *******************************************************************/
int64_t CollectTask::get_time(Com::ServerCmd      in_cmdId     ,
                              const std::string & in_label     ,
                              const std::string & in_callerName) const
{
    int32_t error       ;
    int64_t delay_100ns = 0LL;
    bool    result      = m_Com->get_command(in_cmdId, delay_100ns, &error);

    // log management
    if(result)
    {
        show_information(delay_100ns, in_label, "(100 ns unit)");
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }

    return delay_100ns;
}

/***************************************************************************
 * \brief Set an activation boolean
 * \param[in]  in_cmdId       command to be send to the socket server
 * \param[in]  in_IsEnabled   enables or disables the functionnality
 * \param[in]  in_label       label for logging management
 * \param[in]  in_callerName  Class and method name of the caller 
 *                            (for exception management)
 ***************************************************************************/
void CollectTask::set_activation_boolean(Com::ServerCmd      in_cmdId     ,
                                         const bool          in_IsEnabled ,
                                         const std::string & in_label     ,
                                         const std::string & in_callerName) const
{
    int32_t error  ;
    int32_t Value  = (in_IsEnabled) ? 1 : 0;
    bool    result = m_Com->set_command(in_cmdId, &error, Value);

    // log management
    if(result)
    {
        show_information(((in_IsEnabled) ? "Enabled" : "Disabled"), in_label);
    } 
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }
}

/*******************************************************************
 * \brief Get an activation boolean
 * \param[in]  in_cmdId        command to be send to the socket server
 * \param[in]  in_label        label for logging management
 * \param[in]  in_callerName   Class and method name of the caller 
 *                             (for exception management)
 * \return true if the functionnality is enabled, false if disabled
 *******************************************************************/
bool CollectTask::get_activation_boolean(Com::ServerCmd      in_cmdId     ,
                                         const std::string & in_label     ,
                                         const std::string & in_callerName) const
{
    int32_t error     ;
    int32_t Value     ;
    bool    isEnabled = false;
    bool    result    = m_Com->get_command(in_cmdId, Value, &error);

    // log management
    if(result)
    {
        isEnabled = (Value == 1);

        show_information(((isEnabled) ? "Enabled" : "Disabled"), in_label);
    }
    else
    // exception management
    {
        std::string msg_error = get_error_string(error);

        throw_tango_exception("TANGO_DEVICE_ERROR" ,
                              msg_error.c_str()    ,
                              in_callerName.c_str());
    }

    return isEnabled;
}

//============================================================================================================
/*******************************************************************
 * \brief convert an int to an hexa string
 * \param in_value  The value to convert
 * \return theinteger in hexa form into a string
 *******************************************************************/
std::string CollectTask::int_to_hex( int32_t in_value )
{
  std::stringstream stream;
  stream << "0x" << std::setfill ('0') << std::setw(sizeof(int32_t)*2) << std::hex << in_value;
  return stream.str();
}

//###########################################################################
}