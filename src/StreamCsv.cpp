/*************************************************************************/
/*! 
 *  \file   StreamCsv.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamCsv.h"

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
StreamCsv::StreamCsv(Tango::DeviceImpl* dev, Controller * in_controller) : Stream(dev, in_controller)
{
    DEBUG_STREAM << "StreamCsv::StreamCsv() - [BEGIN]" << std::endl;
    m_file_index = 1;
    DEBUG_STREAM << "StreamCsv::StreamCsv() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamCsv::~StreamCsv() 
{
}

/*******************************************************************
 * \brief Get the stream type
 * \return the stream type
 *******************************************************************/
enum Stream::Types StreamCsv::get_type(void) 
{
    return CSV_STREAM;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_acquisition_data acquisition data
 *******************************************************************/
void StreamCsv::update(const AcquisitionData & in_acquisition_data)
{
    const size_t    nb_Frames_read    = in_acquisition_data.get_frames_number    ();
    const uint32_t  first_frame_index = in_acquisition_data.get_first_frame_index(); 

    INFO_STREAM << "StreamCsv::update - "
                << "frames indexes [" << first_frame_index << "->" << (first_frame_index + nb_Frames_read -1) << "]) " 
                << "number (" << nb_Frames_read << ") size (" << in_acquisition_data.get_frame(0).size() << ")" << std::endl;

    for(size_t frameIndex = 0 ; frameIndex < nb_Frames_read ; frameIndex++)
    {
        const std::vector<int32_t> & ChannelsData = in_acquisition_data.get_frame(frameIndex);

        char filename[255];
        sprintf(filename, "%s/%s_%06d.csv", m_target_path.c_str(), m_target_file.c_str(), m_file_index);
        std::ofstream output_file(filename, std::ios::out | std::ofstream::binary);
        m_file_index++;

        //Header only at beginning of file
        output_file.seekp(0, ios::end);
        if((long)0 == output_file.tellp())
        {
            output_file<<"frame;"<<std::endl;
        }
        
        //- channel
        if(m_items & STREAM_ITEM_FRAME)
        {
            std::ostream_iterator<int32_t> output_iterator(output_file, ";");
            std::copy(ChannelsData.begin(), ChannelsData.end(), output_iterator);
        }
        
        output_file<<std::endl;
        output_file.flush();
    }
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamCsv::reset_index(void)
{
    m_file_index = 1;
}

//###########################################################################
}