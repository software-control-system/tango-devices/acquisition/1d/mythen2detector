/*************************************************************************/
/*! 
 *  \file   CollectTask.hpp
 *  \brief  class which calls Mythen commands (templates part)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_COLLECT_TASK_HPP_
#define MYTHEN_COLLECT_TASK_HPP_

// TANGO
#include <tango.h>

/*************************************************************************/

namespace Mythen2Detector_ns
{

/************************************************************************
 * \brief show the information of several types of data
 * \param[in] in_data          data to show
 * \param[in] in_label         defines the data 
 * \param[in] in_optionalScale defines an optional scale for the data
*************************************************************************/
template<typename T>
void CollectTask::show_information(const T           & in_data         ,
                                   const std::string & in_label        ,
                                   const std::string & in_optionalScale) const
{
    std::stringstream strStream;

    strStream << "[" << in_label << " : " << in_data;

    if(in_optionalScale.size() > 0)
    {
        strStream << " " << in_optionalScale;
    }

    strStream << "]" << std::endl;

    MYTHEN_TASK_DEBUG_STREAM << strStream.str();
}

/************************************************************************
 * \brief show the information stored in a vector
 * \param[in] in_data          data list
 * \param[in] in_label         defines the data 
 * \param[in] in_optionalScale defines an optional scale for the data
*************************************************************************/
template<typename T>
void CollectTask::show_vector_information(const std::vector<T> & in_data         ,
                                          const std::string    & in_label        ,
                                          const std::string    & in_optionalScale) const
{
    std::stringstream strStream;

    strStream << "[" << in_label << " : ";
    
    for(std::size_t Index = 0 ; Index < in_data.size() ; Index++)
    {
        if(Index > 0)
        {
            strStream << ", ";
        }

        strStream << "(" << Index << ") " << in_data[Index];
    }

    if(in_optionalScale.size() > 0)
    {
        strStream << " " << in_optionalScale;
    }

    strStream << "]" << std::endl;

    MYTHEN_TASK_DEBUG_STREAM << strStream.str();
}

/************************************************************************
 * \brief show the information stored in a vector and manage the max values 
 * \param[in] in_data          data list
 * \param[in] in_maxdata       max data
 * \param[in] in_label         defines the data 
 * \param[in] in_optionalScale defines an optional scale for the data
*************************************************************************/
template<typename T>
void CollectTask::show_vector_proportion_information(const std::vector<T> & in_data         ,
                                                     const std::vector<T> & in_maxdata      ,
                                                     const std::string    & in_label        ,
                                                     const std::string    & in_optionalScale) const
{
    std::stringstream strStream;

    strStream << "[" << in_label << " : ";
    
    for(std::size_t Index = 0 ; Index < in_data.size() ; Index++)
    {
        if(Index > 0)
        {
            strStream << ", ";
        }

        strStream << "(" << Index << ") " << in_data[Index] << "/" << in_maxdata[Index];
    }

    if(in_optionalScale.size() > 0)
    {
        strStream << " " << in_optionalScale;
    }

    strStream << "]" << std::endl;

    MYTHEN_TASK_DEBUG_STREAM << strStream.str();
}

/*******************************************************************
 * \brief get a vector of data for a number of elements
 * \param[in]  in_cmdId         command to be send to the socket server
 * \param[in]  in_callerName  Class and method name of the caller 
 *                            (for exception management)
 * \return a vector of data 
 *******************************************************************/
template< typename T >
std::vector<T> CollectTask::get_data_vector(Com::ServerCmd      in_cmdId         , 
                                            const int32_t       in_elementsNumber,
                                            const std::string & in_callerName    ) const
{
    std::vector<T> dataVector;

    dataVector.clear();

    if(in_elementsNumber > 0)
    {
        int32_t error ;
        bool    result;
        T    *  temp  = new T[in_elementsNumber];

        result = m_Com->get_command(in_cmdId, temp, in_elementsNumber, &error);

        if(result)
        {
            dataVector = std::vector<T>(temp, temp + in_elementsNumber);
        }

        delete [] temp;

        // exception management
        if(!result)
        {
            std::string msg_error = get_error_string(error);

            throw_tango_exception("TANGO_DEVICE_ERROR" ,
                                  msg_error.c_str()    ,
                                  in_callerName.c_str());
        }
    }

    return dataVector;
}

/*******************************************************************
 * \brief get the tango logger - to avoid the removing of const 
 *******************************************************************/
//! \brief Gets the associated logger.
inline log4tango::Logger * CollectTask::const_get_logger() const
{
    return ((Com *)this)->get_logger();
}

} // namespace Mythen2Detector_ns

#endif /* MYTHEN_COLLECT_TASK_HPP_ */

//###########################################################################
