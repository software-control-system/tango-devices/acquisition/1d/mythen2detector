/*************************************************************************/
/*! 
 *  \file   Flatfield.h
 *  \brief  class used to manage the flatfield files
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_FLATFIELD_H_
#define MYTHEN_FLATFIELD_H_

//TANGO
#include <tango.h>

namespace Mythen2Detector_ns
{
/*************************************************************************/
/// we can not include controller header because Flatfield and Controller 
/// are interdependant.
/// Controller uses methods of Flatfield to create and save flatfield
/// files.
/// Flatfield uses methods of Controller to get module informations.
class Controller;

/*************************************************************************/
static const std::string FLATFIELD_FILE_EXTENSION = ".ftf"; 
static const std::string FLATFIELD_DELIMITER      = "_"   ; /// file name is build with <name>+<delimiter>+<module serial number>+<extension>

/*************************************************************************/
class Flatfield : public Tango::LogAdapter
{
    public:
        /// constructor
        Flatfield(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~Flatfield();

        /*******************************************************************
         * \brief Load the flatfields of all modules
         * \param[in]  in_name          flatfield name
         * \return     container filled with the flatfield
         *******************************************************************/
        std::vector<int32_t> load(const std::string & in_name);

        /*******************************************************************
         * \brief Save a module flatfield into a file
         * \param[in] in_name          flatfield name
         * \param[in] in_module_index  module index [0...active modules max[
         * \param[in] in_flatfield     flatfield data
         *******************************************************************/
        void save(const std::string          & in_name        ,
                  const int32_t              & in_module_index,
                  const std::vector<int32_t> & in_flatfield   );

        /*******************************************************************
         * \brief List the flatfield files in the directory
         * \return container filled with the flatfield files names
         *******************************************************************/
        std::vector<std::string> get_list(void);

    private :
        /*******************************************************************
         * \brief Load the module flatfield of a file
         * \param[in]  in_name          flatfield name
         * \param[in]  in_module_index  module index [0...active modules max[
         * \param[out] out_flatfield    container filled with the flatfield
         *******************************************************************/
        void load(const std::string    & in_name        ,
                  const int32_t        & in_module_index,
                  std::vector<int32_t> & out_flatfield  );

        /*******************************************************************
         * \brief Build a flatfield file name
         * \param[in] in_name          flatfield name (alias)
         * \param[in] in_module_index  module index [0...active modules max[
         *******************************************************************/
        std::string build_file_name(const std::string & in_name        ,
                                    const int32_t     & in_module_index);

        /*******************************************************************
         * \brief Get the flatfield files path
         * \return flatfield files path (with '/' at end)
         *******************************************************************/
        std::string get_path(void);

        /*******************************************************************
         * \brief Build a complete flatfield file path
         * \param[in] in_name          flatfield name (alias)
         * \param[in] in_module_index  module index [0...active modules max[
         *******************************************************************/
        std::string build_complete_path(const std::string & in_name        ,
                                        const int32_t     & in_module_index);

        /*******************************************************************
         * \brief Check if a flatfield file exists
         * \param[in]  in_name          flatfield name
         * \param[in]  in_module_index  module index [0...active modules max[
         * \return     true is the flatfield exists, false either
         *******************************************************************/
        bool exists(const std::string & in_name        ,
                    const int32_t     & in_module_index);

    private :
        /// Owner Device server object
        Tango::DeviceImpl * m_device    ;
        Controller        * m_controller; /// Controller object used for calling methods to abort the acquisition
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_CSV_H_

//###########################################################################
