/*************************************************************************/
/*! 
 *  \file   StreamLog.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_STREAM_LOG_H_
#define MYTHEN_STREAM_LOG_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Stream.h"

namespace Mythen2Detector_ns
{
/*************************************************************************/
class StreamLog : public Stream
{
    public:
        /// constructor
        StreamLog(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~StreamLog();

        /// get the stream type
        virtual enum Stream::Types get_type();

        /// Update the stream with new acquisition data
        virtual void update(const AcquisitionData & in_acquisition_data);

        /// Reset the file index
        virtual void reset_index(void);

    private :
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_LOG_H_

//###########################################################################
