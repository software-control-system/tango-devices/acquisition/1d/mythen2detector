/*************************************************************************/
/*! 
 *  \file   StreamLog.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamLog.h"

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
StreamLog::StreamLog(Tango::DeviceImpl* dev, Controller * in_controller) : Stream(dev, in_controller)
{
    DEBUG_STREAM << "StreamLog::StreamLog() - [BEGIN]" << std::endl;

    DEBUG_STREAM << "StreamLog::StreamLog() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamLog::~StreamLog() 
{
}

/*******************************************************************
 * \brief Get the stream type
 * \return the stream type
 *******************************************************************/
enum Stream::Types StreamLog::get_type(void) 
{
    return LOG_STREAM;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_acquisition_data acquisition data
 *******************************************************************/
void StreamLog::update(const AcquisitionData & in_acquisition_data)
{
    // we can now treat the acquisition data objects which were in the list in one time
    const std::vector< std::vector<int32_t> > frames = in_acquisition_data.get_frames();

    const size_t   nb_Frames_read    = frames.size();
    const uint32_t first_frame_index = in_acquisition_data.get_first_frame_index();

    INFO_STREAM << "StreamLog::update - "
                << "frames indexes [" << first_frame_index << "->" << (first_frame_index + nb_Frames_read -1) << "]) " 
                << "number (" << nb_Frames_read << ") size (" << frames[0].size() << ")" << std::endl;
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamLog::reset_index(void)
{
}

//###########################################################################
}