/*************************************************************************/
/*! 
 *  \file   ComNet.cpp
 *  \brief  Socket communication class
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "ComNet.h"

#include <netinet/tcp.h>
#include <netdb.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

namespace Mythen2Detector_ns
{

//#define MYTHEN_COM_NET_ACTIVATE_TRAME_TRACE

/*******************************************************************
 * \brief constructor
 *******************************************************************/
ComNet::ComNet(Tango::DeviceImpl * dev) : Com(dev)
{
    COM_INFO_STREAM << "ComNet::ComNet" << endl;

	// Ignore the sigpipe we get we try to send quit to
	// dead server in disconnect, just use error codes
	struct sigaction pipe_act;

	sigemptyset(&pipe_act.sa_mask);

    pipe_act.sa_flags   = 0      ;
	pipe_act.sa_handler = SIG_IGN;

    sigaction(SIGPIPE, &pipe_act, 0);

	m_sock = -1;
    memset(&m_server_name, 0, sizeof(struct sockaddr_in));
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
ComNet::~ComNet() 
{
    COM_INFO_STREAM << "ComNet::~ComNet - [BEGIN]" << endl;
    disconnect();
    COM_INFO_STREAM << "ComNet::~ComNet - [END]" << endl;
}

/*******************************************************************
 * \brief Connection to Mythen tcpip server
 *******************************************************************/
void ComNet::connect(const std::string & in_hostname, int in_port) 
{
    COM_INFO_STREAM << "ComNet::connect -> " << in_hostname << ":" << in_port << endl;

    struct hostent  * hostPtr ;
	struct protoent * protocol;

    if (m_connected) 
    {
        std::ostringstream MsgErr;

        MsgErr << "Already connected" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::connect()");
	}

    // creating the socket
    m_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (m_sock < 0)
    {
        std::ostringstream MsgErr;

        MsgErr << "Can't create socket" << endl;
        throw_tango_exception("NETWORK_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::connect()");
    }

    // geting host address
    hostPtr = gethostbyname(in_hostname.c_str());
    
    if (hostPtr == NULL)
    {
		endhostent();
		close(m_sock);

        std::ostringstream MsgErr;

        MsgErr << "Can't get gethostbyname : " << in_hostname << endl;
        throw_tango_exception("NETWORK_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::connect()");
    }

    // connecting
    m_server_name.sin_family = AF_INET;
    m_server_name.sin_port   = htons(in_port);
    memcpy(&m_server_name.sin_addr.s_addr, hostPtr->h_addr, hostPtr->h_length);
	endhostent();

    // Because there is also a connect method in the class, we need to specify that
    // the connect call is on the global namespace (::) to avoid a compile error.
    if(::connect(m_sock, (struct sockaddr*) &m_server_name, sizeof(struct sockaddr_in)) != 0)
    {
		close(m_sock);

        std::ostringstream MsgErr;

        MsgErr << "Connection to server refused. Is the server running?" << endl;
        throw_tango_exception("NETWORK_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::connect()");
    }

    // setting socket options
    protocol = getprotobyname("tcp");

    if (protocol == 0) 
    {
        std::ostringstream MsgErr;

        MsgErr << "Can't get protocol TCP" << endl;
        throw_tango_exception("NETWORK_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::connect()");
    } 
    else 
    {
	    int opt = 1;
	
        if (setsockopt(m_sock, protocol->p_proto, TCP_NODELAY, (char *) &opt, 4) < 0) 
        {
            std::ostringstream MsgErr;

            MsgErr << "Can't set socket options" << endl;
            throw_tango_exception("NETWORK_ERROR",
                                  MsgErr.str().c_str(),
                                  "ComNet::connect()");
	    }
    }

    endprotoent();

    m_connected = true;
}

/*******************************************************************
 * \brief Disconnection from Mythen tcpip server
 *******************************************************************/
void ComNet::disconnect(void) 
{
	if (m_connected) 
    {
        COM_INFO_STREAM << "ComNet::disconnect" << endl;

        shutdown(m_sock, 2);
		close   (m_sock   );

		m_connected = false;
	}
}

/************************************************************************
 * \brief Send a command to the DCS and receive its response
 * @param in_cmd_id       command id
 * @param in_send_buffer  optional binary buffer which will be concatened 
 * @param in_send_lenght  optional binary buffer lenght in bytes
 * @param out_recv_buffer optional response buffer
 * @param in_recv_lenght  expected size of the response
 * @param out_error       error value
 * @param in_args         optional arguments
 ************************************************************************/
bool ComNet::command(Com::ServerCmd   in_cmd_id      , 
                     const uint8_t  * in_send_buffer ,
                     int              in_send_lenght ,
                     uint8_t        * out_recv_buffer,
                     int              in_recv_lenght ,
                     int32_t        * out_error      ,
                     va_list          in_args        )
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_network_lock);
    bool result;

    // retrieve the command description for logging
    COM_DEBUG_STREAM << "ComNet::sendCmd(" << in_cmd_id << ") : " << Com::get_command_description(in_cmd_id) << endl;

	if (!m_connected) 
    {
        std::ostringstream MsgErr;

        MsgErr << "Not connected!" << endl;
        throw_tango_exception("LOGIC_ERROR",
                              MsgErr.str().c_str(),
                              "ComNet::sendCmd()");
	}

    // retrieve the command string linked to the command id
    std::string command           = Com::get_command_string              (in_cmd_id);
    bool        returnTypeIsFloat = Com::is_float_return_type_for_command(in_cmd_id);

    std::string formatted_string  = string_format_arg(command, in_args);

    std::vector<uint8_t> net_buffer;
    net_buffer.resize(formatted_string.size() + in_send_lenght, 0);

    // copy the string into the buffer
    memcpy(net_buffer.data(), formatted_string.data(), formatted_string.size());

    // copy the optional binary buffer
    if(in_send_buffer != NULL)
    {
        memcpy(net_buffer.data() + formatted_string.size(), in_send_buffer, in_send_lenght);
    }

    // send the command
    result = net_send(formatted_string, net_buffer, out_error);

    // receive the result
    if (result)
    {
        result = net_receive(out_recv_buffer, in_recv_lenght, out_error, returnTypeIsFloat);
    }

    if(!result)
    {
        COM_DEBUG_STREAM << "ComNet::command(): Error: ["   << *out_error << "] " << get_error_string(*out_error) 
                         << " (" << formatted_string << ")" << endl;
    }

    return (result);
}

/*******************************************************************
 * \brief Send a command to the DCS
 *******************************************************************/
// to avoid a warning
#ifdef MYTHEN_COM_NET_ACTIVATE_TRAME_TRACE
bool ComNet::net_send(const std::string          & in_Command   ,
                      const std::vector<uint8_t> & in_net_buffer,
                      int32_t                    * out_error    )
#else
bool ComNet::net_send(const std::string          & /*in_Command*/,
                      const std::vector<uint8_t> & in_net_buffer,
                      int32_t                    * out_error    )
#endif
{
    int n;

    // no error by default
    *out_error = NO_ERROR;

    n = send(m_sock, in_net_buffer.data(), in_net_buffer.size(), 0);

#ifdef MYTHEN_COM_NET_ACTIVATE_TRAME_TRACE
    COM_DEBUG_STREAM << "ComNet::send(): " << n << " bytes sent: " << in_Command.c_str() << endl;
#endif

    if(n < 0)
    {
        COM_ERROR_STREAM << "ComNet::send(): write to socket error" << endl;
        *out_error = NETWORK_ERROR;
    }

    return (n >= 0);
}


/*******************************************************************
 * \brief Receive the response from the DCS and save it in the buffer
 *******************************************************************/
bool ComNet::net_receive(uint8_t * out_recv_buffer, int in_recv_lenght, int32_t * out_error, bool in_isFloatType)
{
    int     responseLength = 0;
    int     n              ;
    int     readMax        ;
    uint8_t TempBuffer[4]  ; // use in the case of a NULL out_recv_buffer (just an error code is expected to be received)

    // in the case of a set command, the expected reception data will only be an error code, which can be a float or an int
    if(out_recv_buffer == NULL)
    {
        out_recv_buffer = reinterpret_cast<uint8_t*>(TempBuffer);
        in_recv_lenght  = sizeof(TempBuffer);
    }

    // no error by default
    *out_error = NO_ERROR;

    // read, until the expected response length could be read (the response might be fragmented into several packets)
    for(;;) 
    {
        readMax = in_recv_lenght - responseLength; // remaining size to read

        n = recv(m_sock, out_recv_buffer + responseLength, readMax, 0);

        if (n <= 0)
        {
            COM_ERROR_STREAM << "ComNet::receive(): Could not receive response" << endl;
            *out_error = NETWORK_ERROR;
            return false;
        }

        responseLength += n;
        
        //Server returned error code ?
        if (responseLength == 4  || responseLength == in_recv_lenght)
        {
            break;
        }
    }

    if (responseLength == 4)
    {
        int32_t Error;

        if(!in_isFloatType)
        {
            Error = *(reinterpret_cast<int32_t *>(out_recv_buffer));
        }
        else
        {
            Error = static_cast<int32_t>(*(reinterpret_cast<float *>(out_recv_buffer)));
        }

        if(Error < 0)
        {
            *out_error = Error;
            return false;
        }
    }

#ifdef MYTHEN_COM_NET_ACTIVATE_TRAME_TRACE
    COM_DEBUG_STREAM << "ComNet::receive(): " << responseLength << " bytes received" << endl;
#endif

    return true;
}

//###########################################################################
}