/*************************************************************************/
/*! 
 *  \file   StreamCsv.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_STREAM_CSV_H_
#define MYTHEN_STREAM_CSV_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Stream.h"

namespace Mythen2Detector_ns
{
/*************************************************************************/
class StreamCsv : public Stream
{
    public:
        /// constructor
        StreamCsv(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~StreamCsv();

        /// get the stream type
        virtual enum Stream::Types get_type();

        /// Update the stream with new acquisition data
        virtual void update(const AcquisitionData & in_acquisition_data);

        /// Reset the file index
        virtual void reset_index(void);

    private :
        int m_file_index; // index used to build csv files names
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_STREAM_CSV_H_

//###########################################################################
