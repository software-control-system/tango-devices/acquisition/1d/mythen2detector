/*----- PROTECTED REGION ID(Mythen2DetectorStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        Mythen2DetectorStateMachine.cpp
//
// description : State machine file for the Mythen2Detector class
//
// project :     Mythen2 dectector TANGO device.
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <Mythen2Detector.h>

/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::Mythen2DetectorStateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  INIT     |  
//  STANDBY  |  
//  FAULT    |  
//  RUNNING  |  


namespace Mythen2Detector_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbModules_allowed()
 *	Description : Execution allowed for nbModules attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbModules_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbModulesStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbModulesStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbChannels_allowed()
 *	Description : Execution allowed for nbChannels attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbChannels_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbChannelsStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbChannelsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbBadChannels_allowed()
 *	Description : Execution allowed for nbBadChannels attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbBadChannels_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbBadChannelsStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbBadChannelsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbBits_allowed()
 *	Description : Execution allowed for nbBits attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbBits_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbBitsStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbBitsStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbBitsStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbBitsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_triggerMode_allowed()
 *	Description : Execution allowed for triggerMode attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_triggerMode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::triggerModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::triggerModeStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::triggerModeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::triggerModeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_frameRateMax_allowed()
 *	Description : Execution allowed for frameRateMax attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_frameRateMax_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::frameRateMaxStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::frameRateMaxStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbFrames_allowed()
 *	Description : Execution allowed for nbFrames attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbFrames_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbFramesStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbFramesStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbFramesStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbFramesStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_currentFrame_allowed()
 *	Description : Execution allowed for currentFrame attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_currentFrame_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::currentFrameStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::currentFrameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_readoutTime_allowed()
 *	Description : Execution allowed for readoutTime attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_readoutTime_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::readoutTimeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::readoutTimeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_exposureTime_allowed()
 *	Description : Execution allowed for exposureTime attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_exposureTime_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::exposureTimeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::exposureTimeStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::exposureTimeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::exposureTimeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_delayAfterFrame_allowed()
 *	Description : Execution allowed for delayAfterFrame attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_delayAfterFrame_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::delayAfterFrameStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::delayAfterFrameStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::delayAfterFrameStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::delayAfterFrameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_delayBeforeFrame_allowed()
 *	Description : Execution allowed for delayBeforeFrame attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_delayBeforeFrame_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::delayBeforeFrameStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::delayBeforeFrameStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::delayBeforeFrameStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::delayBeforeFrameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_nbGatesByFrame_allowed()
 *	Description : Execution allowed for nbGatesByFrame attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_nbGatesByFrame_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbGatesByFrameStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbGatesByFrameStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::nbGatesByFrameStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::nbGatesByFrameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_badChannelInterpolation_allowed()
 *	Description : Execution allowed for badChannelInterpolation attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_badChannelInterpolation_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::badChannelInterpolationStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::badChannelInterpolationStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::badChannelInterpolationStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::badChannelInterpolationStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_flatFieldCorrection_allowed()
 *	Description : Execution allowed for flatFieldCorrection attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_flatFieldCorrection_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatFieldCorrectionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatFieldCorrectionStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatFieldCorrectionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatFieldCorrectionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_temperature_allowed()
 *	Description : Execution allowed for temperature attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_temperature_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::temperatureStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::temperatureStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_streamType_allowed()
 *	Description : Execution allowed for streamType attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_streamType_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTypeStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTypeStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTypeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTypeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_streamTargetPath_allowed()
 *	Description : Execution allowed for streamTargetPath attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_streamTargetPath_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTargetPathStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTargetPathStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTargetPathStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTargetPathStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_streamTargetFile_allowed()
 *	Description : Execution allowed for streamTargetFile attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_streamTargetFile_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTargetFileStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTargetFileStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamTargetFileStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamTargetFileStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_streamNbAcqPerFile_allowed()
 *	Description : Execution allowed for streamNbAcqPerFile attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_streamNbAcqPerFile_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamNbAcqPerFileStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamNbAcqPerFileStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::streamNbAcqPerFileStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::streamNbAcqPerFileStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_rateCorrection_allowed()
 *	Description : Execution allowed for rateCorrection attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_rateCorrection_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::rateCorrectionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::rateCorrectionStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::rateCorrectionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::rateCorrectionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_flatfieldPath_allowed()
 *	Description : Execution allowed for flatfieldPath attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_flatfieldPath_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::RUNNING)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatfieldPathStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatfieldPathStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatfieldPathStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatfieldPathStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_flatfieldName_allowed()
 *	Description : Execution allowed for flatfieldName attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_flatfieldName_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatfieldNameStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatfieldNameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_fileGeneration_allowed()
 *	Description : Execution allowed for fileGeneration attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_fileGeneration_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for fileGeneration attribute in read access.
	/*----- PROTECTED REGION ID(Mythen2Detector::fileGenerationStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::fileGenerationStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_estimatedFrameRate_allowed()
 *	Description : Execution allowed for estimatedFrameRate attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_estimatedFrameRate_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for estimatedFrameRate attribute in read access.
	/*----- PROTECTED REGION ID(Mythen2Detector::estimatedFrameRateStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::estimatedFrameRateStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_frame_allowed()
 *	Description : Execution allowed for frame attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_frame_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::frameStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::frameStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_flatfield_allowed()
 *	Description : Execution allowed for flatfield attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_flatfield_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Mythen2Detector::flatfieldStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::flatfieldStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_Snap_allowed()
 *	Description : Execution allowed for Snap attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_Snap_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::SnapStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::SnapStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_Stop_allowed()
 *	Description : Execution allowed for Stop attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_Stop_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::StopStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::StopStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_Start_allowed()
 *	Description : Execution allowed for Start attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_Start_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::StartStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::StartStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_GetDataStreams_allowed()
 *	Description : Execution allowed for GetDataStreams attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_GetDataStreams_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::GetDataStreamsStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::GetDataStreamsStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_StreamResetIndex_allowed()
 *	Description : Execution allowed for StreamResetIndex attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_StreamResetIndex_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::StreamResetIndexStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::StreamResetIndexStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_ResetDCS4_allowed()
 *	Description : Execution allowed for ResetDCS4 attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_ResetDCS4_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::ResetDCS4StateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::ResetDCS4StateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_CreateFlatfield_allowed()
 *	Description : Execution allowed for CreateFlatfield attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_CreateFlatfield_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::CreateFlatfieldStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::CreateFlatfieldStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_GetFlatfieldList_allowed()
 *	Description : Execution allowed for GetFlatfieldList attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_GetFlatfieldList_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::GetFlatfieldListStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::GetFlatfieldListStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_ChooseFlatfield_allowed()
 *	Description : Execution allowed for ChooseFlatfield attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_ChooseFlatfield_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::ChooseFlatfieldStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::ChooseFlatfieldStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_RefreshFlatfield_allowed()
 *	Description : Execution allowed for RefreshFlatfield attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_RefreshFlatfield_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::RefreshFlatfieldStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::RefreshFlatfieldStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_GetTemperatures_allowed()
 *	Description : Execution allowed for GetTemperatures attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_GetTemperatures_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::GetTemperaturesStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::GetTemperaturesStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_GetHumidities_allowed()
 *	Description : Execution allowed for GetHumidities attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_GetHumidities_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::GetHumiditiesStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::GetHumiditiesStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Mythen2Detector::is_GetHighVoltages_allowed()
 *	Description : Execution allowed for GetHighVoltages attribute
 */
//--------------------------------------------------------
bool Mythen2Detector::is_GetHighVoltages_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(Mythen2Detector::GetHighVoltagesStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Mythen2Detector::GetHighVoltagesStateAllowed
		return false;
	}
	return true;
}

}	//	End of namespace
