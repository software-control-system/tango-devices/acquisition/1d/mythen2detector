/*************************************************************************/
/*! 
 *  \file   Com.h
 *  \brief  mythen communication abstract class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_COM_H_
#define MYTHEN_COM_H_

//TANGO
#include <tango.h>

// GLOBAL
#include "Config.h"

// LOCAL
#include "Cmd.h"

/*************************************************************************/
/* Macros used to log on cout or a tango logger.                         */ 
/* MYTHEN_TANGO_INDEPENDANT needs to be defined in the pom.xml for an    */
/* integration in a tango independant software.                          */
/*************************************************************************/
#ifdef MYTHEN_TANGO_INDEPENDANT
    #define COM_INFO_STREAM  std::cout
    #define COM_DEBUG_STREAM std::cout
    #define COM_ERROR_STREAM std::cout
#else
    #define COM_INFO_STREAM  INFO_STREAM
    #define COM_DEBUG_STREAM DEBUG_STREAM
    #define COM_ERROR_STREAM ERROR_STREAM
#endif
/*************************************************************************/

namespace Mythen2Detector_ns
{
    static const int32_t NO_ERROR                             = 0    ; // value for no error
    static const int32_t NETWORK_ERROR                        = -255 ; // communication error - must not be returned by Mythen device
    static const int32_t SELECT_ALL_MODULES                   = 65535; // for selection command like -module which takes a module index
    static const int     ASSEMBLY_DATE_BYTE_SIZE              = 50   ; // system assembly date size (end null character?)
    static const int     FIRMWARE_VERSION_BYTE_SIZE           = 9    ; // firmware version size
    static const int     SOFTWARE_VERSION_BYTE_SIZE           = 7    ; // software version size
    static const int     MODULES_FIRMWARE_VERSION_BYTE_SIZE   = 8    ; // firmware version size of modules
    static const int     BITS_MODES_NUMBER                    = 4    ; // number of bits modes

    static const int32_t STATUS_ACQUISITION_RUNNING           = 0x00000001; // status Acquisition running
    static const int32_t STATUS_ACQUISITION_EXPOSURE_INACTIVE = 0x00000008; // status Acquisition running & exposure inactive
    static const int32_t STATUS_NO_DATA_FOR_READOUT           = 0x00010000; // status no data for readout (empty buffer)
    static const int32_t STATUS_MASK                          = 0x00010009; // status mask for all usefull bits
    static const int32_t STATUS_ACQUISITION_MASK              = 0x00000009; // status mask for all acquisition bits

    static const std::vector<int> BITS_MODES        { 4,  8, 16, 24 }; // bits modes list
    static const std::vector<int> READOUT_BITS_MODES{24, 16,  8,  4 }; // order of bits mode for the readout times command

    static const std::vector<string> TRIGGER_MODES_LABELS{"Internal", "External Single", "External Multiple", "External Gated" }; // labels of trigger modes

    static const int32_t          BITS_MODE_CURRENT          = -1  ; // used by the check_frame_rate management
    static const int64_t          EXPOSURE_TIME_CURRENT      = -1LL; // used by the check_frame_rate management
    static const int64_t          DELAY_AFTER_FRAME_CURRENT  = -1LL; // used by the check_frame_rate management
    static const int64_t          DELAY_BEFORE_FRAME_CURRENT = -1LL; // used by the check_frame_rate management
    
    // labels used to show informations
    static const std::string INFO_LABEL_DCS4_SERIAL_NUMBER          = "DCS4 serial number"; 
    static const std::string INFO_LABEL_DCS4_FPGA_FIRMWARE          = "DCS4 FPGA firmware"; 
    static const std::string INFO_LABEL_DCS4_ASSEMBLY_DATE          = "DCS4 assembly date";
    static const std::string INFO_LABEL_DCS4_SOFTWARE_VERSION       = "DCS4 socket server software version";
    static const std::string INFO_LABEL_DCS4_MAX_NUMBER_MODULES     = "DCS4 maximum number of modules";
    static const std::string INFO_LABEL_DCS4_NUMBER_OF_MODULES      = "DCS4 number of active modules";
    static const std::string INFO_LABEL_DCS4_TOTAL_CHANNELS         = "DCS4 total number of channels of active modules";
    static const std::string INFO_LABEL_DCS4_TOTAL_BAD_CHANNELS     = "DCS4 total number of bad channels of active modules";

    static const std::string INFO_LABEL_MODULES_FIRMWARES           = "Active modules FPGA firmwares";
    static const std::string INFO_LABEL_MODULES_SERIAL_NUMBERS      = "Active modules serial numbers";	
    static const std::string INFO_LABEL_MODULES_NUMBER_CHANNELS     = "Active modules number of channels";
    static const std::string INFO_LABEL_MODULES_NUMBER_BAD_CHANNELS = "Active modules number of bad channels";

/*************************************************************************/
// abstract class used to send commands to Mythen server
class Com : public Tango::LogAdapter
{
    friend class CollectTask;

    public:
        // SOCKET COMMANDS ID
        enum ServerCmd 
        { 
            // Table 1 - General commands
            GET_ASSEMBLY_DATE = 0, GET_BAD_CHANNELS      , GET_COMMAND_ID          , GET_COMMAND_SET_ID  , 
            GET_DCS_TEMPERATURE  , GET_FRAMERATE_MAX     , GET_FW_VERSION          , GET_HUMIDITY        , 
            GET_HV               , GET_MOD_CHANNELS      , GET_MOD_FW_VERSION      , GET_MOD_NUM         , 

            GET_MODULE           , GET_N_MAX_MODULES     , GET_N_MODULES           , GET_SENSOR_MATERIAL , 
            GET_SENSOR_THICKNESS , GET_SENSOR_WIDTH      , GET_SYSTEM_NUM          , GET_TEMPERATURE     ,
            GET_TEST_VERSION     , GET_VERSION           , SEL_MODULE              , SET_N_MODULES       ,
            CMD_RESET            ,

            // Table 2 - Acquisition control
            SET_DEL_AFTER        , SET_FRAMES            , GET_DEL_AFTER           , GET_FRAMES          ,
            GET_N_BITS           , GET_READ_OUT_TIMES    , GET_STATUS              , GET_TIME            ,
            SET_N_BITS           , CMD_READ_OUT_ONE_FRAME, CMD_READ_OUT_NB_FRAME   , CMD_START           , 
            CMD_STOP             , SET_TIME              ,

            // Table 3 - Detector settings
            SET_ENERGY           , GET_ENERGY            , GET_ENERGY_MAX          , GET_ENERGY_MIN      ,
            GET_K_THRESH         , GET_K_THRESH_MAX      , GET_K_THRESH_MIN        , SET_K_THRESH        , 
            SET_K_THRESH_ENERGY  , CMD_LOAD_SETTINGS_CU  , CMD_LOAD_SETTINGS_MO    , CMD_LOAD_SETTINGS_CR,
            CMD_LOAD_SETTINGS_AG ,

            // Table 4 - Data correction commands
            SET_BAD_CHAN_INTERP  , SET_FLATFIELD         , SET_FLATFIELD_CORRECTION, GET_BAD_CHAN_INTERP ,
            GET_CUT_OFF          , GET_FLATFIELD         , GET_FLATFIELD_CORRECTION, GET_RATE_CORRECTION ,
            GET_TAU              , CMD_LOAD_FLATFIELD    , SET_RATE_CORRECTION     , SET_TAU             ,

            // Table 5 - Trigger and Gating control commands
            SET_CONT_TRIGGER_MODE, SET_DEL_BEFORE        , SET_GATED_MEASURE_MODE  , SET_GATES_NB        ,
            GET_CONT_TRIGGER_MODE, GET_DEL_BEFORE        , GET_GATED_MEASURE_MODE  , GET_GATES_NB        ,
            GET_TRIGGER_MODE     , SET_TRIGGER_MODE      ,

            // Table 6 - Debugging commands
            CMD_START_LOG        , GET_LOG_STATUS        , CMD_STOP_LOG            , CMD_GET_LOG         ,
            CMD_TEST_DATA_SET    ,
        };

        // TRIGGER MODES
        enum TriggerMode 
        { 
            TRIGGER_INTERNAL           = 0,
            TRIGGER_EXTERNAL_SINGLE    ,
            TRIGGER_EXTERNAL_MULTIPLE  ,
            TRIGGER_EXTERNAL_GATE      ,
            TRIGGER_CURRENT            ,  // used by the check_frame_rate management
        };
        
    protected:
        typedef std::map <int32_t, std::string> ErrorCodeMap ;
        typedef std::pair<int32_t, std::string> ErrorCodePair;

        typedef std::map <ServerCmd, Cmd> ServerCmdMap ;
        typedef std::pair<ServerCmd, Cmd> ServerCmdPair;

    public:
        explicit Com(Tango::DeviceImpl * dev);
        virtual ~Com();

        virtual void connect(const std::string & in_hostname, int in_port) = 0;
        virtual void disconnect(void) = 0;

        // get commands
        template<typename T>
        bool get_command(Com::ServerCmd in_CmdId, T & out_Buffer, int32_t * out_Error);

        template<typename T>
        bool get_command(Com::ServerCmd in_CmdId, T * out_Buffer, int in_Length, int32_t * out_Error);

        // set commands
        bool set_command(Com::ServerCmd in_CmdId, int32_t * out_Error, ...);

        // action commands
        bool action_command(Com::ServerCmd in_CmdId, int32_t * out_Error, ...);

        template<typename T>
        bool action_command(Com::ServerCmd in_CmdId, T * out_Buffer, int in_Length, int32_t * out_Error, ...);

        // binary commands
        bool binary_command(Com::ServerCmd in_CmdId, const uint8_t * in_Buffer, int in_Length, int32_t * out_Error, ...);

        // command and error management
        std::string get_error_string(int32_t   in_ErrorId);
        std::string get_command_string(ServerCmd in_CmdId  );
		std::string get_command_description(ServerCmd in_CmdId  );
        bool        is_float_return_type_for_command(ServerCmd in_CmdId  );

    protected:
        virtual bool command(Com::ServerCmd   in_cmd_id      , 
                             const uint8_t  * in_send_buffer ,
                             int              in_send_lenght ,
                             uint8_t        * out_recv_buffer,
                             int              in_recv_lenght ,
                             int32_t        * out_error      ,
                             va_list          in_args        ) = 0;

        std::string string_format_arg(const std::string & in_format, va_list in_args);

        void throw_tango_exception(const char * reason,
                                   const char * desc  ,
                                   const char * origin);

    private:
        void insert_new_command(bool in_FloatReturnType, ServerCmd in_CmdId, std::string in_cmd, std::string in_description);
        void Insert_new_errors (int32_t in_ErrorId, std::string in_description);

    protected:
        bool         m_connected ; // true if connected
        ErrorCodeMap m_error_codes; // to retrieve the error code description with the error code
        ServerCmdMap m_server_cmds; // to retrieve the command string to send to the device server

        /// Owner Device server object
        Tango::DeviceImpl * m_device;

    protected:
};

} // namespace Mythen2Detector_ns

#include "Com.hpp"

#endif /* MYTHEN_COM_H_ */

//###########################################################################
