/*************************************************************************/
/*! 
 *  \file   StreamNo.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamNo.h"

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
StreamNo::StreamNo(Tango::DeviceImpl* dev, Controller * in_controller) : Stream(dev, in_controller)
{
    DEBUG_STREAM << "StreamNo::StreamNo() - [BEGIN]" << std::endl;

    DEBUG_STREAM << "StreamNo::StreamNo() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamNo::~StreamNo() 
{
}

/*******************************************************************
 * \brief Get the stream type
 * \return the stream type
 *******************************************************************/
enum Stream::Types StreamNo::get_type(void) 
{
    return NO_STREAM;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_acquisition_data acquisition data
 *******************************************************************/
void StreamNo::update(const AcquisitionData & /*in_acquisition_data*/)
{
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamNo::reset_index(void)
{
}

//###########################################################################
}