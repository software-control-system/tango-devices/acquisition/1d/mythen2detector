/*************************************************************************/
/*! 
 *  \file   Mythen2Detector.hpp
 *  \brief  class Mythen2Detector
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
#ifndef MYTHEN_MYTHEN2DETECTOR_HPP_
#define MYTHEN_MYTHEN2DETECTOR_HPP_

//- TANGO
#include <tango.h>

//============================================================================================================
// TEMPLATE IMPLEMENTATION
//============================================================================================================
template <class F1, class F2>
void Mythen2Detector::create_attribute(const std::string &   name                ,
                                       int                   data_type           ,
                                       Tango::AttrDataFormat data_format         ,
                                       Tango::AttrWriteType  access_type         ,
                                       Tango::DispLevel      disp_level          ,
                                       size_t                polling_period_in_ms,
                                       const std::string &   unit                ,
                                       const std::string &   format              ,
                                       const std::string &   desc                ,
                                       F1                    read_callback       ,
                                       F2                    write_callback      ,
                                       yat::Any              user_data           )
{
	DEBUG_STREAM << "Mythen2Detector::create_attribute() - [BEGIN]" << endl;
	INFO_STREAM << "\t- Create dynamic attribute [" << name << "]" << endl;

    ////////////////////////////////////////////////////////////////////////////////////////    
	yat4tango::DynamicAttributeInfo dai;
	dai.dev = m_device;
	//- specify the dyn. attr.  name
	dai.tai.name = name;
	//- associate the dyn. attr. with its data
	dai.set_user_data(user_data);
	//- describe the dynamic attr we want...
	dai.tai.data_type        = data_type;
	dai.tai.data_format      = data_format;
	dai.tai.writable         = access_type;
	dai.tai.disp_level       = disp_level;
	dai.tai.unit             = unit;
	dai.tai.format           = format;
	dai.tai.description      = desc;
    dai.polling_period_in_ms = polling_period_in_ms;

	//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	//	dai.cdb = true;
	
	//- instanciate the read callback (called when the dyn. attr. is read)    
	if(access_type == Tango::READ ||access_type == Tango::READ_WRITE)
	{
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, read_callback);
	}

	//- instanciate the write callback (called when the dyn. attr. is read)    
	if(access_type == Tango::WRITE ||access_type == Tango::READ_WRITE)
	{
		dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, write_callback);
	}
	
	//- add the dyn. attr. to the device
	DEBUG_STREAM << "\t- add the dyn. attr. to the device [" << m_device << "]" << endl;
	m_dim.dynamic_attributes_manager().add_attribute(dai);
	DEBUG_STREAM << "Mythen2Detector::create_attribute() - [END]" << endl;
}

/*****************************************************************************************
 * \brief Fill the read attribute with the device informations
 * \param[out] out_attr       Tango attribute to update with the Tango attribute member
 * \param[out] out_attr_read  Tango attribute member to filled with the data value
 * \param[in]  in_method      Pointer on a controller method to call to retrieve the data
 * \param[in]  in_callerName  Class and method name of the caller (for exception management)
******************************************************************************************/
template< typename T1, typename T2>
void Mythen2Detector::read_static_attribute(Tango::Attribute & out_attr,
                                            T1 * out_attr_read,
                                            T2 (Mythen2Detector_ns::Controller::*in_method)(),
                                            const std::string & in_callerName)
{
    try
    {
        *out_attr_read = static_cast<T1>((m_controller.get()->*in_method)());
        out_attr.set_value(out_attr_read);
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;

        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          in_callerName.c_str());
    }
}

/********************************************************************************************************
 * \brief Use the write attribut to set informations in the device
 * \param[in]  in_attr_write                Tango value set by the user
 * \param[in]  in_method                    Pointer on a controller method to call to set the data
 * \param[in]  in_optionalMemorizedProperty Optional memorized property name (NULL if not used)
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void Mythen2Detector::write_static_attribute(T1 & in_attr_write,
                                             void (Mythen2Detector_ns::Controller::*in_method)(const T2 &),
                                             const char * in_optionalMemorizedProperty,
                                             const std::string & in_callerName)
{
    try
    {
        T2 data = static_cast<T2>(in_attr_write);
        (m_controller.get()->*in_method)(data);

        if(in_optionalMemorizedProperty != NULL)
        {
            yat4tango::PropertyHelper::set_property(this, in_optionalMemorizedProperty, in_attr_write);
        }
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;

        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          in_callerName.c_str());
    }
}

/********************************************************************************************
 * \brief Fill a dynamic attribute with a information of a device module
 * \param[in]  T1              TANGO data type (Tango::DevFloat for example)
 * \param[in]  T2              data type of the device
 * \param[out] out_cbd         Tango attribute to update with the new value
 * \param[in]  in_method       Pointer on a controller method to call to retrieve the data
 * \param[in]  in_callerName   Class and method name of the caller (for exception management)
 * \param[in]  in_is_enabled_during_running Can we read the attribut during running state ?
*********************************************************************************************/
template< typename T1, typename T2>
void Mythen2Detector::read_dynamic_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                             T2 (Mythen2Detector_ns::Controller::*in_method)(const int32_t &),
                                             const std::string & in_callerName,
                                             const bool in_is_enabled_during_running)
{
    DEBUG_STREAM << in_callerName << " : " << out_cbd.dya->get_name() << endl;

    try
    {
        bool            device_was_initialized = is_device_initialized();
        Tango::DevState state                  = m_controller->get_state();

        if ((state == Tango::FAULT && device_was_initialized) || state == Tango::INIT)
        {
            std::string reason = "It's currently not allowed to read attribute ";
            reason += out_cbd.dya->get_name();

            Tango::Except::throw_exception("TANGO_DEVICE_ERROR" ,
                                           reason.c_str()       ,
                                           in_callerName.c_str());
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!out_cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            in_callerName.c_str());
        }

        std::string attribut_name  = out_cbd.dya->get_name();
        std::string attribut_index = attribut_name.substr (attribut_name.size()-2,2);

        // conversion of the string to an integer - indexes start at 1 in the interface.
        int module_index = strtol (attribut_index.c_str(),NULL,10) - 1; 

        // set the attribute value
        T2 * temp = out_cbd.dya->get_user_data<T2>();

        // if in_is_enabled_during_running is false,
        // during an acquisition, we do not allow the access to the modules to avoid problems.
        // We use the last known value
        bool attribut_is_in_alarm = true;
        
        if((in_is_enabled_during_running) || (state != Tango::RUNNING))
        {       
            *temp = (m_controller.get()->*in_method)(module_index);
            attribut_is_in_alarm = false;
        }

        out_cbd.tga->set_value((T1*)temp);
        out_cbd.tga->set_quality((attribut_is_in_alarm) ? Tango::ATTR_ALARM : Tango::ATTR_VALID);
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          in_callerName.c_str());
    }
}

/********************************************************************************************************
 * \brief Use the write dynamic attribut to set informations in the device
 * \param[in]  T1                           TANGO data type (Tango::DevFloat for example)
 * \param[in]  T2                           data type of the device
 * \param[in]  in_cbd                       To get the new Tango value set by the user
 * \param[in]  in_method                    Pointer on a controller method to call to set the data
 * \param[in]  in_optionalMemorizedProperty Optional memorized property name (NULL if not used)
 *                                          An index will be concatenate. 
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void Mythen2Detector::write_dynamic_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                              void (Mythen2Detector_ns::Controller::*in_method)(const int32_t &, const T2 &),
                                              const char * in_optionalMemorizedProperty,
                                              const std::string & in_callerName)
{
    DEBUG_STREAM << in_callerName << " : " << in_cbd.dya->get_name() << endl;

    try
    {
        bool            device_was_initialized = is_device_initialized();
        Tango::DevState state                  = m_controller->get_state();

        if ((state == Tango::FAULT && device_was_initialized) || 
            (state == Tango::INIT) ||
            (state == Tango::RUNNING))
        {
            std::string reason = "It's currently not allowed to write attribute ";
            reason += in_cbd.dya->get_name();

            Tango::Except::throw_exception("TANGO_DEVICE_ERROR" ,
                                           reason.c_str()       ,
                                           in_callerName.c_str());
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!in_cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            in_callerName.c_str());
        }

        std::string attribut_name  = in_cbd.dya->get_name();
        std::string attribut_index = attribut_name.substr (attribut_name.size()-2,2);
        
        // conversion of the string to an integer - indexes start at 1 in the interface.
        int module_index = strtol (attribut_index.c_str(),NULL,10) - 1; 

        T1 val;
        in_cbd.tga->get_write_value( val);

        // get the attribute value
        T2 * temp = in_cbd.dya->get_user_data<T1>();

        (m_controller.get()->*in_method)(module_index, static_cast<T2>(val));
        *temp = static_cast<T2>(val);

        if(in_optionalMemorizedProperty != NULL)
        {
            std::stringstream tempValue ; // value to store
            std::stringstream tempStream; // built property name (name + index)

            tempValue  << *temp;
            tempStream << in_optionalMemorizedProperty << std::setfill('0') << std::setw(2) << (module_index + 1);
    
            yat4tango::PropertyHelper::set_property(this, tempStream.str(), tempValue.str());
        }
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          in_callerName.c_str());
    }
}

/*************************************************************************************************************
 * \brief Use to update a static attribute and the hardware with a property value
 * \param[in]  T1                    TANGO data type for read attribute variable (Tango::DevFloat for example)
 * \param[in]  T2                    Data type for memorized property (string for example)
 * \param[in]  in_attribute_name     Name of the attribute linked to the property value
 * \param[out] out_attr_read         Tango attribute member to filled with the data value
 * \param[in]  in_memorized_property Property variable
 * \param[in]  in_write_method       Pointer on a controller method to call to set the data
*************************************************************************************************************/
template< typename T1, typename T2>
void Mythen2Detector::write_property_in_static_attribute(const std::string & in_attribute_name,
                                                         T1 * out_attr_read,
                                                         const T2 & in_memorized_property,
                                                         void (Mythen2Detector_ns::Mythen2Detector::*in_write_method)(Tango::WAttribute &))
{
	INFO_STREAM << "Write tango hardware at Init - " << in_attribute_name << "." << endl;

    *out_attr_read = in_memorized_property;
	Tango::WAttribute & attribute = dev_attr->get_w_attr_by_name(in_attribute_name.c_str());
	attribute.set_write_value(*out_attr_read);
    (this->*in_write_method)(attribute);
}

/**********************************************************************************************************************
 * \brief Use to update a dynamic attribute and the hardware with a property value
 * \param[in]  T1                    TANGO data type for attribute and property variables (Tango::DevFloat for example)
 * \param[in]  in_attribute_name     Name of the attribute linked to the property
 * \param[in]  in_module_index       module index value to build the correct attribute & property names
 * \param[in]  in_property_name      Name of the property linked to the attribute
 * \param[in]  in_write_method       Pointer on a controller method to call to set the data
**********************************************************************************************************************/
template< typename T1>
void Mythen2Detector::write_property_in_dynamic_attribute(const std::string & in_attribute_name,
                                                          const std::string & in_property_name ,
                                                          const int32_t     & in_module_index  ,
                                                          void (Mythen2Detector_ns::Mythen2Detector::*in_write_method)(yat4tango::DynamicAttributeWriteCallbackData &))
{
    // build the correct attribute and property names
    std::string       attribute_name;
    std::string       property_name ;
    std::stringstream tempStream    ; // built property or attribute name (name + index)

    tempStream.str("");
    tempStream << in_attribute_name << std::setfill('0') << std::setw(2) << (in_module_index + 1);
    attribute_name = tempStream.str();

    tempStream.str("");
    tempStream << in_property_name << std::setfill('0') << std::setw(2) << (in_module_index + 1);
    property_name = tempStream.str();

    INFO_STREAM << "Write tango hardware at Init - " << attribute_name << "." << endl;

    // retrieve the property value using its name
    T1 memorizedValue = yat4tango::PropertyHelper::get_property<T1 >(this, property_name.c_str());

    // retrieve the attribute using its name
	Tango::WAttribute           & attribute     = dev_attr->get_w_attr_by_name(attribute_name.c_str());
    yat4tango::DynamicAttribute & dyn_attribute = m_dim.dynamic_attributes_manager().get_attribute(attribute_name);

    // get the user data
    T1 * user_data = dyn_attribute.get_user_data<T1>();

    // update the attribute user data with the property value
    *user_data = memorizedValue;

    // update the attribute
    attribute.set_write_value(user_data);

    // call the write method of the dynamic attribute
    yat4tango::DynamicAttributeWriteCallbackData cbd;
    cbd.tga = &attribute    ;
    cbd.dya = &dyn_attribute;

    (this->*in_write_method)(cbd);
}

#endif // MYTHEN_MYTHEN2DETECTOR_HPP_