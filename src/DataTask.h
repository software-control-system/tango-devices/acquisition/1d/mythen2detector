/*************************************************************************/
/*! 
 *  \file   DataTask.h
 *  \brief  Class which treats acquisition data.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_DATA_TASK_H_
#define MYTHEN_DATA_TASK_H_

// TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat4tango/DeviceTask.h>

// PROJECT
#include "AcquisitionData.h"

/*************************************************************************/
static const size_t DATA_TASK_PERIODIC_MS = 500;

namespace Mythen2Detector_ns
{
/*************************************************************************/
/// we can not include controller header because DataTask and Controller 
/// are interdependant.
/// Controller uses methods of DataTask to add new data
/// DataTask uses methods of Controller to give data to the stream manager.
class Controller;

/*************************************************************************/
class DataTask : public yat4tango::DeviceTask
{
    public:

    public:
        /// constructor 
        DataTask(Tango::DeviceImpl * dev, Controller * in_controller);

        /// destructor
        virtual ~DataTask();

        /// Setter of the state
        void set_state(Tango::DevState state);

        /// Getter of the state
        Tango::DevState get_state(void);

        /// Setter of the status
        void set_status(const std::string& status);

        /// Getter of the status
        std::string get_status(void);

        /// add the acquisition data
        void add(const AcquisitionData & in_new_data);

    protected:
        /// [yat4tango::DeviceTask implementation]
        virtual void process_message(yat::Message& msg) throw(Tango::DevFailed);

    private:
        /// Manage the FAULT status with an exception
        void on_fault(Tango::DevFailed df);

        /// Manage the FAULT status with an error message
        void on_fault(const std::string& status_message);

        /// Compute the state and status 
        void compute_state_status();

        /// treat the data from the acquisition
        void treat(void);

        /// check if the acquisition data list is empty
        bool is_empty(void);

    private:
        Tango::DeviceImpl   * m_device    ; /// Owner Device server object
        Controller          * m_controller; /// Controller object used for calling methods to store the data   

        Tango::DevState   m_state       ; /// state
        std::stringstream m_status      ; /// status
        yat::Mutex        m_state_lock  ; /// mutex used to protect state access
        yat::Mutex        m_data_lock   ; /// mutex used to protect data access (FIFO)

        std::list<AcquisitionData> m_data_list; /// list of data from the acquisition to treat
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_DATA_TASK_H_