/*************************************************************************/
/*! 
 *  \file   AcquisitionData.h
 *  \brief  class used for access to acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef MYTHEN_ACQUISITION_DATA_H_
#define MYTHEN_ACQUISITION_DATA_H_

// SYSTEM
#include <cstddef>
#include <stdint.h>
#include <list>
#include <vector>

namespace Mythen2Detector_ns
{

/*\brief Class used for access to acquisition data - used by the Controller, CollectTask, DataTask, Stream classes*/

class AcquisitionData
{
    public:
        /// constructor - no frame
        AcquisitionData();

        /// constructor - several frames
        AcquisitionData(const std::vector< std::vector<int32_t> > & in_readout_data, 
                        const uint32_t in_first_frame_index);

        /// constructor - only one frame
        AcquisitionData(const std::vector<int32_t> & in_readout_data, 
                        const uint32_t in_first_frame_index);

        /// get several frames of read channels from the hardware
        const std::vector< std::vector<int32_t> > & get_frames(void) const;

        /// get index of the first frame which was collected and store in the m_readout_data
        uint32_t get_first_frame_index(void) const;

        /// get the number of frames
        std::size_t get_frames_number(void) const;

        /// get the readout data of a specific frame
        const std::vector<int32_t> & get_frame(uint32_t in_frame_index = 0) const;

        /// merge two Acquisition data objects
        bool merge(const AcquisitionData & in_object_to_merge);

        /// check if the acquisition data is empty
        bool is_empty() const;

        /// create an acquisition data object with the last frame data
        AcquisitionData get_last_frame(void) const;

    private :
        std::vector< std::vector<int32_t> > m_readout_data     ; /// several frames of read channels from the hardware
        uint32_t                            m_first_frame_index; /// index of the first frame in the m_readout_data
};

} // namespace Mythen2Detector_ns

#endif // MYTHEN_ACQUISITION_DATA_H_

//###########################################################################
