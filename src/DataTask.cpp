/*************************************************************************/
/*! 
 *  \file   DataTask.cpp
 *  \brief  Class which treats acquisition data.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// PROJECT
#include "DataTask.h"
#include "Controller.h"

// SYSTEM
#include <vector>
#include <string>

namespace Mythen2Detector_ns
{
//============================================================================================================
// DataTask class
//============================================================================================================
/*******************************************************************
 * \brief constructor
 * \param[in] dev acces to tango device for log management
 *******************************************************************/
DataTask::DataTask(Tango::DeviceImpl* dev, Controller * in_controller) : 
                    yat4tango::DeviceTask(dev), m_device(dev), m_controller(in_controller)
{
    DEBUG_STREAM << "DataTask::DataTask() - [BEGIN]" << std::endl;

    // default status management
    set_status("Starting...");
    set_state (Tango::INIT  );

    set_periodic_msg_period(DATA_TASK_PERIODIC_MS);
    enable_timeout_msg (false);
    enable_periodic_msg(true );

    go(); // post the INIT msg         

    DEBUG_STREAM << "DataTask::DataTask() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
DataTask::~DataTask()
{
    DEBUG_STREAM << "DataTask::~DataTask()" << std::endl;
}

//============================================================================================================
/*******************************************************************
 * \brief Setter of the state
*******************************************************************/
void DataTask::set_state(Tango::DevState state)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState DataTask::get_state(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    compute_state_status();
    return m_state;
}

/*******************************************************************
 * \brief Setter of the status
*******************************************************************/
void DataTask::set_status(const std::string& status)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string DataTask::get_status(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an exception
*******************************************************************/
void DataTask::on_fault(Tango::DevFailed df)
{
    DEBUG_STREAM << "DataTask::on_fault()" << std::endl;

    std::stringstream status;
    status.str("");
    status << "Origin\t: " << df.errors[0].origin << std::endl;
    status << "Desc\t: "   << df.errors[0].desc   << std::endl;
    status << "Reason\t: " << df.errors[0].reason << std::endl;

    on_fault(status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an error message
*******************************************************************/
void DataTask::on_fault(const std::string& status_message)
{
    set_state (Tango::FAULT  );
    set_status(status_message);
}

/*******************************************************************
 * \brief Compute the state and status 
*******************************************************************/
void DataTask::compute_state_status()
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    
    if(m_state != Tango::FAULT)
    {
        if(is_empty())
        {
            set_state (Tango::STANDBY);
            set_status("DataTask is in standby...");
        }
        else
        {
            set_state (Tango::RUNNING);
            set_status("DataTask is treating an acquisition...");
        }
    }
}

//============================================================================================================
/*******************************************************************
 * \brief add the acquisition data
 *******************************************************************/
void DataTask::add(const AcquisitionData & in_new_data)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_data_lock);
    m_data_list.push_back(in_new_data);
}

/*******************************************************************
 * \brief treat the data from the acquisition
 *******************************************************************/
void DataTask::treat(void)
{
    AcquisitionData data;

    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_data_lock);

        // exceptions are managed by the process_message method.
        if(m_data_list.empty())
            return;

        // we try to merge all the data from the list into an unique object
        // we take the first element of the list
        data = m_data_list.front();
        m_data_list.pop_front();

        while(!m_data_list.empty())
        {
            if(data.merge(m_data_list.front()))
            {
                // the first element of the list was merged into so we can remove it from the list
                m_data_list.pop_front();
            }
            else
            {
                // the two elements of the list can not be merged (data are not contiguous)
                INFO_STREAM << "DataTask::treat: the two elements of the list can not be merged (data are not contiguous)." << std::endl;
                break;
            }
        }

        // we re-insert the element at the first place of the list
        // The list must not be empty till we have completely treated the data
        m_data_list.push_front(data); 
    }

    {
        // we can now treat the acquisition data objects which were in the list in one time
        const std::vector< std::vector<int32_t> > frames = data.get_frames();

        const size_t   nb_Frames_read    = frames.size();
        const uint32_t first_frame_index = data.get_first_frame_index();

        INFO_STREAM << "DataTask::treat - "
                    << "frames indexes [" << first_frame_index << "->" << (first_frame_index + nb_Frames_read -1) << "]) " 
                    << "number (" << nb_Frames_read << ") size (" << frames[0].size() << ")" << std::endl;

        // call to the Controller to update its stream
        m_controller->update_stream(data);
    }

    // we can now remove the first element of the list which was completly treated by the DataTask
    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_data_lock);
        m_data_list.pop_front();
    }
}

/*******************************************************************
 * \brief check if the acquisition data list is empty
 *******************************************************************/
bool DataTask::is_empty(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_data_lock);
    return m_data_list.empty();
}

//============================================================================
//- DataTask::process_message
//============================================================================
void DataTask::process_message(yat::Message& msg) throw (Tango::DevFailed)
{
    try
    {
        switch (msg.type())
        {
            //-----------------------------------------------------
            case yat::TASK_INIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " DataTask::TASK_INIT" << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                set_state(Tango::STANDBY);
            }
            break;

            //-----------------------------------------------------
            case yat::TASK_EXIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " DataTask::TASK_EXIT" << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
            break;

            //-----------------------------------------------------
            case yat::TASK_TIMEOUT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " DataTask::TASK_TIMEOUT" << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
            break;

            //-----------------------------------------------------------------------------------------------
            case yat::TASK_PERIODIC:
            {
                treat();
            }
            break;
        }
    }
    catch (yat::Exception& ex)
    {
        ex.dump();
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << ex.errors[0].origin << std::endl;
        error_msg << "Desc\t: "   << ex.errors[0].desc   << std::endl;
        error_msg << "Reason\t: " << ex.errors[0].reason << std::endl;
        ERROR_STREAM << "Exception from - process_message() : " << error_msg.str() << std::endl;
        on_fault(error_msg.str());
        throw;
    }
}

//###########################################################################
}