/*************************************************************************/
/*! 
 *  \file   Cmd.cpp
 *  \brief  mythen command information class 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "Cmd.h"

namespace Mythen2Detector_ns
{

/*******************************************************************
 * \brief constructor
 *******************************************************************/
Cmd::Cmd(const std::string & in_cmd            , 
         const std::string & in_description    ,
         bool                in_FloatReturnType)
{
    m_cmd               = in_cmd            ;
    m_description       = in_description    ;
    m_float_return_type = in_FloatReturnType;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Cmd::~Cmd() 
{
}

/*******************************************************************
 * \brief Accessor to cmd string
 *******************************************************************/
std::string Cmd::get_command_string() 
{
	return m_cmd;
}

/*******************************************************************
 * \brief Accessor to cmd description
 *******************************************************************/
std::string Cmd::get_command_description() 
{
	return m_description;
}

/*******************************************************************
 * \brief Accessor to cmd return type
 *******************************************************************/
bool Cmd::is_float_return_type() 
{
	return m_float_return_type;
}

//###########################################################################
}