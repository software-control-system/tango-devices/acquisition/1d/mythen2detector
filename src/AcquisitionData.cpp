/*************************************************************************/
/*! 
 *  \file   AcquisitionData.cpp
 *  \brief  class used for access to acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "AcquisitionData.h"

namespace Mythen2Detector_ns
{
/*******************************************************************
 * \brief constructor - no frame
 *******************************************************************/
AcquisitionData::AcquisitionData()
{
    m_readout_data.clear();
    m_first_frame_index = 0;
}

/*******************************************************************
 * \brief constructor - several frames
 *******************************************************************/
AcquisitionData::AcquisitionData(const std::vector< std::vector<int32_t> > & in_readout_data, 
                                 const uint32_t in_first_frame_index)
{
    m_readout_data      = in_readout_data     ;
    m_first_frame_index = in_first_frame_index;
}

/*******************************************************************
 * \brief constructor - only one frame
 *******************************************************************/
AcquisitionData::AcquisitionData(const std::vector<int32_t> & in_readout_data, 
                                 const uint32_t in_first_frame_index)
{
    std::vector< std::vector<int32_t> > readout_data_frames;
    readout_data_frames.push_back(in_readout_data);

    m_readout_data      = readout_data_frames ;
    m_first_frame_index = in_first_frame_index;
}

/*******************************************************************
 * \brief get several frames of read channels from the hardware
 * \return several frames of read channels from the hardware
 *******************************************************************/
const std::vector< std::vector<int32_t> > & AcquisitionData::get_frames() const
{
	return m_readout_data;
}

/*******************************************************************
 * \brief get index of the first frame which was collected and store
 *        in the m_readout_data
 * \return index of the first frame of m_readout_data
 *******************************************************************/
uint32_t AcquisitionData::get_first_frame_index() const 
{
	return m_first_frame_index;
}

/*******************************************************************
 * \brief merge two Acquisition data objects
 * \param[in] in_object_to_merge object to merge in this object 
 * \return true if successfull
 *******************************************************************/
bool AcquisitionData::merge(const AcquisitionData & in_object_to_merge)
{
    bool result = false;

    // check if the frames indexes are coherents
    if((m_first_frame_index + m_readout_data.size()) == in_object_to_merge.m_first_frame_index)
    {
        // check the channels number
        if(m_readout_data[0].size() == in_object_to_merge.m_readout_data[0].size())
        {
            m_readout_data.insert( m_readout_data.end(),
                                   in_object_to_merge.m_readout_data.begin(),
                                   in_object_to_merge.m_readout_data.end  ());

            result = true;
        }
    }

    return result;
}

/*******************************************************************
 * \brief create an acquisition data object with the last frame data
 * \return the last frame data
 *******************************************************************/
AcquisitionData AcquisitionData::get_last_frame(void) const 
{
    if(m_readout_data.size() > 0)
    {
        std::vector< std::vector<int32_t> > last_frame_readout_data     ;
        uint32_t                            last_frame_first_frame_index;

        last_frame_readout_data.push_back(m_readout_data[m_readout_data.size() - 1]); // last frame
        
        last_frame_first_frame_index = m_first_frame_index + m_readout_data.size() - 1;

        return AcquisitionData(last_frame_readout_data, last_frame_first_frame_index);
    }
    else
    {
        return AcquisitionData(); // empty object
    }
}

/*******************************************************************
 * \brief get the number of frames
 * \return the number of frames
 *******************************************************************/
std::size_t AcquisitionData::get_frames_number(void) const
{
    return m_readout_data.size();
}

/*******************************************************************
 * \brief get the readout data of a specific frame
 * \param[in] in_frame_index Index of the frame [0...frames number[
 * \return the readout data
 *******************************************************************/
const std::vector<int32_t> & AcquisitionData::get_frame(uint32_t in_frame_index) const
{
    return m_readout_data[in_frame_index];
}

/*******************************************************************
 * \brief check if the acquisition data is empty
 * \return true if there is no acquisition data
 *******************************************************************/
bool AcquisitionData::is_empty() const 
{
	return (m_readout_data.size() == 0);
}

//###########################################################################
}