/*************************************************************************/
/*! 
 *  \file   Controller.cpp
 *  \brief  Interface class which calls Task methods.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// PROJECT
#include "Controller.h"

#include "ComSim.h"
#include "ComNet.h"
#include "Com.h"
#include "StreamNo.h"
#include "StreamCsv.h"
#include "StreamLog.h"
#include "StreamNexus.h"
#include "Flatfield.h"

// SYSTEM
#include <vector>
#include <string>

namespace Mythen2Detector_ns
{
//============================================================================================================
//#define CONTROLLER_LOG_ACQUISITION_TIME
    
//============================================================================================================
// Controller::InitParameters class
//============================================================================================================
/**************************************************************************
* \brief constructor
* \param[in] in_socket_server_host_address ip address of the socket server
* \param[in] in_socket_server_port         ip port    of the socket server 
* \param[in] in_stream_type                type of stream
* \param[in] in_stream_target_path         path for the stream files
* \param[in] in_stream_target_file         name for the stream file 
* \param[in] in_stream_nb_acq_per_file     number of files per acquisition
* \param[in] in_stream_write_mode          stream write mode
* \param[in] in_stream_items               defines the content to stream
**************************************************************************/
Controller::InitParameters::InitParameters(const std::string & in_socket_server_host_address,
                                           const int           in_socket_server_port        ,
                                           const std::string & in_stream_type               ,
                                           const std::string & in_stream_target_path        ,
                                           const std::string & in_stream_target_file        ,
                                           const uint32_t      in_stream_nb_acq_per_file    ,
                                           const std::string & in_stream_write_mode         ,
                                           const std::vector<std::string> & in_stream_items )
{
    m_socket_server_host_address = in_socket_server_host_address;
    m_socket_server_port         = in_socket_server_port        ;
    m_stream_type                = in_stream_type               ;
    m_stream_target_path         = in_stream_target_path        ;
    m_stream_target_file         = in_stream_target_file        ;
    m_stream_nb_acq_per_file     = in_stream_nb_acq_per_file    ;
    m_stream_write_mode          = in_stream_write_mode         ;
    m_stream_items               = in_stream_items              ;
}

//============================================================================================================
// Controller class
//============================================================================================================
/*******************************************************************
 * \brief constructor
 * \param[in] dev access to tango device for log management
 *******************************************************************/
Controller::Controller(Tango::DeviceImpl* dev, const std::string& detector_type) : yat4tango::DeviceTask(dev), m_device(dev)
{
    DEBUG_STREAM << "Controller::Controller() - [BEGIN]" << std::endl;

    // create the collect task
    m_collect_task.reset(new CollectTask(m_device, detector_type));

    // create the data task
    m_data_task.reset(new DataTask(m_device, this), yat4tango::DeviceTaskExiter());

    // default status management
    set_status("Starting...");
    set_state (Tango::INIT  );

    enable_timeout_msg (false);
    enable_periodic_msg(false);
    set_periodic_msg_period(CONTROLLER_TASK_PERIODIC_MS);

    DEBUG_STREAM << "Controller::Controller() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Controller::~Controller()
{
    DEBUG_STREAM << "Controller::~Controller() - [BEGIN]" << std::endl;
 
    // release the collect task
    m_collect_task.reset();

    // release the data task
    m_data_task.reset();

    DEBUG_STREAM << "Controller::~Controller() - [END]" << std::endl;
}

/*******************************************************************
 * \brief initializer - connection to the device
 * \param[in] in_init_parameters Allow to pass the needed data for
 *                               the controller initialization.
 *******************************************************************/
void Controller::init(const Controller::InitParameters & in_init_parameters)
{
    DEBUG_STREAM << "Controller::init() - [BEGIN]" << std::endl;

    ///----------------------------------------------------------------------------------
    /// Connection to server
    m_collect_task->connect(in_init_parameters.m_socket_server_host_address,
                            in_init_parameters.m_socket_server_port        );

    // create the stream manager
    build_stream(in_init_parameters);

    go(); // post the INIT msg         

    DEBUG_STREAM << "Controller::init() - [END]" << std::endl;
}

/*******************************************************************
 * \brief releaser - disconnection from the device
 *******************************************************************/
void Controller::release(void)
{
    DEBUG_STREAM << "Controller::release() - [BEGIN]" << std::endl;

    // need to stop a current acquisition ?
    try { m_collect_task->disconnect(); } catch(...) {};
    
    // release the stream
    m_stream.reset();

    DEBUG_STREAM << "Controller::release() - [END]" << std::endl;
}

//============================================================================================================
/*******************************************************************
 * \brief Setter of the state
*******************************************************************/
void Controller::set_state(Tango::DevState state)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState Controller::get_state(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    compute_state_status();
    return m_state;
}

/*******************************************************************
 * \brief Setter of the status
*******************************************************************/
void Controller::set_status(const std::string& status)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string Controller::get_status(void)
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an exception
*******************************************************************/
void Controller::on_fault(Tango::DevFailed df)
{
    DEBUG_STREAM << "Controller::on_fault()" << std::endl;

    std::stringstream status;
    status.str("");
    status << "Origin\t: " << df.errors[0].origin << std::endl;
    status << "Desc\t: "   << df.errors[0].desc   << std::endl;
    status << "Reason\t: " << df.errors[0].reason << std::endl;

    on_fault(status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with an error message
*******************************************************************/
void Controller::on_fault(const std::string& status_message)
{
    set_state (Tango::FAULT  );
    set_status(status_message);
}

/*******************************************************************
 * \brief Manage the abort command
*******************************************************************/
void Controller::abort(std::string status)
{
    DEBUG_STREAM << "Controller::abort() - [BEGIN]" << std::endl;    

    yat::Message* msg = yat::Message::allocate(CONTROLLER_ABORT_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(status);
    post(msg);

    DEBUG_STREAM << "Controller::abort() - [END]" << std::endl;
}

/*******************************************************************
 * \brief Compute the state and status 
*******************************************************************/
void Controller::compute_state_status()
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_lock);

    Tango::DevState collect_task_state = m_collect_task->get_state();
    Tango::DevState data_task_state    = m_data_task->get_state   ();
    Tango::DevState stream_state       = m_stream->get_state      ();

    if(collect_task_state == Tango::FAULT)
    {
        on_fault(m_collect_task->get_status());
    }
    else
    if(data_task_state == Tango::FAULT)
    {
        on_fault(m_data_task->get_status());
    }
    else
    if(stream_state == Tango::FAULT)
    {
        on_fault(m_stream->get_status());
    }
    else
    if((collect_task_state == Tango::RUNNING) ||
       (data_task_state    == Tango::RUNNING))
    {
        set_state (Tango::RUNNING);
        set_status("Acquisition is running...");
    }
    else
    if((collect_task_state == Tango::STANDBY) &&
       (data_task_state    == Tango::STANDBY))
    {
        set_state (Tango::STANDBY);
        set_status("Waiting for request...");
    }
    else
    {
        set_state (Tango::UNKNOWN);
        set_status("Unkown status...");
    }
}

//============================================================================================================
/*******************************************************************
 * \brief get the last frame received from the hardware
 *        the acquisition.
 * \return the last frame
 *******************************************************************/
std::vector<int32_t> Controller::get_last_frame(void) const
{
    AcquisitionData last_frame = m_collect_task->get_last_frame();
    return last_frame.get_frame();
}
        
//============================================================================================================
/*******************************************************************
 * \brief Reset the detector and all active modules
 *******************************************************************/
void Controller::reset(void) const
{
    return m_collect_task->reset();
}

/*******************************************************************
 * \brief Get informations about DCS4 and active modules
 * \return vector which contains (label, value)
*******************************************************************/
std::vector<std::pair<std::string, std::string> > Controller::get_versions_informations(void)
{
    return m_collect_task->get_versions_informations();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_max_nb_modules(void)
{
    return m_collect_task->get_max_nb_modules();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_nb_modules(void)
{
    return m_collect_task->get_nb_modules();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_nb_channels(void)
{
    return m_collect_task->get_nb_channels();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_nb_bad_channels(void)
{
    return m_collect_task->get_nb_bad_channels();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_temperature(void)
{
    return m_collect_task->get_dcs_temperature_celcius();
}

/*******************************************************************
 * \brief Get the frame rate limitation value
 * \return the frame rate limitation value
 *******************************************************************/
int32_t Controller::get_frame_rate_limitation(void) const
{
    return m_collect_task->get_frame_rate_limitation();
}

/*******************************************************************
 * \brief Set the frame rate limitation value 
 * \param[in] in_new_value frame rate limitation value 
*******************************************************************/
void Controller::set_frame_rate_limitation(const uint32_t in_new_value)
{
    m_collect_task->set_frame_rate_limitation(in_new_value);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_frame_rate_max(void)
{
    return m_collect_task->get_frame_rate_max();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_computed_frame_rate(void)
{
    return m_collect_task->get_computed_frame_rate();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
uint32_t Controller::get_acquired_frames_number(void)
{
    return m_collect_task->get_acquired_frames_number();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
bool Controller::get_bad_channel_interpolation(void)
{
    return m_collect_task->get_bad_channel_interpolation();
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_bad_channel_interpolation(const bool & in_IsEnabled)
{
    m_collect_task->set_bad_channel_interpolation(in_IsEnabled);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
bool Controller::get_flatfield_correction(void)
{
    return m_collect_task->get_flatfield_correction();
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_flatfield_correction(const bool & in_IsEnabled)
{
    m_collect_task->set_flatfield_correction(in_IsEnabled);
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::store_flatfield(const int32_t              & in_slot_index,
                                 const std::vector<int32_t> & in_flatfield )
{
    m_collect_task->store_flatfield(in_slot_index, in_flatfield);
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_loaded_flatfield(const int32_t & in_slot_index)
{
    m_collect_task->set_loaded_flatfield(in_slot_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::vector<int32_t> Controller::get_loaded_flatfield(void)
{
    return m_collect_task->get_loaded_flatfield();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::vector<int32_t> Controller::get_module_loaded_flatfield(const int32_t & in_module_index)
{
    return m_collect_task->get_module_loaded_flatfield(in_module_index);
}

/*******************************************************************
 * \brief Setter of the flatfield path
 * \param[in] in_flatfield_path flatfield path
*******************************************************************/
void Controller::set_flatfield_path(const std::string & in_flatfield_path)
{
    m_collect_task->set_flatfield_path(in_flatfield_path);
}

/*******************************************************************
 * \brief Getter of the flatfield path
 * \return flatfield path
*******************************************************************/
std::string Controller::get_flatfield_path(void)
{
    return m_collect_task->get_flatfield_path();
}

/*******************************************************************
 * \brief Getter of the flatfield name
 * \return flatfield name
*******************************************************************/
std::string Controller::get_flatfield_name(void)
{
    return m_collect_task->get_flatfield_name();
}

/*******************************************************************
 * \brief Save a module flatfield into a file
 * \param[in] in_name          flatfield name
 * \param[in] in_module_index  module index [0...active modules max[
 *******************************************************************/
void Controller::create_flatfield(const std::string & in_name        ,
                                  const int32_t     & in_module_index)
{
    Flatfield flatfield(m_device, this);

    int32_t nb_modules = get_nb_modules();

    for(int32_t module_index = 0; module_index < nb_modules; module_index++)
    {
        // save all modules flatfields or selected module
        if((in_module_index == -1) || (module_index == in_module_index))
        {
            std::vector<int32_t> spectrum = get_module_frame(module_index);
            flatfield.save(in_name, module_index, spectrum);
        }
    }
}

/*******************************************************************
 * \brief List the flatfield files in the directory
 * \return container filled with the flatfield files names
 *******************************************************************/
std::vector<std::string> Controller::get_flatfield_list(void)
{
    Flatfield flatfield(m_device, this);
    return flatfield.get_list();
}

/*******************************************************************
 * \brief Load and activate a flatfield
 * \param[in] in_name flatfield name
 *******************************************************************/
void Controller::choose_flatfield(const std::string & in_name)
{
    const int32_t slot_index = 0;
    Flatfield flatfield(m_device, this);

    // build the flatfield with the flatfield files
    std::vector<int32_t> spectrum = flatfield.load(in_name);

    // store the flatfield in a slot
    m_collect_task->store_flatfield(slot_index, spectrum);

    // activate the flatfield stored in the slot
    m_collect_task->set_loaded_flatfield(slot_index);

    // change the name of the last loaded flatfield
    m_collect_task->set_flatfield_name(in_name);
}

/*******************************************************************
 * \brief Refresh a flatfield
 *******************************************************************/
void Controller::refresh_flatfield(void)
{
    m_collect_task->refresh_loaded_flatfield();
}

/*******************************************************************
 * \brief Getter of the serial number of a module
*******************************************************************/
std::string Controller::get_module_serial_number(const int32_t & in_module_index)
{
    return m_collect_task->get_module_serial_number(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
bool Controller::get_rate_correction(void)
{
    return m_collect_task->get_rate_correction();
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_rate_correction(const bool & in_IsEnabled)
{
    m_collect_task->set_rate_correction(in_IsEnabled);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_number_of_bits(void)
{
    return m_collect_task->get_number_of_bits();
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_number_of_bits(const int32_t & in_bitsMode)
{
    m_collect_task->set_number_of_bits(in_bitsMode);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
string Controller::get_trigger_mode(void)
{
    Com::TriggerMode hardware_trigger_Mode;
    string           tango_trigger_Mode   ;

    hardware_trigger_Mode = m_collect_task->get_trigger_mode();

    // we need to convert the hardware trigger mode to a tango trigger mode
    // search the hardware trigger mode in a container
    const std::vector<enum Com::TriggerMode>::const_iterator 
    iterator = find(TANGO_TO_HARDWARE_TRIGGER_MODE.begin(), 
                    TANGO_TO_HARDWARE_TRIGGER_MODE.end  (),
                    hardware_trigger_Mode                 );

    // found it
    if (iterator != TANGO_TO_HARDWARE_TRIGGER_MODE.end()) 
    {
        tango_trigger_Mode = TANGO_TRIGGER_MODES_LABELS[iterator - TANGO_TO_HARDWARE_TRIGGER_MODE.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Impossible to found the trigger mode " << hardware_trigger_Mode << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::get_trigger_mode()");
    }

    return tango_trigger_Mode;
}

/*******************************************************************
 * \brief Set informations in the device
*******************************************************************/
void Controller::set_trigger_mode(const string & in_triggerMode)
{
    Com::TriggerMode hardware_trigger_Mode;

    // we need to convert the tango trigger mode to a hardware trigger mode
    // search the tango trigger mode in a container
    const std::vector<string>::const_iterator 
    iterator = find(TANGO_TRIGGER_MODES_LABELS.begin(), 
                    TANGO_TRIGGER_MODES_LABELS.end  (),
                    in_triggerMode                    );

    // found it
    if (iterator != TANGO_TRIGGER_MODES_LABELS.end()) 
    {
        hardware_trigger_Mode = TANGO_TO_HARDWARE_TRIGGER_MODE[iterator - TANGO_TRIGGER_MODES_LABELS.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Incorrect trigger mode : " << in_triggerMode << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::set_trigger_mode()");
    }
    
    m_collect_task->set_trigger_mode(hardware_trigger_Mode);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_temperature(const int32_t & in_module_index)
{
    return m_collect_task->get_module_temperature_celcius(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::string Controller::get_string_modules_temperatures(void)
{
    return m_collect_task->get_string_modules_temperatures_celcius();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_energy(const int32_t & in_module_index)
{
    return m_collect_task->get_module_energy(in_module_index);
}

/*******************************************************************
 * \brief Set informations in the device
 * \param[in]  in_module_index Module index [0...active modules max[
 * \param[in]  in_energy       X-ray energy in keV
 *******************************************************************/
void Controller::set_module_energy(const int32_t & in_module_index, const float & in_energy)
{
    m_collect_task->set_module_energy(in_module_index, in_energy);
	
	// when we change the energy of a module, the default flatfield is used by the DCS.
	// We force an update of the flatfield.
	refresh_flatfield();
	
    // We reset the name of the last loaded flatfield because this is no more a user flatfield.
    m_collect_task->set_flatfield_name(std::string(""));
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_energy_min(const int32_t & in_module_index)
{
    return m_collect_task->get_module_energy_min(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_energy_max(const int32_t & in_module_index)
{
    return m_collect_task->get_module_energy_max(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_threshold(const int32_t & in_module_index)
{
    return m_collect_task->get_module_threshold(in_module_index);
}

/*******************************************************************
 * \brief Set informations in the device
 * \param[in]  in_module_index Module index [0...active modules max[
 * \param[in]  in_energy       X-ray energy in keV
 *******************************************************************/
void Controller::set_module_threshold(const int32_t & in_module_index, const float & in_threshold)
{
    m_collect_task->set_module_threshold(in_module_index, in_threshold);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_threshold_min(const int32_t & in_module_index)
{
    return m_collect_task->get_module_threshold_min(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_threshold_max(const int32_t & in_module_index)
{
    return m_collect_task->get_module_threshold_max(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
float Controller::get_module_humidity(const int32_t & in_module_index)
{
    return m_collect_task->get_module_humidity(in_module_index) * 100.0f;
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_module_high_voltage(const int32_t & in_module_index)
{
    return m_collect_task->get_module_high_voltage(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::string Controller::get_string_modules_high_voltages(void)
{
    return m_collect_task->get_string_modules_high_voltages();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::string Controller::get_string_modules_humidities(void)
{
    return m_collect_task->get_string_modules_humidities();
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
std::vector<int32_t> Controller::get_module_frame(const int32_t & in_module_index)
{
    return m_collect_task->get_module_frame(in_module_index);
}

/*******************************************************************
 * \brief Get informations about the device
*******************************************************************/
int32_t Controller::get_module_channels(const int32_t & in_module_index)
{
    return m_collect_task->get_module_channels(in_module_index);
}

/*******************************************************************
 * \brief Get the readout time of the current bits mode
 * \return readout time in ms
 *******************************************************************/
double Controller::get_readout_time(void)
{
    return convert_delay_100ns_units_to_ms(m_collect_task->get_readout_time());
}

/*******************************************************************
 * \brief Set the exposure time
 * \param[in]  in_exposureTime_100ns exposure time in ms
 *******************************************************************/
void Controller::set_exposure_time(const double & in_exposureTime_ms)
{
    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    m_collect_task->set_exposure_time(convert_delay_ms_to_100_ns_units(in_exposureTime_ms));
}

/*******************************************************************
 * \brief Get the exposure time
 * \return exposure time in ms
 *******************************************************************/
double Controller::get_exposure_time(void)
{
    return convert_delay_100ns_units_to_ms(m_collect_task->get_exposure_time());
}

/*******************************************************************
 * \brief Set the delay after a frame
 * \param[in]  in_delay_ms  delay in ms
 *******************************************************************/
void Controller::set_delay_after_frame(const double & in_delay_ms)
{
    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    m_collect_task->set_delay_after_frame(convert_delay_ms_to_100_ns_units(in_delay_ms));
}

/*******************************************************************
 * \brief Get the delay after a frame
 * \return delay in ms
 *******************************************************************/
double Controller::get_delay_after_frame(void)
{
    return convert_delay_100ns_units_to_ms(m_collect_task->get_delay_after_frame());
}

/**************************************************************************
 * \brief Set the delay before frame (between trigger and start of measure)
 * \param[in]  in_delay_ms  delay in ms
 **************************************************************************/
void Controller::set_delay_before_frame(const double & in_delay_ms)
{
    // ! only relevant for internal and single trigger mode !
    // can not be defined below the readout time of the selected bit mode
    m_collect_task->set_delay_before_frame(convert_delay_ms_to_100_ns_units(in_delay_ms));
}

/**************************************************************************
 * \brief Get the delay before frame (between trigger and start of measure)
 * \return delay in ms
 **************************************************************************/
double Controller::get_delay_before_frame(void)
{
    return convert_delay_100ns_units_to_ms(m_collect_task->get_delay_before_frame());
}

/*******************************************************************
 * \brief Set the frames number within an acquisition
 * \param[in]  in_framesNumber frames number
 *******************************************************************/
void Controller::set_frames_number(const int32_t & in_framesNumber)
{
    m_collect_task->set_frames_number(in_framesNumber);
}

/*******************************************************************
 * \brief Get the frames number within an acquisition
 * \return the frames number within an acquisition
 *******************************************************************/
int32_t Controller::get_frames_number(void)
{
    return m_collect_task->get_frames_number();
}

/**********************************************************************
 * \brief Check if the frames number correspond to a start acquisition
 * \param[in]  in_framesNumber frames number to check
 * \return true if the frame number was changed for a start acquisition
 **********************************************************************/
bool Controller::is_start_acquisition_frames_number(const int32_t & in_framesNumber) const
{
    return m_collect_task->is_start_acquisition_frames_number(in_framesNumber);
}

/*******************************************************************
 * \brief Set the gates number for one frame
 * \param[in]  in_gatesNumber Gates number
 *******************************************************************/
void Controller::set_gates_number_by_frame(const int32_t & in_gatesNumber)
{
    m_collect_task->set_gates_number_by_frame(in_gatesNumber);
}

/*******************************************************************
 * \brief Get the gates number for one frame
 * \return Gates number
 *******************************************************************/
int32_t Controller::get_gates_number_by_frame(void)
{
    return m_collect_task->get_gates_number_by_frame();
}

//============================================================================
/*******************************************************************
 * \brief Convert a delay in units of 100 ns to ms
 * \param[in]  in_delay_100ns delay in units of 100 ns
 * \return delay in ms
 *******************************************************************/
double Controller::convert_delay_100ns_units_to_ms(const int64_t & in_delay_100ns) const
{
    // 1ms = 10000 units of 100ns
    double delay_ms = static_cast<double>(in_delay_100ns) / 10000.0;
    return delay_ms;
}

/*******************************************************************
 * \brief Convert a delay in ms to units of 100 ns
 * \param[in]  in_delay_ms delay in ms
 * \return delay in ms
 *******************************************************************/
int64_t Controller::convert_delay_ms_to_100_ns_units(const double & in_delay_ms) const
{
    // 1ms = 10000 units of 100ns
    int64_t delay_100ns = static_cast<int64_t>(in_delay_ms * 10000LL);
    return delay_100ns;
}

/***************************************************************************
 * \brief Manage the start of acquisition
 * \param[in] in_snap_mode true in snap/stop mode, false for start/stop mode
 ***************************************************************************/
void Controller::start_acquisition(bool in_snap_mode)
{
    INFO_STREAM << "--------------------------------------------" << std::endl;
    INFO_STREAM << "Controller::start_acquisition() - [BEGIN]" << std::endl;

    try
    {
        m_collect_task->stop_acquisition ();
        m_collect_task->start_acquisition(in_snap_mode, CONTROLLER_TASK_PERIODIC_MS);
        start_stream_for_acquisition();
        enable_periodic_msg(true); //only when start is done    
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << std::endl;
        on_fault(df);
    }

    INFO_STREAM << "collecting in progress ..." << std::endl;
    INFO_STREAM << "Controller::start_acquisition() - [END]" << std::endl;
}

/*******************************************************************
 * \brief Manage the acquisition stop
 * \param[in] in_sync true for waiting the stop is done
 *******************************************************************/
void Controller::stop_acquisition(bool in_sync)
{
    INFO_STREAM << "--------------------------------------------" << std::endl;
    INFO_STREAM << "Controller::stop_acquisition() - [BEGIN]" << std::endl;

    try
    {
        // CONTROLLER_TASK_PERIODIC_MS is posted in order to stop the acquisition
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_STOP_MSG, DEFAULT_MSG_PRIORITY, true);

        if(!in_sync)
        {
            post(msg);
        }
        else
        {
            wait_msg_handled(msg, 5000); //to ensure that stop is done
        }
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << std::endl;
        on_fault(df);
    }

    INFO_STREAM << "Controller::stop_acquisition() - [END]" << std::endl;
}

/*******************************************************************
 * \brief collect the data from the acquisition
 *******************************************************************/
AcquisitionData Controller::collect(void)
{
    // exceptions are managed by the process_message method.
#ifdef CONTROLLER_LOG_ACQUISITION_TIME
    yat::Timer t1;
#endif

    AcquisitionData acquisition_data = m_collect_task->collect();

#ifdef CONTROLLER_LOG_ACQUISITION_TIME
    INFO_STREAM << "Elapsed time : " << t1.elapsed_msec() << " (ms)" << std::endl;
#endif

    return acquisition_data;
}

//============================================================================
//- Controller::process_message
//============================================================================
void Controller::process_message(yat::Message& msg) throw (Tango::DevFailed)
{
    try
    {
        switch (msg.type())
        {
            //-----------------------------------------------------
            case yat::TASK_INIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " Controller::TASK_INIT"                       << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                set_state(Tango::STANDBY);
            }
            break;

            //-----------------------------------------------------
            case yat::TASK_EXIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " Controller::TASK_EXIT"                       << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
            break;

            //-----------------------------------------------------
            case yat::TASK_TIMEOUT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " Controller::TASK_TIMEOUT"                    << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
            break;

            //-----------------------------------------------------------------------------------------------
            case yat::TASK_PERIODIC:
            {
                const Com::TriggerMode trigger_mode = m_collect_task->get_trigger_mode();

                for(;;)
                {
                    // collect data from device
                    AcquisitionData acquisition_data = collect();

                    // check if the acquisition data is not empty.
                    if(!acquisition_data.is_empty())
                    {
                        // give the data to the DataTask instance
                        m_data_task->add(acquisition_data);
                        
                        // in gate mode, we stop only when there is no more frame to collect
                        if(trigger_mode != Com::TriggerMode::TRIGGER_EXTERNAL_GATE)
                        {
                            break;
                        }
                    }
                    else
                    // acquisition data is empty
                    {
                        break;
                    }
                }

                if(m_collect_task->acquisition_is_completed())
                {
                    // stop acquisition if acquisition is running
                    if(m_collect_task->get_state() == Tango::RUNNING)
                    {
                        m_collect_task->stop_acquisition();
                    }

                    // if there is no more acquisition data to treat, we stop the periodic call
                    if((m_collect_task->get_state() != Tango::RUNNING) &&
                       (m_data_task->get_state()    != Tango::RUNNING))
                    {
                        enable_periodic_msg(false); // no periodic message till the next acquisition round

                        stop_stream_after_acquisition();

                        INFO_STREAM << "collecting finished." << std::endl;
                    }
                }
            }
            break;

            //-----------------------------------------------------                
            case CONTROLLER_TASK_STOP_MSG:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " Controller::CONTROLLER_TASK_STOP_MSG"        << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;

                // ask for an acquisition stop if acquisition is running
                // the acquisition will be really stopped, the next time we manage a TASK_PERIODIC event
                if(m_collect_task->get_state() == Tango::RUNNING)
                {
                    if(periodic_msg_enabled())
                    {
                        m_collect_task->force_acquisition_completed();
                    }
                    else
                    {
                        m_collect_task->stop_acquisition();
                    }
                }
            }
            break;

            //-----------------------------------------------------                
            case CONTROLLER_ABORT_MSG:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << " Controller::CONTROLLER_ABORT_MSG"            << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;

                // ask for an acquisition stop if acquisition is running
                // the acquisition will be really stopped, the next time we manage a TASK_PERIODIC event
                if(m_collect_task->get_state() == Tango::RUNNING)
                {
                    if(periodic_msg_enabled())
                    {
                        m_collect_task->force_acquisition_completed();
                    }
                    else
                    {
                        m_collect_task->stop_acquisition();
                    }
                }

                std::string error_msg = msg.get_data<std::string>();
                on_fault(error_msg);
            }
            break;
        }
    }
    catch (yat::Exception& ex)
    {
        ex.dump();
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << ex.errors[0].origin << std::endl;
        error_msg << "Desc\t: "   << ex.errors[0].desc   << std::endl;
        error_msg << "Reason\t: " << ex.errors[0].reason << std::endl;
        ERROR_STREAM << "Exception from - process_message() : " << error_msg.str() << std::endl;
        on_fault(error_msg.str());
        throw;
    }
}

//*****************************************************************************
/*******************************************************************
 * \brief Instanciates the correct stream object
 *******************************************************************/
void Controller::build_stream(const Controller::InitParameters & in_init_parameters)
{
    DEBUG_STREAM << "Controller::build_stream() - [BEGIN]" << endl;

    try
    {
        // searching the stream type label
        enum Stream::Types stream_type = convert_tango_stream_type_to_stream_type(in_init_parameters.m_stream_type);

        // already a stream ? check is the type has changed -> create a new one
        if((m_stream.is_null()) || (m_stream->get_type() != stream_type))
        {
            Stream * stream = NULL;

            //creating the stream
            INFO_STREAM << "Creating stream object : " << in_init_parameters.m_stream_type << endl;

            if(stream_type == Stream::Types::NO_STREAM)
            {
                stream = new StreamNo(m_device, this);
            }
            else
            if(stream_type == Stream::Types::LOG_STREAM)
            {
                stream = new StreamLog(m_device, this);
            }
            else
            if(stream_type == Stream::Types::CSV_STREAM)
            {
                stream = new StreamCsv(m_device, this);
            }
            else
            if(stream_type == Stream::Types::NEXUS_STREAM)
            {
                stream = new StreamNexus(m_device, this);
            }
            
            m_stream.reset(stream);
        }

        // configuring the stream write mode
        enum Stream::WriteModes stream_write_mode = 
            convert_tango_stream_write_mode_to_stream_write_mode(in_init_parameters.m_stream_write_mode);
        
        if(m_stream->get_write_mode() != stream_write_mode)
            m_stream->set_write_mode(stream_write_mode);

        // Set the target path
        if(m_stream->get_target_path() != in_init_parameters.m_stream_target_path)
            m_stream->set_target_path(in_init_parameters.m_stream_target_path); 

        // Set the target file
        if(m_stream->get_target_file() != in_init_parameters.m_stream_target_file)
            m_stream->set_target_file(in_init_parameters.m_stream_target_file); 

        // Set the number of acquisition per file
        if(m_stream->get_nb_acq_per_file() != in_init_parameters.m_stream_nb_acq_per_file)
            m_stream->set_nb_acq_per_file(in_init_parameters.m_stream_nb_acq_per_file); 

        // Set the number of acquired frames
        if(m_stream->get_nb_data_per_acq() != static_cast<uint32_t>(get_frames_number()))
            m_stream->set_nb_data_per_acq(static_cast<uint32_t>(get_frames_number())); 

        // Set the items configuration (bit field)
        uint32_t stream_items = convert_tango_stream_items_to_stream_items_bit_field(in_init_parameters.m_stream_items);

        if(m_stream->get_items() != stream_items)
            m_stream->set_items(stream_items);
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
 //       on_abort(df);
    }
    DEBUG_STREAM << "Controller::build_stream() - [END]" << endl;
}

/****************************************************************************
 * \brief Create an init parameters object using current stream configuration
 * \return init parameters which contains the current stream configuration
*****************************************************************************/
Controller::InitParameters Controller::create_stream_init_parameters(void)
{
    return Controller::InitParameters("", // not used for the stream
                                      0, // not used for the stream
                                      convert_stream_type_to_tango_stream_type(m_stream->get_type()),
                                      m_stream->get_target_path(),
                                      m_stream->get_target_file(),
                                      m_stream->get_nb_acq_per_file(),
                                      convert_stream_write_mode_to_tango_stream_write_mode(m_stream->get_write_mode()),
                                      convert_stream_items_bit_field_to_tango_stream_items(m_stream->get_items()));
}

/*******************************************************************
 * \brief Setter of the stream type
 * \param[in] in_stream_type stream type
*******************************************************************/
void Controller::set_stream_type(const std::string & in_stream_type)
{
    InitParameters init_parameters = create_stream_init_parameters();
    init_parameters.m_stream_type = in_stream_type;

    // update or rebuild the stream
    build_stream(init_parameters);
}

/*******************************************************************
 * \brief Start the stream object for a new acquisition
 ******************************************************************/
void Controller::start_stream_for_acquisition(void)
{
    InitParameters init_parameters = create_stream_init_parameters();

    // update or rebuild the stream
    build_stream(init_parameters);

    // init a new acquisition
    m_stream->init();
}

/*******************************************************************
 * \brief Stop the stream object after a new acquisition
 ******************************************************************/
void Controller::stop_stream_after_acquisition(void)
{
    m_stream->close();
}

/*******************************************************************
 * \brief Getter of the stream type
 * \return stream type
*******************************************************************/
std::string Controller::get_stream_type(void)
{
    return convert_stream_type_to_tango_stream_type(m_stream->get_type());
}

/*******************************************************************
 * \brief Setter of the stream target path
 * \param[in] in_stream_target_path stream target path
*******************************************************************/
void Controller::set_stream_target_path(const std::string & in_stream_target_path)
{
    InitParameters init_parameters = create_stream_init_parameters();
    init_parameters.m_stream_target_path = in_stream_target_path;

    // update or rebuild the stream
    build_stream(init_parameters);
}

/*******************************************************************
 * \brief Getter of the stream target path
 * \return stream target path
*******************************************************************/
std::string Controller::get_stream_target_path(void)
{
    return m_stream->get_target_path();
}

/*******************************************************************
 * \brief Setter of the stream target file
 * \param[in] in_stream_target_file stream target file
*******************************************************************/
void Controller::set_stream_target_file(const std::string & in_stream_target_file)
{
    InitParameters init_parameters = create_stream_init_parameters();
    init_parameters.m_stream_target_file = in_stream_target_file;

    // update or rebuild the stream
    build_stream(init_parameters);
}

/*******************************************************************
 * \brief Getter of the stream target file
 * \return stream target file
*******************************************************************/
std::string Controller::get_stream_target_file(void)
{
    return m_stream->get_target_file();
}

/*********************************************************************
 * \brief Setter of the stream number of acquisition per file
 * \param[in] in_stream_nb_acq_per_file number of acquisition per file
**********************************************************************/
void Controller::set_stream_nb_acq_per_file(const uint32_t & in_stream_nb_acq_per_file)
{
    InitParameters init_parameters = create_stream_init_parameters();
    init_parameters.m_stream_nb_acq_per_file = in_stream_nb_acq_per_file;

    // update or rebuild the stream
    build_stream(init_parameters);
}

/*******************************************************************
 * \brief Getter of the stream number of data per acquisition
 * \return stream number of acquisition per file
*******************************************************************/
uint32_t Controller::get_stream_nb_acq_per_file(void)
{
    return m_stream->get_nb_acq_per_file();
}

/*******************************************************************
 * \brief Getter of the stream number of data per acquisition
 * \param[in] in_acquisition_data acquisition data
*******************************************************************/
void Controller::update_stream(const AcquisitionData & in_acquisition_data)
{
    m_stream->update(in_acquisition_data);
}

/*******************************************************************
 * \brief Reset the stream file index
 *******************************************************************/
void Controller::stream_reset_index(void)
{
    m_stream->reset_index();
}

/*******************************************************************
 * \brief Get the data stream string
 * \return the data stream string (stream file and data items)
 *******************************************************************/
std::string Controller::get_data_streams(void)
{
    return m_stream->get_data_streams();
}

//*****************************************************************************
/*******************************************************************
 * \brief Convert a tango stream type label to a stream type enum
 * \param[in] in_tango_stream_type Tango stream type in label
 * \return the stream type enum
*******************************************************************/
enum Stream::Types Controller::convert_tango_stream_type_to_stream_type(const std::string & in_tango_stream_type)
{
    enum Stream::Types stream_type;
    string             tango_stream_type = in_tango_stream_type;

    std::transform(tango_stream_type.begin(), tango_stream_type.end(), tango_stream_type.begin(), ::toupper);

    // searching the stream type label
    const std::vector<std::string>::const_iterator 
    iterator = find(TANGO_STREAM_TYPE_LABELS.begin(), 
                    TANGO_STREAM_TYPE_LABELS.end  (),
                    tango_stream_type               );

    // found it
    if (iterator != TANGO_STREAM_TYPE_LABELS.end()) 
    {
        stream_type = TANGO_STREAM_TYPE_LABELS_TO_TYPE[iterator - TANGO_STREAM_TYPE_LABELS.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Incorrect stream type : " << tango_stream_type << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::convert_tango_stream_type_to_stream_type()");
    }

    return stream_type;
}

/*******************************************************************
 * \brief Convert a stream type enum to a tango stream type label
 * \param[in] in_stream_type stream type enum
 * \return a tango stream type label
*******************************************************************/
std::string Controller::convert_stream_type_to_tango_stream_type(const enum Stream::Types & in_stream_type)
{
    string tango_stream_type;

    // we need to convert the stream type to a tango stream type
    // search the stream type in a container
    const std::vector<enum Stream::Types>::const_iterator 
    iterator = find(TANGO_STREAM_TYPE_LABELS_TO_TYPE.begin(), 
                    TANGO_STREAM_TYPE_LABELS_TO_TYPE.end  (),
                    in_stream_type                          );

    // found it
    if (iterator != TANGO_STREAM_TYPE_LABELS_TO_TYPE.end()) 
    {
        tango_stream_type = TANGO_STREAM_TYPE_LABELS[iterator - TANGO_STREAM_TYPE_LABELS_TO_TYPE.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Impossible to found the stream type " << tango_stream_type << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::convert_stream_type_to_tango_stream_type()");
    }

    return tango_stream_type;
}

/************************************************************************
 * \brief Convert a tango stream write mode label to a stream write mode
 * \param[in] in_tango_stream_write_mode Tango stream write mode in label
 * \return the stream write mode enum
*************************************************************************/
enum Stream::WriteModes Controller::convert_tango_stream_write_mode_to_stream_write_mode(const std::string & in_tango_stream_write_mode)
{
    enum Stream::WriteModes stream_write_mode;
    string tango_stream_write_mode = in_tango_stream_write_mode;

    std::transform(tango_stream_write_mode.begin(), tango_stream_write_mode.end(), tango_stream_write_mode.begin(), ::toupper);

    // searching the stream type label
    const std::vector<std::string>::const_iterator 
    iterator = find(TANGO_STREAM_WRITE_MODE_LABELS.begin(), 
                    TANGO_STREAM_WRITE_MODE_LABELS.end  (),
                    tango_stream_write_mode               );

    // found it
    if (iterator != TANGO_STREAM_TYPE_LABELS.end()) 
    {
        stream_write_mode = TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE[iterator - TANGO_STREAM_WRITE_MODE_LABELS.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Incorrect stream write mode : " << tango_stream_write_mode << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::convert_tango_stream_write_mode_to_stream_write_mode()");
    }

    return stream_write_mode;
}

/*******************************************************************
 * \brief Convert a stream write mode enum to a tango stream write mode label
 * \param[in] in_stream_write_mode Stream write mode enum
 * \return a tango stream write mode
*******************************************************************/
std::string Controller::convert_stream_write_mode_to_tango_stream_write_mode(const enum Stream::WriteModes & in_stream_write_mode)
{
    string tango_stream_write_mode;

    // we need to convert the stream type to a tango stream type
    // search the stream type in a container
    const std::vector<enum Stream::WriteModes>::const_iterator 
    iterator = find(TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE.begin(), 
                    TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE.end  (),
                    in_stream_write_mode                                );

    // found it
    if (iterator != TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE.end()) 
    {
        tango_stream_write_mode = TANGO_STREAM_WRITE_MODE_LABELS[iterator - TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        MsgErr << "Impossible to found the stream write mode " << in_stream_write_mode << std::endl;

        Tango::Except::throw_exception("LOGIC_ERROR",
                                       MsgErr.str().c_str(),
                                       "Controller::convert_stream_write_mode_to_tango_stream_write_mode()");
    }

    return tango_stream_write_mode;
}

/************************************************************************
 * \brief Convert a tango stream items vector to a stream items bit field
 * \param[in] in_tango_stream_items Tango stream items
 * \return the stream items bit field
*************************************************************************/
uint32_t Controller::convert_tango_stream_items_to_stream_items_bit_field(const std::vector<std::string> & in_tango_stream_items)
{
    size_t   nb_items     = in_tango_stream_items.size();
    uint32_t stream_items = 0;

    for(size_t item_index = 0 ; item_index < nb_items ; item_index++)
    {
        // search the stream item in a container
        const std::vector<std::string>::const_iterator 
        iterator = find(TANGO_STREAM_ITEMS_LABELS.begin(), 
                        TANGO_STREAM_ITEMS_LABELS.end  (),
                        in_tango_stream_items[item_index]);
        // found it
        if (iterator != TANGO_STREAM_ITEMS_LABELS.end()) 
        {
            stream_items |= TANGO_STREAM_ITEM_LABEL_TO_ITEM_BIT[iterator - TANGO_STREAM_ITEMS_LABELS.begin()]; // calculation gives the index
        }
        else
        {
            std::ostringstream MsgErr;
            MsgErr << "Impossible to found the stream items " << in_tango_stream_items[item_index] << std::endl;

            Tango::Except::throw_exception("LOGIC_ERROR",
                                           MsgErr.str().c_str(),
                                           "Controller::convert_tango_stream_items_to_stream_items_bit_field()");
        }
    }
    
    return stream_items;
}

/************************************************************************
 * \brief Convert a stream items bit field to a tango stream items vector
 * \param[in] in_stream_items stream items bit field
 * \return Tango stream items
*************************************************************************/
std::vector<std::string> Controller::convert_stream_items_bit_field_to_tango_stream_items(const uint32_t & in_stream_items)
{
    size_t nb_items = TANGO_STREAM_ITEMS_LABELS.size();
    std::vector<std::string> tango_stream_items;

    for(size_t item_index = 0 ; item_index < nb_items ; item_index++)
    {
        if(in_stream_items & TANGO_STREAM_ITEM_LABEL_TO_ITEM_BIT[item_index])
        {
            tango_stream_items.push_back(TANGO_STREAM_ITEMS_LABELS[item_index]);
        }
    }

    return tango_stream_items;
}

/************************************************************************
 * \brief Checks if an acquisition generates data files 
 * \return true if it generates data files, else false
*************************************************************************/
bool Controller::get_file_generation(void)
{
	enum Stream::Types stream_type = m_stream->get_type();

	return ((stream_type == Stream::Types::CSV_STREAM  ) || 
	        (stream_type == Stream::Types::NEXUS_STREAM));
}

//###########################################################################
}