//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <string>

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <sstream>
#include <yat/time/Timer.h>

#include <tango.h>

#include <CollectTask.h>

#include <ComSim.h>
#include <ComNet.h>
#include <Com.h>

template<typename T>
void convertFrameBuffer(std::vector<T> & out_Container, const int32_t * in_readoutData, int in_NbElement) 
{
    while(in_NbElement > 0)
    {
        out_Container.push_back(static_cast<T>(*in_readoutData));
        in_readoutData++;
        in_NbElement--  ;
    }
}

//------------------------------------------------------------------------------------------------------
// main test
//------------------------------------------------------------------------------------------------------
int main (int argc, char* argv[])
{
    // defines defaults
    string HostAddress = "applab.dectris.com"; 
    int    port        = 1031;

    if (argc != 3)
    {   
        // We expect 3 arguments: the program name, the ip address and the ip port
        std::cout << "Usage  : " << argv[0] << " HostAddress        Port" << std::endl;
        std::cout << "Exemple: " << argv[0] << " applab.dectris.com 1031" << std::endl;
    }
    else
    {
        HostAddress = argv[1]      ;
        port        = stoi(argv[2]);
    }


    std::cout<<"*********** STARTING ********************"<<std::endl;

    Mythen2Detector_ns::CollectTask * TaskManager = new Mythen2Detector_ns::CollectTask(NULL);

    TaskManager->connect(HostAddress, port);

    try 
    {
        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_assembly_date   ();
        TaskManager->get_software_version();
        TaskManager->get_firmware_version();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_dcs_temperature_kelvin ();
        TaskManager->get_dcs_temperature_celcius();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_frame_rate_max();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_max_nb_modules  ();
        TaskManager->get_nb_modules();

        TaskManager->select_module(1);
        TaskManager->get_selected_modules();

        TaskManager->select_all_modules();
        TaskManager->get_selected_modules();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_modules_temperature_kelvin ();
        TaskManager->get_modules_temperature_celcius();
        std::cout << TaskManager->get_string_modules_temperatures_celcius() << std::endl;

        {
            std::cout << "----------------------------------------------" << std::endl;
            vector< vector<bool> > badChannelsByModule      ;
            vector<int32_t>        badChannelsNumberByModule;
            int32_t                allBadChannelsNumber     ;

            TaskManager->get_modules_bad_channels(badChannelsByModule, badChannelsNumberByModule, allBadChannelsNumber);
        }

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_modules_humidity();
        std::cout << TaskManager->get_string_modules_humidities() << std::endl;

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_modules_high_voltage();
        std::cout << TaskManager->get_string_modules_high_voltages() << std::endl;

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_modules_firmware_version();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_modules_serial_number();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_serial_number();
    } 
    catch(...) {};

    std::cout << "----------------------------------------------" << std::endl;
    TaskManager->reset            ();
    TaskManager->start_acquisition(true, 50);
    TaskManager->stop_acquisition ();

    {
        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_delay_after_frame   (1000);
        TaskManager->get_delay_after_frame();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_frames_number(10);
        TaskManager->get_frames_number();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_number_of_bits(4);
        TaskManager->get_number_of_bits();
        TaskManager->set_number_of_bits(8);
        TaskManager->get_number_of_bits();
        TaskManager->set_number_of_bits(16);
        TaskManager->get_number_of_bits();

        try 
        {
            TaskManager->set_number_of_bits(23);
        } 
        catch(...) {};

        TaskManager->set_number_of_bits(24);
        TaskManager->get_number_of_bits();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_exposure_time(200000);
        TaskManager->get_exposure_time();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->get_readout_time_of_bits_mode( 4);
        TaskManager->get_readout_time_of_bits_mode( 8);
        TaskManager->get_readout_time_of_bits_mode(16);
        TaskManager->get_readout_time_of_bits_mode(24);
        TaskManager->get_readout_time();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_delay_before_frame  (1000);
        TaskManager->get_delay_before_frame();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_bad_channel_interpolation(true);
        TaskManager->get_bad_channel_interpolation();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_flatfield_correction(true);
        TaskManager->get_flatfield_correction();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_rate_correction(true);
        TaskManager->get_rate_correction();

        std::cout << "----------------------------------------------" << std::endl;

        try 
        {
			TaskManager->set_modules_energy(8.03);
        } 
        catch(...) {};

        try 
        {
			TaskManager->set_module_energy(1, 7.01);
        } 
        catch(...) {};

        TaskManager->get_modules_energy    ();
        TaskManager->get_modules_energy_min();
        TaskManager->get_modules_energy_max();

        std::cout << "----------------------------------------------" << std::endl;
        try 
        {
			TaskManager->set_modules_threshold(10.0);
        } 
        catch(...) {};

        try 
        {
			TaskManager->set_module_threshold(1, 9.01);
        } 
        catch(...) {};
		
        TaskManager->get_modules_threshold();
        TaskManager->get_modules_threshold_min();
        TaskManager->get_modules_threshold_max();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->load_predefined_setting_Cu();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_gates_number_by_frame (6);
        TaskManager->get_gates_number_by_frame();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_gate(false);
        TaskManager->get_gate ();
        TaskManager->set_gate(true );
        TaskManager->get_gate ();
        TaskManager->set_gate(false);
        TaskManager->get_gate ();
        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_trigger(false);
        TaskManager->get_trigger();
        TaskManager->set_trigger(true );
        TaskManager->get_trigger();
        TaskManager->set_trigger(false);
        TaskManager->get_trigger();
        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_continuous_trigger(false);
        TaskManager->get_continuous_trigger();
        TaskManager->set_continuous_trigger(true );
        TaskManager->get_continuous_trigger();
        TaskManager->set_continuous_trigger(false);
        TaskManager->get_continuous_trigger();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_trigger_mode(Mythen2Detector_ns::Com::TriggerMode::TRIGGER_INTERNAL);
        TaskManager->get_trigger_mode();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_trigger_mode(Mythen2Detector_ns::Com::TriggerMode::TRIGGER_EXTERNAL_SINGLE);
        TaskManager->get_trigger_mode();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_trigger_mode(Mythen2Detector_ns::Com::TriggerMode::TRIGGER_EXTERNAL_MULTIPLE);
        TaskManager->get_trigger_mode();

        std::cout << "----------------------------------------------" << std::endl;
        TaskManager->set_trigger_mode(Mythen2Detector_ns::Com::TriggerMode::TRIGGER_EXTERNAL_GATE);
        TaskManager->get_trigger_mode();

        try 
        {
            TaskManager->get_readout_time_of_bits_mode(23);
        } 
        catch(...) {};
    }

    std::cout << "----------------------------------------------" << std::endl;
    TaskManager->get_dcs_status();

    //---------------------------------------------------------

    {
        TaskManager->reset();

        TaskManager->get_dcs_status();

        // 1ms = 10000 units of 100ns
        long long durationMs = 50;
        long long duration   = durationMs * 10000LL;
        int32_t   nbFrames   = 10;
        int32_t   nbits      = 4;
        int       readoutFrameNb  = 10; 

        TaskManager->load_predefined_setting_Cu();
        TaskManager->set_number_of_bits(nbits   );
        TaskManager->set_exposure_time(duration);
        TaskManager->set_frames_number(nbFrames);

        TaskManager->set_trigger_mode(Mythen2Detector_ns::Com::TriggerMode::TRIGGER_INTERNAL);

        TaskManager->start_acquisition(true, 50 * nbFrames);

        {
            yat::Timer t1;

            std::cout << "starting acquisition ok" << std::endl;
            TaskManager->get_dcs_status();

            for(int s1 = 0 ; s1 < nbFrames ; s1++)
            {
                TaskManager->get_dcs_status();
                sleep((durationMs / 1000));      // sleep during acquisition
            }

            // sleep(nbFrames * (durationMs / 1000));      // sleep during acquisition

            vector< vector<int32_t> > readoutData;

            readoutData = TaskManager->get_readout_data(readoutFrameNb);

            for(int frameIndex = 0 ; frameIndex < readoutFrameNb ; frameIndex++)
            {
                const vector<int32_t> & ChannelsData = readoutData[frameIndex];
                
                std::cout << "frame (" << frameIndex << ") size (" << ChannelsData.size() << ") values : ";
                
                for(int i = 0 ; i < 350 ; i++)
                {
                    std::cout << ChannelsData[i] << ", ";
                }

                std::cout << std::endl;
            }
            
            TaskManager->get_dcs_status();

            TaskManager->stop_acquisition();

            std::cout<<"Elapsed time : " << t1.elapsed_msec() << " (ms)" << std::endl << std::endl;

            TaskManager->get_dcs_status();
        }
    }

    TaskManager->disconnect();

    delete TaskManager;

    std::cout<<"*********** ENDING ********************"<<std::endl;

/*        {
            std::cout << "starting test" << std::endl;

            int       readoutDataSize = AllModuleschannelsNb;
            int32_t * readoutData     = new int32_t[readoutDataSize];

            if(Com->action_command(Mythen2Detector_ns::Com::ServerCmd::CMD_TEST_DATA_SET, readoutData, readoutDataSize))
            {
                std::cout << "test values : ";
                
                for(int i = 0 ; i < readoutDataSize ; i++)
                {
                    std::cout << readoutData[i] << ", ";
                }

                std::cout << std::endl;
            }

            std::vector<int32_t> Container32;
            std::vector<int32_t> Container16;
            std::vector<int32_t> Container8 ;

            convertFrameBuffer(Container32, readoutData, readoutDataSize);
            convertFrameBuffer(Container16, readoutData, readoutDataSize);
            convertFrameBuffer(Container8 , readoutData, readoutDataSize);

            std::cout << "container 8  : (" << Container8.size () << ") elements" << std::endl;
            std::cout << "container 16 : (" << Container16.size() << ") elements" << std::endl;
            std::cout << "container 32 : (" << Container32.size() << ") elements" << std::endl;

            delete [] readoutData;
        }

        get_status(Com);*/

    return 0;
}
//------------------------------------------------------------------------------------------------------
